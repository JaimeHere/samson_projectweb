from django.contrib import admin

from apps.contractor.models import Contractor, ContractorEmployeeField, EmployeeFieldTimeSheet, EmployeeField
admin.site.register(Contractor)
admin.site.register(ContractorEmployeeField)
admin.site.register(EmployeeFieldTimeSheet)
admin.site.register(EmployeeField)
