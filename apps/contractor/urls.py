"""
URLs CONTRACTORS
"""
from django.contrib.auth.decorators import login_required

from django.conf.urls import url
from apps.contractor.views import index, ContractorCreate, ContractorList, ContractorUpdate, ContractorDelete, ContractorDetail, \
    EmployeeFieldList, EmployeeFieldCreate, EmployeeFieldUpdate, \
    ContractorEmployeeFieldCreate, ContractorEmployeeFieldList, ContractorEmployeeFieldAllList, ContractorEmployeeFieldUpdate, ContractorEmployeeFieldAllUpdate,\
    EmployeeFieldProfile,get_data, ContractorChartData


urlpatterns = [
    url(r'^$', index, name='index'),

    url(r'^(?P<pk>\d+)/create/$', \
        login_required(ContractorCreate.as_view()), name='contractor_create'),

    url(r'^(?P<pk>\d+)/list/$', \
        login_required(ContractorList.as_view()), name='contractor_list'),

    url(r'^(?P<companies_id>\d+)/update/(?P<pk>\d+)/$', \
        login_required(ContractorUpdate.as_view()), name='contractor_update'),

    url(r'^(?P<companies_id>\d+)/delete/(?P<pk>\d+)/$', \
        login_required(ContractorDelete.as_view()), name='contractor_delete'),

    url(r'^(?P<companies_id>\d+)/(?P<pk>\d+)/detail/$', login_required(ContractorDetail.as_view()), name='contractor_detail'),

    # URLs CONTRACTOR EMPLOYEE FIELD
    url(r'^(?P<companies_id>\d+)/(?P<pk>\d+)/contractor_employee_field_list/$', \
        login_required(ContractorEmployeeFieldList.as_view()), name='contractor_employee_field_list'),

    # all employees
    url(r'^(?P<pk>\d+)/contractor_employee_field_all_list/$', \
        login_required(ContractorEmployeeFieldAllList.as_view()), name='contractor_employee_field_all_list'),

    url(r'^(?P<companies_id>\d+)/(?P<contractor_id>\d+)/contractor_employee_field_create/(?P<pk>\d+)/$', \
        login_required(ContractorEmployeeFieldCreate.as_view()), name='contractor_employee_field_create'),

    url(r'^(?P<companies_id>\d+)/(?P<contractor_id>\d+)/contractor_employee_field_update/(?P<pk>\d+)/$', \
        login_required(ContractorEmployeeFieldUpdate.as_view()), name='contractor_employee_field_update'),

    url(r'^(?P<companies_id>\d+)/(?P<contractor_id>\d+)/contractor_employee_field_all_update/(?P<pk>\d+)/$', \
        login_required(ContractorEmployeeFieldAllUpdate.as_view()), name='contractor_employee_field_all_update'),


    # URLs: EMPLOYEE FIELDs
    url(r'^(?P<companies_id>\d+)/employee_field_create/(?P<pk>\d+)/$', login_required(EmployeeFieldCreate.as_view()), name='employee_field_create'),
    url(r'^(?P<companies_id>\d+)/(?P<pk>\d+)/employee_field_update/$', login_required(EmployeeFieldUpdate.as_view()),
        name='employee_field_update'),
    url(r'^(?P<pk>\d+)/employee_field_list/$', login_required(EmployeeFieldList.as_view()), name='employee_field_list'),

    # Profile
    url(r'^(?P<companies_id>\d+)/(?P<contractor_id>\d+)/(?P<pk>\d+)/employee_field_profile/$', login_required(EmployeeFieldProfile.as_view()),
        name='contractor_employee_field_profile'),

    # DATA Graph
    url(r'^(?P<pk>\d+)/api/data/$', login_required(get_data), name='api-data'),
    url(r'^(?P<pk>\d+)/api/chart/data/$', login_required(ContractorChartData.as_view())),
    ###################

]
