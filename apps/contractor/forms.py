from django import forms
from apps.contractor.models import Contractor, EmployeeField, ContractorEmployeeField
from apps.company.models import Company, CategoryEmployeeField

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset, HTML
from crispy_forms.bootstrap import PrependedText, Div, FormActions  # InlineRadios, TabHolder, Tab, FormActions

US_STATES = (
    ('AL', 'Alabama'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('CA', 'California'), ('CO', 'Colorado'),
    ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'),
    ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'),
    ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'),
    ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'),
    ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'),
    ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'),
    ('PA', 'Pennsylvania'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'),
    ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VA', 'Virginia'),
    ('WA', 'Washington'),
    ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming'))

COUNTRY = (
    ('US', 'United States'), ('CAD', 'Canada'))

class ContractorForm(forms.ModelForm):
    class Meta:
        model = Contractor

        fields = ['companies', 'name', 'ceo', 'president', 'fed', 'email', 'website_url', 'phone_number',
                  'phone_number_ext', 'summary', 'notes',
                  'city_address', 'country_address', 'state_address', 'zip_code_address', 'street_address', 'is_active',
                  ]

    name = forms.CharField(label="Contractor Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'",
                                      'placeholder': 'Contractor Name'}
                           ))

    ceo = forms.CharField(label="CEO Name:", required=True, max_length=75,
                          widget=forms.TextInput(
                              attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                     'placeholder': 'both name(s) e.g Jon Doe'}))

    president = forms.CharField(label="President Name:", required=True,
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                           'placeholder': 'both name(s) e.g Jon Doe'}))

    fed = forms.CharField(label="FED:", required=True,
                          widget=forms.TextInput(
                              attrs={'type': 'text', 'name': 'fed', 'class': 'form-control', 'required': True,
                                     'placeholder': 'FED Register'}))

    phone_number = forms.CharField(label="Phone Number:",
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control', 'data-inputmask': "'mask':'(999) 999-9999'",
                                              'placeholder': '(___) ___-____'}))

    phone_number_ext = forms.CharField(label="Phone number ext:",
                                       widget=forms.TextInput(
                                           attrs={'type': 'number',
                                                  'class': 'form-control input-number value="1" min="1" max="10"',
                                                  'placeholder': 'Phone number ext.'}))

    street_address = forms.CharField(label="Street address:", required=False, max_length=150,
                                     widget=forms.TextInput(
                                         attrs={'type': 'text',
                                                'class': 'form-control input-number value="1" min="1" max="10"',
                                                'placeholder': 'e.g: 4422 Faroe Place'}))

    city_address = forms.CharField(label="City address:", required=False, max_length=150,
                                   widget=forms.TextInput(
                                       attrs={'type': 'text',
                                              'class': 'form-control input-number value="1" min="1" max="10"',
                                              'placeholder': 'e.g: Rockville'}))

    country_address = forms.CharField(label="Country address:", required=True, max_length=50,
                                      widget=forms.Select(choices=COUNTRY))

    state_address = forms.CharField(label="State address:", required=True, max_length=50,
                                    widget=forms.Select(choices=US_STATES))

    zip_code_address = forms.CharField(label="zip code address:", required=True, max_length=10,
                                       widget=forms.TextInput(
                                           attrs={'type': 'text', 'class': 'form-control',
                                                  'placeholder': 'e.g: 20850'}))

    email = forms.EmailField(label="email:", required=False,
                             widget=forms.TextInput(
                                 attrs={'type': 'email', 'required': True, 'placeholder': 'jon.doe@example.com'}))

    website_url = forms.URLField(initial='http://', label="Website URL:", required=False,
                                 widget=forms.TextInput(
                                     attrs={'type': 'url', 'class': 'form-control',
                                            'placeholder': 'http://www.example.com'}))
    summary = forms.CharField(label="Summary:", required=False,
                              widget=forms.Textarea(
                                  attrs={'type': 'text', 'class': 'form-control',
                                         'placeholder': 'Summary About this Contractor'}))

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'About this Contractor'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    def __init__(self, companies_id, *args, **kwargs):
        super(ContractorForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)
        # self.error_class = ''
        # self.helper.labels_uppercase = True

        self.helper.form_id = 'add-new-company-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        # self.helper.form_action = 'submit_survey'
        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if companies_id:
            self.helper.form.fields['companies'].queryset = Company.objects.all().filter(pk=companies_id)

        self.helper.layout = Layout(
            Fieldset('<i class="fa fa-info-circle"></i> General Information',
                     Div(
                         PrependedText('companies', ''),
                         PrependedText('name', '<i class="glyphicon glyphicon-home"></i>'),
                         PrependedText('ceo', '<i class="glyphicon glyphicon-user"></i>'),

                         css_class='col-md-6'
                     ),

                     Div(
                         PrependedText('president', '<i class="glyphicon glyphicon-user"></i>'),
                         PrependedText('fed', '<i class="glyphicon glyphicon-asterisk"></i>'),

                         css_class='col-md-6'
                     ),
                     ),

            Fieldset('<i class="fa fa-phone"></i> Contact Information',
                     Div(
                         PrependedText('phone_number', '<i class="glyphicon glyphicon-phone-alt"></i>',
                                       placeholder='(___) ___-____'),
                         PrependedText('phone_number_ext', '<i class="glyphicon glyphicon-phone-alt"></i>'),
                         css_class='col-md-6'
                     ),

                     Div(

                         PrependedText('email', '<i class="glyphicon glyphicon-envelope"></i>'),
                         PrependedText('website_url', '<i class="glyphicon glyphicon-link"></i>'),

                         css_class='col-md-6'
                     ),
                     ),

            Fieldset('<i class="fa fa-map-marker"></i> Location Information',
                     Div(
                         PrependedText('country_address', ''),
                         PrependedText('street_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         PrependedText('city_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         css_class='col-md-6'
                     ),
                     Div(
                         PrependedText('state_address', ''),
                         PrependedText('zip_code_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         css_class='col-md-6'
                     ),
                     ),
            Fieldset('<i class="fa fa-align-left"></i> Summary of Contractor',
                     Div(
                         PrependedText('summary', '<i class="fa fa-align-left"></i>'),
                         css_class='col-md-12'
                     ),
                     ),

            Fieldset('<i class="fa fa-align-left"></i> Notes Information',
                     Div(
                         PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                         PrependedText('is_active', ''),
                         css_class='col-md-12'
                     ),
                     ),
        )


class EmployeeFieldForm(forms.ModelForm):
    class Meta:
        model = EmployeeField
        fields = ['category_employees_field', 'first_name', 'last_name', 'email', 'avatar',
                  'cell_phone_number', 'city_address', 'country_address', 'state_address', 'zip_code_address',
                  'street_address', 'office_or_field', 'notes', 'is_active',
                  ]

    first_name = forms.CharField(label="First name:", required=True, max_length=100,
                                 widget=forms.TextInput(
                                     attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                            'data-validate-length-range': "'6'", 'data-validate-words': "'2'",
                                            'placeholder': 'first name(s) Doe'}
                                 ))

    last_name = forms.CharField(label="Last name:", required=True, max_length=100,
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                           'placeholder': 'last name(s) e.g White'}))

    email = forms.EmailField(label="email:", required=False, max_length=100,
                             widget=forms.TextInput(
                                 attrs={'type': 'email', 'required': True, 'placeholder': 'jon.doe@example.com'}))

    cell_phone_number = forms.CharField(label="Cell Phone Number:", max_length=14,
                                        widget=forms.TextInput(
                                            attrs={'class': 'form-control', 'data-inputmask': "'mask':'(999) 999-9999'",
                                                   'placeholder': '(___) ___-____'}))

    street_address = forms.CharField(label="Street address:", required=False, max_length=150,
                                     widget=forms.TextInput(
                                         attrs={'type': 'text',
                                                'class': 'form-control input-number value="1" min="1" max="10"',
                                                'placeholder': 'e.g: 4422 Faroe Place'}))

    city_address = forms.CharField(label="City address:", required=False, max_length=100,
                                   widget=forms.TextInput(
                                       attrs={'type': 'text',
                                              'class': 'form-control input-number value="1" min="1" max="10"',
                                              'placeholder': 'e.g: Rockville'}))

    country_address = forms.CharField(label="Country address:", required=True, max_length=100,
                                      widget=forms.Select(choices=COUNTRY))

    state_address = forms.CharField(label="State address:", required=True, max_length=2,
                                    widget=forms.Select(choices=US_STATES))

    zip_code_address = forms.CharField(label="zip code address:", required=True, max_length=5,
                                       widget=forms.TextInput(
                                           attrs={'type': 'text', 'class': 'form-control',
                                                  'placeholder': 'e.g: 20850'}))

    office_or_field = forms.CharField(label="Office/Field", required=False,
                                      widget=forms.CheckboxInput(
                                          attrs={'class': 'js-switch'}))

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control',
                                       'placeholder': 'About this Employee'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    def __init__(self, companies_id, *args, **kwargs):
        # self.helper.form.fields['company'].queryset = Company.objects.filter(company_id = 1)

        super(EmployeeFieldForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        # User.objects.get(username = request.GET['username']) self.kwargs['username']).id
        # self.helper.form.fields['category_employee'].queryset = CategoryEmployee.objects.filter(company_id=1)

        # a = User.objects.get(username = self.kwargs['username']).id

        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        # self.helper.form_action = 'submit_survey'
        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if companies_id:
            self.helper.form.fields['category_employees_field'].queryset = CategoryEmployeeField.objects.all().filter(companies_id=companies_id)

        self.helper.layout = Layout(
            Fieldset('<i class="fa fa-info-circle"></i> General Information',
                     Div(
                         PrependedText('category_employees_field', ''),

                         PrependedText('first_name', '<i class="glyphicon glyphicon-home"></i>'),
                         PrependedText('last_name', '<i class="glyphicon glyphicon-user"></i>'),
                         css_class='col-md-6'
                     ),
                     Div(
                         PrependedText('email', '<i class="glyphicon glyphicon-envelope"></i>'),
                         PrependedText('cell_phone_number', '<i class="glyphicon glyphicon-phone-alt"></i>',
                                       placeholder='(___) ___-____'),
                         css_class='col-md-6'
                     ),
                     ),

            Fieldset('<i class="fa fa-map-marker"></i> Location Information',
                     Div(
                         PrependedText('country_address', ''),
                         PrependedText('street_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         PrependedText('city_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         css_class='col-md-6'
                     ),
                     Div(
                         PrependedText('state_address', ''),
                         PrependedText('zip_code_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         css_class='col-md-6'
                     ),
                     ),
            Fieldset('<i class="fa fa-align-left"></i> Notes About of Employee:',
                     Div(
                         PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                         PrependedText('is_active', ''),

                         css_class='col-md-12'
                     ),
                     ),
        )


class ContractorEmployeeFieldForm(forms.ModelForm):
    class Meta:
        model = ContractorEmployeeField
        fields = ['contractors', 'employees_field', 'notes', 'is_active', ]

    contractors = forms.Select()
    employees_field = forms.Select()

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'About this Employee Field'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}
                                ))


    def __init__(self, companies_id, contractor_id, employees_field_id, *args, **kwargs):
        super(ContractorEmployeeFieldForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-contractor-employee-form'
        self.helper.form_method = 'POST'
        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if companies_id:
            self.helper.form.fields['contractors'].queryset = Contractor.objects.all().filter(
                companies_id=companies_id, id=contractor_id)

            self.helper.form.fields['employees_field'].queryset = EmployeeField.objects.all().filter(id=employees_field_id,
                category_employees_field__companies_id=companies_id)

        self.helper.layout = Layout(
            Div(
                PrependedText('contractors', ''),
                PrependedText('employees_field', ''),
                PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                PrependedText('is_active', ''),

                css_class='col-md-12'
            ),
            FormActions(
                Submit('save', 'Save changes'),
            ),
        )

