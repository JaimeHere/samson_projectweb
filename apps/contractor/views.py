from django.shortcuts import render, redirect

from apps.contractor.forms import ContractorForm, EmployeeFieldForm, ContractorEmployeeFieldForm
from apps.contractor.models import Contractor, EmployeeField, ContractorEmployeeField
from apps.project.models import ProjectContractor, DailyProduce, ProjectContractorEmployeeField
from apps.company.models import Company

from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from django.db.models import F, Q, Sum, Avg, Max, Min, Case, When

# Import Graph
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'base/index.html')

# Create your views here.
# ==================================
# CONTRACTOR CRUD
# ==================================
class ContractorCreate(CreateView):
    model = Contractor
    form_class = ContractorForm
    template_name = 'apps/contractor/contractor_form.html'
    success_url = reverse_lazy('contractor:contractor_list')

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(ContractorCreate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(ContractorCreate, self).get_initial()
        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        initial.update(
            {
                'companies_id': companies_id['id'],
                'companies': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(ContractorCreate, self).get_form_kwargs()
        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('contractor:contractor_list', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('contractor:contractor_list', args=(self.object.id,))


class ContractorList(ListView):
    model = Contractor
    template_name = 'apps/contractor/contractor_list.html'

    # Pasar parametros a la plantilla usando context
    def get_context_data(self, **kwargs):
        context = super(ContractorList, self).get_context_data(**kwargs)

        context['contractorList'] = Contractor.objects.all().filter(companies_id=self.kwargs['pk'])

        return context


class ContractorUpdate(UpdateView):
    model = Contractor
    form_class = ContractorForm
    template_name = 'apps/contractor/contractor_form.html'
    success_url = reverse_lazy('contractor:contractor_list')

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(ContractorUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(ContractorUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('contractor:contractor_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('contractor:contractor_list', args=(self.object.id,))


class ContractorDelete(DeleteView):
    model = Contractor
    template_name = 'apps/contractor/contractor_delete.html'
    success_url = reverse_lazy('contractor:contractor_list')


    def get_context_data(self, **kwargs):

        context = super(ContractorDelete, self).get_context_data(**kwargs)
        a = ''
        b  = ''
        a = ProjectContractor.objects.filter(contractors_id=self.kwargs['pk']).count()
        b = ContractorEmployeeField.objects.filter(contractors_id=self.kwargs['pk']).count()

        if a > 0:
            context['DeleteRegisterExist'] = a

        if b > 0:
            context['DeleteRegisterExist'] = b

        context['DeleteRegister'] = Contractor.objects.get(companies_id=self.kwargs['companies_id'], pk=self.kwargs['pk'])

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('contractor:contractor_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('contractor:contractor_list', args=(self.object.id,))


# ###################
# CONTRACTOR DETAIL #
# ###################

class ContractorDetail(DetailView):
    model = Contractor
    template_name = 'apps/contractor/contractor_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ContractorDetail, self).get_context_data(**kwargs)

        ########################################
        # Employee List Assigned to Contractor #
        ########################################
        contractor_employee_field_list = ContractorEmployeeField.objects.select_related('contractors'). \
            all().filter(contractors_id=self.kwargs['pk'])

        context['ContractorEmployeeFieldList'] = contractor_employee_field_list

        ########################################
        # Employee List Assigned to Contractor #
        ########################################
        project_contractor_list = ProjectContractor.objects.select_related('contractors'). \
            all().filter(contractors_id=self.kwargs['pk'])

        context['ProjectContractorList'] = project_contractor_list

        total_projects = 0
        total_man_days = 0
        total_production_exp = 0
        total_produccion_exe = 0
        total_employees = 0
        total_hours = 0
        total_cost_man_days_spend = 0

        # { MAN DAYS CALCULATOR
        total_projects = ProjectContractor.objects.all().filter(contractors_id=self.kwargs['pk']).count()
        total_employees_field = ContractorEmployeeField.objects.all().filter(contractors_id=self.kwargs['pk']).count()

        contractor_cost_hours = DailyProduce.objects. \
            values('project_contractor_employees_field__project_contractors__contractors__companies__name'). \
            filter(project_contractor_employees_field__project_contractors__contractors__id=self.kwargs['pk']). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            cost_md=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            hours=Sum('work_hours')
        )

        if contractor_cost_hours.count() > 0:
            if contractor_cost_hours[0]['cost'] is not None and contractor_cost_hours[0]['hours'] is not None:
                total_cost_man_days_spend = contractor_cost_hours[0]['cost']
                total_hours = contractor_cost_hours[0]['hours'] + 0

        data = ({
            'total_projects': total_projects,
            'total_man_days': total_hours / 8,
            'total_cost_man_days_spend': total_cost_man_days_spend,
            'total_produccion_exe': total_produccion_exe,
            'total_employees_field': total_employees_field
        })
        context['ContractorDetail'] = data

        return context

#############################
# CONTRACTOR EMPLOYEE FIELD #
#############################

class ContractorEmployeeFieldCreate(CreateView):
    model = ContractorEmployeeField
    form_class = ContractorEmployeeFieldForm
    template_name = 'apps/contractor/contractor_employee_field/contractor_employee_field_form.html'

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        form.save(commit=False)  # Para obtener el ID
        EmployeeField.objects.filter(id=form.instance.employees_field_id).update(is_assigned=True)

        form.save()  # Guardamos el codigo

        return super(ContractorEmployeeFieldCreate, self).form_valid(form)

    def get_initial(self):
        initial = super(ContractorEmployeeFieldCreate, self).get_initial()

        initial.update(
            {
                'companies':self.kwargs['companies_id'],
                'contractors': self.kwargs['contractor_id'],
                'contractor_id': self.kwargs['contractor_id'],
                'employees_field': self.kwargs['pk']
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(ContractorEmployeeFieldCreate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
                'contractor_id': self.kwargs['contractor_id'],
                'employees_field_id': self.kwargs['pk'],
            })

        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ContractorEmployeeFieldCreate, self).get_context_data(**kwargs)

        context['contractor_info'] = Contractor.objects.get(id=self.kwargs['contractor_id'])

        context.update({
            'contractor_id': self.kwargs['contractor_id'],
        })
        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('contractor:contractor_employee_field_list', kwargs={'pk': self.kwargs['contractor_id'],
                                                                                     'companies_id': self.kwargs[
                                                                                         'companies_id']})
        else:
            return reverse_lazy('contractor:contractor_employee_field_list', args=(self.object.id,))

# UPDATE
#########
class ContractorEmployeeFieldUpdate(UpdateView):
    model = ContractorEmployeeField
    form_class = ContractorEmployeeFieldForm
    template_name = 'apps/contractor/contractor_employee_field/contractor_employee_field_form.html'

    def get_initial(self):
        initial = super(ContractorEmployeeFieldUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(ContractorEmployeeFieldUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
                'contractor_id': self.kwargs['contractor_id'],
                'employees_field_id': self.kwargs['pk'],

            })

        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ContractorEmployeeFieldUpdate, self).get_context_data(**kwargs)
        context.update({
            'contractor_id': self.kwargs['contractor_id'],
        })
        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('contractor:contractor_detail',
                                kwargs={
                                        'companies_id': self.kwargs['companies_id'],
                                        'pk': self.kwargs['contractor_id']
                                })
        else:
            return reverse_lazy('contractor:contractor_detail', args=(self.object.id,))


# UPDATE ALL
#############
class ContractorEmployeeFieldAllUpdate(UpdateView):
    model = ContractorEmployeeField
    form_class = ContractorEmployeeFieldForm
    template_name = 'apps/contractor/contractor_employee_field/contractor_employee_field_form.html'

    def get_initial(self):
        initial = super(ContractorEmployeeFieldAllUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )
        return initial

    def get_form_kwargs(self):
        kwargs = super(ContractorEmployeeFieldAllUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
                'contractor_id': self.kwargs['contractor_id'],
                'employees_field_id': self.kwargs['pk'],

            })
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ContractorEmployeeFieldAllUpdate, self).get_context_data(**kwargs)
        context.update({
            'contractor_id': self.kwargs['contractor_id'],
        })
        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('contractor:contractor_employee_field_all_list',
                                kwargs={
                                            'pk': self.kwargs['companies_id']
                                        })
        else:
            return reverse_lazy('contractor:contractor_employee_field_all_list', args=(self.object.id,))

# LIST
# ============
class ContractorEmployeeFieldList(ListView):
    model = EmployeeField
    template_name = 'apps/contractor/contractor_employee_field/contractor_employee_field_list.html'
    success_url = reverse_lazy('contractor:employee_field_list')

    def get_context_data(self, **kwargs):
        context = super(ContractorEmployeeFieldList, self).get_context_data(**kwargs)


        lista = ContractorEmployeeField.objects.all().annotate(). \
            values('employees_field_id'). \
            filter(contractors_id=self.kwargs['pk'])

        employee_ready = EmployeeField.objects.exclude(id__in=lista).filter(is_assigned=False).order_by(
            'is_active').reverse()
        # .filter(is_active=False)

        context['EmployeeFieldList'] = employee_ready

        total_employees_field = ContractorEmployeeField.objects.all(). \
            filter(contractors_id=self.kwargs['pk']).count()

        context['contractor_employee_field_total'] = total_employees_field

        context['contractor_employee_field_info'] = Contractor.objects.get(pk=self.kwargs['pk'])

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('contractor:contractor_employee_field_list', kwargs={'pk': self.kwargs['project_id']})
        else:
            return reverse_lazy('contractor:contractor_employee_field_list', args=(self.object.id,))


class ContractorEmployeeFieldAllList(ListView):
    model = EmployeeField
    template_name = 'apps/contractor/contractor_employee_field/contractor_employee_field_all_list.html'
    success_url = reverse_lazy('contractor:employee_field_all_list')

    def get_context_data(self, **kwargs):
        context = super(ContractorEmployeeFieldAllList, self).get_context_data(**kwargs)

        contractor_list = ContractorEmployeeField.objects.all().select_related().filter(
           contractors__companies__id=self.kwargs['pk'])
        context['EmployeeFieldAllList'] = contractor_list

        total_employees_field = ContractorEmployeeField.objects.all(). \
            filter(contractors_id=self.kwargs['pk']).count()

        context['contractor_employee_field_total'] = total_employees_field

        return context


# ######################
# EMPLOYEE FIELD CRUD
########################
class EmployeeFieldCreate(CreateView):
    model = EmployeeField
    form_class = EmployeeFieldForm
    template_name = 'apps/contractor/employee_field/employee_field_form.html'

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('contractor:contractor_employee_field_list', kwargs={'pk': self.kwargs['pk'], 'companies_id': self.kwargs['companies_id']})
        else:
            return reverse_lazy('contractor:contractor_detail', args=(self.object.id,))

    def form_valid(self, form):
        last_register = ContractorEmployeeField.objects.all().filter(contractors_id=self.kwargs['pk'])
        if last_register:
            last_register = EmployeeField.objects.all().filter(
                contractoremployeefield__contractors_id=self.kwargs['pk']).latest('pk')

            correlative = int(last_register.employee_field_code[6:]) + 1
        else:
            correlative = 1

        form.instance.create_user = self.request.user
        form.save(commit=False)  # Para obtener el ID

        # Generamos el codigo
        generate_code = form.instance.first_name[:1] + form.instance.last_name[:1] + \
                        '-' + str(self.kwargs['pk']) + 'F-' + str(correlative)

        form.instance.employee_field_code = generate_code
        form.save()  # Guardamos el codigo

        return super(EmployeeFieldCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EmployeeFieldCreate, self).get_context_data(**kwargs)

        context['contractor_info'] = Contractor.objects.get(id=self.kwargs['pk'])

        context.update({'contractor_id': self.kwargs})
        return context

    def get_initial(self):
        initial = super(EmployeeFieldCreate, self).get_initial()

        initial.update(
            {
                'contractors': self.kwargs['pk'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(EmployeeFieldCreate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs


class EmployeeFieldUpdate(UpdateView):
    model = EmployeeField
    form_class = EmployeeFieldForm
    template_name = 'apps/contractor/employee_field/employee_field_form.html'

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(EmployeeFieldUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )
        return initial

    def get_form_kwargs(self):
        kwargs = super(EmployeeFieldUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_context_data(self, **kwargs):
        context = super(EmployeeFieldUpdate, self).get_context_data(**kwargs)

        context['companies_id'] = self.kwargs['companies_id']

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('contractor:contractor_employee_field_all_list', kwargs={
                'pk': self.kwargs['companies_id']
            })
        else:
            return reverse_lazy('contractor:contractor_employee_field_all_list', args=(self.object.id,))

class EmployeeFieldList(ListView):
    model = EmployeeField
    template_name = 'apps/contractor/employee_field/employee_field_list.html'
    success_url = reverse_lazy('contractor:employee_field_list')

    def get_context_data(self, **kwargs):
        context = super(EmployeeFieldList, self).get_context_data(**kwargs)

        employee_field_list = EmployeeField.objects.select_related('contractors'). \
            all().filter(contractors_id=self.kwargs['pk'])

        context['EmployeeFieldList'] = employee_field_list

        context['employee_field_info'] = Contractor.objects.get(pk=self.kwargs['pk'])

        return context


class EmployeeFieldProfile(DetailView):
    model = ContractorEmployeeField
    template_name = 'apps/contractor/contractor_employee_field/contractor_employee_field_profile.html'

    def get_context_data(self, **kwargs):
        context = super(EmployeeFieldProfile, self).get_context_data(**kwargs)

        context['EmployeeFieldList'] = ContractorEmployeeField.objects.select_related(). \
            get(contractors__id=self.kwargs['contractor_id'],
                employees_field_id=self.kwargs['pk'])

        qr_code = EmployeeField.objects.all().get(id=self.kwargs['pk'])

        if qr_code:
            context['QRCodeData'] = {
                'employee_field_code': qr_code.employee_field_code,
            }

        employee_field_projects = DailyProduce.objects. \
            values('project_contractor_employees_field__project_contractors__contractors__id',
                   'project_contractor_employees_field__project_contractors__contractors__name',
                   'project_contractor_employees_field__project_contractors__projects__id',
                   'project_contractor_employees_field__project_contractors__projects__po',
                   'project_contractor_employees_field__project_contractors__projects__name',
                   'project_contractor_employees_field__project_contractors__projects__cost',
                   'project_contractor_employees_field__project_contractors__projects__street_address',
                   'project_contractor_employees_field__project_contractors__projects__state_address',
                   'project_contractor_employees_field__project_contractors__projects__city_address',
                   'project_contractor_employees_field__project_contractors__projects__is_wage_scale',
                   'project_contractor_employees_field__project_contractors__projects__complexity',
                   'project_contractor_employees_field__project_contractors__projects__start_date',
                   'project_contractor_employees_field__project_contractors__projects__finish_date',
                   'project_contractor_employees_field__project_contractors__projects__complexity',
                   ). \
            filter(project_contractor_employees_field__contractor_employees_field__employees_field_id=self.kwargs[
            'pk']). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            production_e=Sum('project_manday_schedules__production'),
            production_p=Sum('production'),

            hours=Sum('work_hours')
        )

        context['ProjectDetailEmployee'] = employee_field_projects

        # PENDIENTE
        # data = ({
        #     'total_cost': employee_field_projects[1]['cost'],
        #     'total_works_hours': employee_field_projects[1]['hours']
        # })
        # print(data)
        #
        # context['EmployeeFieldTotal'] = data
        # print(context['EmployeeFieldTotal'])

        return context

    success_url = reverse_lazy('contractor:contractor_detail')


def get_data(request, *args, **kwargs):
    data = {
        "Man Day": 100,
        "Cost": 10,
    }
    return JsonResponse(data)  # http response


#############
# Graficar datos
class ContractorChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, *args, **kwargs):
        contractor_graph = DailyProduce.objects.values(
            'project_contractor_employees_field__project_contractors__projects__po'). \
            filter(project_contractor_employees_field__project_contractors__contractors__id=self.kwargs['pk']). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                ),
            ), hours=Sum('work_hours')).values_list(
            'project_contractor_employees_field__project_contractors__projects__po', 'cost', 'hours')
        #####

        l = []
        c = []
        h = []
        for i, item in enumerate(contractor_graph):
            label = contractor_graph[i][0]
            cost = contractor_graph[i][1]
            hours = contractor_graph[i][2] / 8
            l.append(label)
            c.append(cost)
            h.append(hours)

        # Send Data to Graph
        data = {
            "labels_graph": l,
            "default_man_days": h,
            "default_project_cost": c
        }
        return Response(data)
