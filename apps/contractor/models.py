from apps.company.models import Company, CategoryEmployeeField
import qrcode
from django.core.files.base import *

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Contractor(models.Model):
    companies = models.ForeignKey(Company, on_delete=models.CASCADE)

    name = models.CharField(max_length=100, unique=True, blank=True)
    ceo = models.CharField(max_length=100, blank=True, null=False)
    president = models.CharField(max_length=100, blank=True, null=False)

    fed = models.CharField(max_length=25, blank=True)

    email = models.CharField(max_length=100, blank=True)
    website_url = models.CharField(max_length=100, blank=True)

    phone_number = models.CharField(max_length=14, blank=True, unique=True)
    phone_number_ext = models.CharField(max_length=5, blank=True)

    country_address = models.CharField(max_length=100, blank=True)
    street_address = models.CharField(max_length=100, blank=True)
    city_address = models.CharField(max_length=75, blank=True)
    state_address = models.CharField(max_length=50, blank=True)
    zip_code_address = models.CharField(max_length=10, blank=True)

    contractor_logo = models.ImageField(upload_to='./images/contractor/logo/'.format(name), blank=True, null=True)

    summary = models.TextField(blank=True, null=True)
    notes = models.TextField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'contractor'
        verbose_name = 'contractor'
        verbose_name_plural = 'contractors'


    def __str__(self):
        return '{}'.format(self.name)


EMPLOYEE_STATES = (
    ('A', 'Permanent'),
    ('P', 'Part-timer'),
    ('T', 'Fixed-term'),
    ('E', 'External'),
    ('S', 'Summer worker'),
    ('I', 'Inactive'),
)


class EmployeeField(models.Model):
    # contractors = models.ForeignKey(Contractor)
    category_employees_field = models.ForeignKey(CategoryEmployeeField, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, blank=True)
    employee_field_code = models.CharField(max_length=100, blank=True)

    last_name = models.CharField(max_length=100, null=False)
    date_of_birth = models.DateField(max_length=100, null=True, blank=True)

    salary = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)

    email = models.CharField(max_length=100, blank=True)
    avatar = models.ImageField(upload_to='./images/employee_field/avatar/'.format(employee_field_code), blank=True, null=True)

    avg_rating = models.PositiveIntegerField(blank=True, default=0)

    cell_phone_number = models.CharField(max_length=14, blank=True, unique=True)

    country_address = models.CharField(max_length=100, blank=True)
    street_address = models.CharField(max_length=150, blank=True)
    city_address = models.CharField(max_length=100, blank=True)
    state_address = models.CharField(max_length=2, blank=True)
    zip_code_address = models.CharField(max_length=5, blank=True)

    by_direct_deposit = models.BooleanField(default=True)
    by_salary = models.BooleanField(default=True)

    notes = models.TextField(max_length=255, blank=True, null=True)

    status = models.CharField(max_length=1, blank=True, null=True, choices=EMPLOYEE_STATES, default='A')

    is_assigned = models.BooleanField(default=False)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'employee_field'
        verbose_name = 'employee_field'
        verbose_name_plural = 'employees_field'

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)


class EmployeeFieldTimeSheet(models.Model):
    employees_field = models.ForeignKey(EmployeeField, on_delete=models.CASCADE)
    check_in = models.DateTimeField(auto_now_add=True)
    check_out = models.DateTimeField(auto_now_add=True)

    notes = models.TextField(max_length=255, blank=True, null=True)

    long = models.DecimalField(max_digits=8, decimal_places=3)
    lat = models.DecimalField(max_digits=8, decimal_places=3)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'employee_field_time_sheet'
        verbose_name = 'employee_field_time_sheet'
        verbose_name_plural = 'employee_field_time_sheets'

    def __str__(self):
        return '{}'.format(self.employees_field)

class ContractorEmployeeField(models.Model):
    # OneToMany
    contractors = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    employees_field = models.ForeignKey(EmployeeField, on_delete=models.CASCADE)

    notes = models.TextField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'contractor_employee_field'
        verbose_name = 'contractor_employee_field'
        verbose_name_plural = 'contractor_employees_field'
        ordering = ['contractors__name', 'employees_field' ]

    def __str__(self):
        return '{}'.format(self.employees_field)