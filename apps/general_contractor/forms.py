from django import forms
from apps.general_contractor.models import GeneralContractor
from apps.company.models import Company

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset
from crispy_forms.bootstrap import PrependedText, Div, InlineRadios, TabHolder, Tab

US_STATES = (
    ('AL', 'Alabama'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('CA', 'California'), ('CO', 'Colorado'),
    ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'),
    ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'),
    ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'),
    ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'),
    ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'),
    ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'),
    ('PA', 'Pennsylvania'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'),
    ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VA', 'Virginia'),
    ('WA', 'Washington'),
    ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming'))

COUNTRY = (
    ('US', 'United States'), ('CAD', 'Canada'))

class GeneralContractorForm(forms.ModelForm):
    class Meta:
        model = GeneralContractor
        fields = ['companies', 'name', 'ceo', 'president', 'fed', 'email', 'website_url', 'office_phone_number', 'phone_number', 'phone_number_ext',
                  'city_address', 'country_address', 'state_address', 'zip_code_address', 'street_address', 'is_active',
                  ]

    companies = forms.Select()

    name = forms.CharField(label="Company Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'",
                                      'placeholder': 'General Contractor Name'}
                           ))

    ceo = forms.CharField(label="CEO Name:", required=True, max_length=75,
                          widget=forms.TextInput(
                              attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                     'placeholder': 'both name(s) e.g Jon Doe'}))

    president = forms.CharField(label="President Name:", required=True,
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                           'placeholder': 'both name(s) e.g Jon Doe'}))

    fed = forms.CharField(label="FED:", required=True,
                          widget=forms.TextInput(
                              attrs={'type': 'text', 'name': 'fed', 'class': 'form-control', 'required': True,
                                     'placeholder': 'FED Register'}))

    office_phone_number = forms.CharField(label="Office Phone Number:",
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control', 'data-inputmask': "'mask':'(999) 999-9999'",
                                              'placeholder': '(___) ___-____'}))

    phone_number = forms.CharField(label="Phone Number:",
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control', 'data-inputmask': "'mask':'(999) 999-9999'",
                                              'placeholder': '(___) ___-____'}))

    phone_number_ext = forms.CharField(label="Phone number ext:",
                                       widget=forms.TextInput(
                                           attrs={'type': 'number',
                                                  'class': 'form-control input-number value="1" min="1" max="10"',
                                                  'placeholder': 'Phone number ext.'}))

    street_address = forms.CharField(label="Street address:", required=False, max_length=150,
                                     widget=forms.TextInput(
                                         attrs={'type': 'text',
                                                'class': 'form-control input-number value="1" min="1" max="10"',
                                                'placeholder': 'e.g: 4422 Faroe Place'}))

    city_address = forms.CharField(label="City address:", required=False, max_length=150,
                                   widget=forms.TextInput(
                                       attrs={'type': 'text',
                                              'class': 'form-control input-number value="1" min="1" max="10"',
                                              'placeholder': 'e.g: Rockville'}))

    country_address = forms.CharField(label="Country address:", required=True, max_length=50,
                                      widget=forms.Select(choices=COUNTRY))

    state_address = forms.CharField(label="State address:", required=True, max_length=50,
                                    widget=forms.Select(choices=US_STATES))

    zip_code_address = forms.CharField(label="zip code address:", required=True, max_length=10,
                                       widget=forms.TextInput(
                                           attrs={'type': 'text', 'class': 'form-control',
                                                  'placeholder': 'e.g: 20850'}))

    email = forms.EmailField(label="email:", required=False,
                             widget=forms.TextInput(
                                 attrs={'type': 'email', 'required': True, 'placeholder': 'jon.doe@example.com'}))

    website_url = forms.URLField(initial='http://', label="Website URL:", required=False,
                                 widget=forms.TextInput(
                                     attrs={'type': 'url', 'class': 'form-control',
                                            'placeholder': 'http://www.example.com'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    def __init__(self, companies_id, *args, **kwargs):
        super(GeneralContractorForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-general-contractor-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if companies_id:
            self.helper.form.fields['companies'].queryset = Company.objects.filter(id=companies_id)

        self.helper.layout = Layout(
            Fieldset('<i class="fa fa-info-circle"></i> General Information',
                     Div(
                         PrependedText('companies', ''),
                         PrependedText('name', '<i class="glyphicon glyphicon-home"></i>'),
                         PrependedText('ceo', '<i class="glyphicon glyphicon-user"></i>'),

                         css_class='col-md-6'
                     ),

                     Div(
                         PrependedText('president', '<i class="glyphicon glyphicon-user"></i>'),
                         PrependedText('fed', '<i class="glyphicon glyphicon-asterisk"></i>'),

                         css_class='col-md-6'
                     ),
                     ),

            Fieldset('<i class="fa fa-info-circle"></i> Contact Information',
                     Div(
                         PrependedText('office_phone_number', '<i class="glyphicon glyphicon-phone-alt"></i>',
                                       placeholder='(___) ___-____'),

                         PrependedText('phone_number', '<i class="glyphicon glyphicon-phone-alt"></i>',
                                       placeholder='(___) ___-____'),

                         PrependedText('phone_number_ext', '<i class="glyphicon glyphicon-phone-alt"></i>'),

                         css_class='col-md-6'
                     ),

                     Div(

                         PrependedText('email', '<i class="glyphicon glyphicon-envelope"></i>'),
                         PrependedText('website_url', '<i class="glyphicon glyphicon-link"></i>'),

                         css_class='col-md-6'
                     ),
                     ),


            Fieldset('<i class="fa fa-map-marker"></i> Location Information',
                     Div(
                         PrependedText('country_address', ''),
                         PrependedText('street_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         PrependedText('city_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         css_class='col-md-6'
                     ),
                     Div(
                         PrependedText('state_address', ''),
                         PrependedText('zip_code_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         PrependedText('is_active', ''),
                         css_class='col-md-6'
                     ),
            )

        )
