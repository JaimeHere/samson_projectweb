from django.contrib import admin

# Register your models here.
from apps.general_contractor.models import GeneralContractor
admin.site.register(GeneralContractor)
