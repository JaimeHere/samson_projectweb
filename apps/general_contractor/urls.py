"""
URLs Company
"""
from django.contrib.auth.decorators import login_required

from django.conf.urls import url
from apps.general_contractor.views import index, GeneralContractorCreate, GeneralContractorList, GeneralContractorUpdate, GeneralContractorDelete

urlpatterns = [
    url(r'^$', index, name='index'),

    url(r'^(?P<pk>\d+)/create$', login_required(GeneralContractorCreate.as_view()), name='gc_create'),
    url(r'^(?P<pk>\d+)/list$', login_required(GeneralContractorList.as_view()), name='gc_list'),
    url(r'^(?P<companies_id>\d+)/update/(?P<pk>\d+)/$', login_required(GeneralContractorUpdate.as_view()), name='gc_update'),
    url(r'^(?P<companies_id>\d+)/delete/(?P<pk>\d+)/$', login_required(GeneralContractorDelete.as_view()), name='gc_delete'),

]


