from django.db import models
from apps.company.models import Company
from django.contrib.auth.models import User

# Create your models here.

class GeneralContractor(models.Model):
    companies = models.ForeignKey(Company, on_delete=models.CASCADE)

    name = models.CharField(max_length=100, unique=True, blank=True)
    ceo = models.CharField(max_length=100, blank=True, null=False)
    president = models.CharField(max_length=100, blank=True, null=False)

    fed = models.CharField(max_length=25, blank=True)

    email = models.CharField(max_length=100, blank=True)
    website_url = models.CharField(max_length=100, blank=True)

    phone_number = models.CharField(max_length=14, blank=True)

    office_phone_number = models.CharField(max_length=14, blank=True)
    phone_number_ext = models.CharField(max_length=5, blank=True)

    country_address = models.CharField(max_length=100, blank=True)
    street_address = models.CharField(max_length=100, blank=True)
    city_address = models.CharField(max_length=75, blank=True)
    state_address = models.CharField(max_length=50, blank=True)
    zip_code_address = models.CharField(max_length=10, blank=True)

    general_contractor_logo = models.ImageField(upload_to='./static/images/general_contractor_logo/'.format(name), blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'general_contractor'

        verbose_name = 'general_contractor'
        verbose_name_plural = 'general_contractors'


    def __str__(self):
        return '{}'.format(self.name)

