from django.shortcuts import render, redirect
from apps.general_contractor.forms import GeneralContractorForm
from apps.general_contractor.models import GeneralContractor
from apps.company.models import Company
from apps.project.models import Project

from django.urls import reverse_lazy

from django.views.generic import CreateView, ListView, UpdateView, DeleteView


# Create your views here.
def index(request):
    return render(request, 'base/index.html')

# Create your views here.
class GeneralContractorCreate(CreateView):
    model = GeneralContractor
    form_class = GeneralContractorForm
    template_name = 'apps/general_contractor/general_contractor_form.html'
    success_url = reverse_lazy('general_contractor:gc_list')

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(GeneralContractorCreate, self).form_valid(form)


    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(GeneralContractorCreate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        initial.update(
            {
                'companies_id': companies_id['id'],
                'companies': companies_id['id'],

            }
        )
        return initial

    def get_form_kwargs(self):
        kwargs = super(GeneralContractorCreate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        print(self.kwargs)

        if kwargs != None:
            return reverse_lazy('general_contractor:gc_list', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('general_contractor:gc_list', args=(self.object.id,))


class GeneralContractorList(ListView):
    model = GeneralContractor
    template_name = 'apps/general_contractor/general_contractor_list.html'

    def get_context_data(self, **kwargs):
        context = super(GeneralContractorList, self).get_context_data(**kwargs)

        general_contractor_list = GeneralContractor.objects.all().filter(companies_id=self.kwargs['pk'])

        context['GeneralContractorList'] = general_contractor_list

        return context

class GeneralContractorUpdate(UpdateView):
    model = GeneralContractor
    form_class = GeneralContractorForm
    template_name = 'apps/general_contractor/general_contractor_form.html'
    success_url = reverse_lazy('general_contractor:gc_list')

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(GeneralContractorUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(GeneralContractorUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        print(self.kwargs)

        if kwargs != None:
            return reverse_lazy('general_contractor:gc_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('general_contractor:gc_list', args=(self.object.id,))


class GeneralContractorDelete(DeleteView):
    model = GeneralContractor
    template_name = 'apps/general_contractor/general_contractor_delete.html'

    def get_context_data(self, **kwargs):

        context = super(GeneralContractorDelete, self).get_context_data(**kwargs)

        context['DeleteRegisterExist'] = Project.objects.filter(companies_id=self.kwargs['companies_id'], general_contractors_id=self.kwargs['pk'])
        context['DeleteRegister'] = GeneralContractor.objects.get(companies_id=self.kwargs['companies_id'], pk=self.kwargs['pk'])

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('general_contractor:gc_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('general_contractor:gc_list', args=(self.object.id,))