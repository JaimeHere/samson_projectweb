from django.apps import AppConfig


class CompanyConfig(AppConfig):
    name = 'general_contractor'
