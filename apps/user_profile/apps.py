from django.apps import AppConfig


class CompanyConfig(AppConfig):
    name = 'user_profile'
