# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-06-14 15:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0003_userproject'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userproject',
            options={'verbose_name': 'user_project', 'verbose_name_plural': 'user_projects'},
        ),
    ]
