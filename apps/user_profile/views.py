from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy

from apps.user_profile.forms import UserProfileForm

from django.views.generic import CreateView, View, UpdateView, ListView

from apps.user_profile.models import UserProfile, UserProject
from apps.user_profile.forms import SignUpForm
from apps.user_profile.models import UserProfile

from django.contrib.auth.models import User


class SignUpView(CreateView):
    model = UserProfile
    form_class = SignUpForm
    template_name = 'apps/user_profile/user_form.html'

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        username = authenticate(username=username, password=password)

        login(self.request, username)

        # Saltar a profile futuro

        return redirect('/')
        # return redirect('apps/user_profile/perfil_form.html')

class UserProfileList(ListView):
    model = UserProfile
    template_name = 'apps/User/pending.html'

    def get_context_data(self, **kwargs):
        context = super(UserProfileList, self).get_context_data(**kwargs)

        context['user_information'] = UserProfile.objects.get(username_id=self.kwargs['pk'])

        return context


class UserProfiles(UpdateView):
    model = UserProfile
    form_class = UserProfileForm
    template_name = 'apps/user_profile/profile_form.html'

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(UserProfiles, self).get_initial()

        #companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies': self.kwargs['companies_id'],
                'username': self.kwargs['pk']
            }
        )
        return initial

    def get_form_kwargs(self):
        kwargs = super(UserProfiles, self).get_form_kwargs()

        kwargs.update(
            {
                'companies_id': self.kwargs['companies_id'],
                'username_id': self.kwargs['pk']
            }
        )

        return kwargs


    def get_success_url(self, **kwargs):
        if kwargs != None:
            a = UserProject.objects.all()
            return reverse_lazy('company:company_dashboard',
                                              kwargs={
                                                  'pk':self.kwargs['companies_id'],
                                              })
        else:
            return reverse_lazy('company:company_dashboard', args=(self.object.id,))


class WelcomeView(View):

    def get(self, request, *args, **kwargs):
        print('===================================')



class UserList(ListView):
    model = User
    template_name = 'apps/user_profile/user/user_list.html'

    def get_context_data(self, **kwargs):
        context = super(UserList, self).get_context_data(**kwargs)

        context['userList'] = UserList.objects.all()

        return context
