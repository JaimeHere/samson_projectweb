"""
URLs Company
"""
from django.conf.urls import url
#from apps.user_profile.views import SignUpView, SignInView, SignOutView, WelcomeView
from apps.user_profile.views import SignUpView, UserProfiles

urlpatterns = [
    url(r'^sing_up/$', SignUpView.as_view(), name='sign_up'), # Register
    url(r'^(?P<companies_id>\d+)/profile/(?P<pk>\d+)/$', UserProfiles.as_view(), name='profile'),  # Register

]
