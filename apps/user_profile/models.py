from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from apps.company.models import Company
from apps.project.models import Project

class UserProfile(models.Model):
    ADMIN=1
    STAFF=2
    SUPERINTENDENT=3
    PME=4
    FOREMAN=5
    PE=6

    ROLE_CHOICES = (
        (ADMIN, 'Administrator'),
        (STAFF, 'Staff'),
        (SUPERINTENDENT, 'Superintendent'),
        (PME, 'Project Manager'),
        (FOREMAN, 'Foreman'),
    )

    companies = models.ForeignKey(Company, null=True, on_delete=models.CASCADE)
    username = models.OneToOneField(User, on_delete=models.CASCADE)

    role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, null=True, blank=True)
    avatar = models.ImageField(upload_to='./images/user_profile/avatar/'.format(username), blank=True, null=True)

    class Meta:
        db_table = 'user_profile'
        verbose_name = 'user_profile'
        verbose_name_plural = 'user_profiles'

    def __str__(self):
        return '{}'.format(self.username)

@receiver(post_save, sender=User)
def create_user_profielepa(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(username=instance)

# @receiver(post_save, sender=User)
# def guardar_usuario_perfil(sender, instance, **kwargs):
#     instance.user_perfil.save()

class UserProject(models.Model):
    # Relations OneToMany
    projects = models.ForeignKey(Project, on_delete=models.CASCADE)
    userprofile = models.ForeignKey(UserProfile, on_delete=models.CASCADE)

    notes = models.TextField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'user_project'
        verbose_name = 'user_project'
        verbose_name_plural = 'user_projects'

    def __str__(self):
        return '{} - {}'.format(self.userprofile, self.projects)