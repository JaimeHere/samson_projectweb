from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset, HTML
# from crispy_forms.bootstrap import PrependedText, Div, InlineRadios, TabHolder, Tab
from crispy_forms.bootstrap import PrependedText, Div, FormActions
from apps.user_profile.models import UserProfile
from apps.company.models import Company

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=140, required=True)
    last_name = forms.CharField(max_length=140, required=False)
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
        )

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-contractor-form'
        self.helper.form_method = 'POST'
        # self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        self.helper.layout = Layout(
            Div(
                PrependedText('username', ''),
                PrependedText('first_name', ''),
                PrependedText('last_name', ''),
                PrependedText('email', ''),
                PrependedText('password1', ''),
                PrependedText('password2', ''),

                css_class='col-md-12'
            ),
            FormActions(

                Submit('save', 'Save changes'),
            ),
        )


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = (
            'companies',
            'username',
            'role',
            'avatar',
        )
    avatar = forms.ImageField(required=False, label="avatar")


    def __init__(self, companies_id, username_id, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-contractor-form'
        self.helper.form_method = 'POST'

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        if companies_id:
            self.helper.form.fields['companies'].queryset = Company.objects.filter(id=companies_id)
            self.helper.form.fields['username'].queryset = User.objects.filter(id=username_id)

        self.helper.layout = Layout(
            Fieldset('',
                     Fieldset('',

            Div(
                # PrependedText('first_name', ''),
                # PrependedText('last_name', ''),
                # PrependedText('email', ''),
                PrependedText('companies', ''),
                PrependedText('username', ''),
                PrependedText('role', ''),
                PrependedText('avatar', ''),

                css_class='col-md-12'
            ),
                              ),
                     ),
            FormActions(

                Submit('save', 'Save changes'),
            )
        )