from django.contrib import admin
from django.contrib.auth.models import User
from apps.user_profile.models import UserProfile, UserProject


# Register your models here.

class UserProfileAdmin(admin.ModelAdmin):
    # list_display = ('companies', 'username', 'role', 'avatar')
    list_display = ('username')


# admin.site.unregister(User)
admin.site.register(UserProfile)
admin.site.register(UserProject)
