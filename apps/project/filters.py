from django import forms
from apps.project.models import Project, DailyProduce

import django_filters

class ProjectFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')
    po = django_filters.NumberFilter(name='date_joined', lookup_expr='year')
    list = django_filters.ModelMultipleChoiceFilter(queryset=DailyProduce.objects.all(), widget=forms.CheckboxSelectMultiple)

    class Meta:
        model = Project
        fields = ['name']