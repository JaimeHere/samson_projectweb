# from crispy_forms.bootstrap import PrependedText, Div, InlineRadios, TabHolder, Tab
from crispy_forms.bootstrap import PrependedText, Div, FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset
from django import forms

from apps.company.models import Company, CategoryProject
from apps.contractor.models import ContractorEmployeeField
from apps.employee.models import Employee
from apps.general_contractor.models import GeneralContractor
from apps.project.models import Project, DailyProduce, ProjectEmployee, ProjectContractor, ProjectManDaySchedule, \
    ProjectContractorEmployeeField, CostCode, ProjectFloorPhase, ProjectFloorPhaseArea, ProjectFile

from bootstrap_datepicker_plus import DatePickerInput, TimePickerInput, DateTimePickerInput, MonthPickerInput, YearPickerInput

from django import forms



US_STATES = (
    ('AL', 'Alabama'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('CA', 'California'), ('CO', 'Colorado'),
    ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'),
    ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'),
    ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'),
    ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'),
    ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'),
    ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'),
    ('PA', 'Pennsylvania'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'),
    ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VA', 'Virginia'),
    ('WA', 'Washington'),
    ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming'))

COUNTRY = (
    ('US', 'United States'), ('CAD', 'Canada'))

class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project

        fields = ['companies', 'general_contractors', 'category_projects', 'name', 'cost', 'email', 'website_url',
                  'phone_number', 'phone_number_ext', 'po', 'start_date', 'finish_date', 'num_stars', 'complexity', 'logo',
                  'country_address', 'street_address', 'city_address', 'state_address', 'summary', 'notes',
                  'zip_code_address', 'is_active', 'is_wage_scale']

    name = forms.CharField(label="Project name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text',
                                      'class': 'form-control',
                                      'required': True,
                                      'data-validate-length-range': "'6'",
                                      'data-validate-words': "'2'", 'placeholder': 'Project Name'}
                           ))
    po = forms.CharField(label="PO #:", required=True, max_length=150,
                         widget=forms.TextInput(
                             attrs={'type': 'text',
                                    'class': 'form-control input-number value="1" min="1" max="10"',
                                    'placeholder': 'PO Number'}))

    cost = forms.DecimalField(label="Project Cost", max_digits=12, decimal_places=2, required=True,
                              widget=forms.TextInput(
                                  attrs={'type': 'text'}
                              ))

    email = forms.EmailField(label="Company email:", required=False,
                             widget=forms.TextInput(
                                 attrs={'type': 'email', 'required': True, 'placeholder': 'jon.doe@example.com'}))

    phone_number = forms.CharField(label="Phone Number:", max_length=14,
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control', 'data-inputmask': "'mask':'(999) 999-9999'",
                                              'placeholder': '(___) ___-____'}))

    phone_number_ext = forms.CharField(label="Phone number ext:", max_length=5,
                                       widget=forms.TextInput(
                                           attrs={'type': 'text',
                                                  'class': 'form-control', 'placeholder': 'Phone number ext.'}))



    start_date = forms.CharField(label="Street address:", required=True,
                                     widget=forms.TextInput(
                                         attrs={'type': 'text', 'id':'start_date'}))


    finish_date = forms.CharField(label="Street address:", required=True,
                                     widget=forms.TextInput(
                                         attrs={'type': 'text', 'id':'finish_date'}))

    street_address = forms.CharField(label="Street address:", required=False, max_length=150,
                                     widget=forms.TextInput(
                                         attrs={'type': 'text',
                                                'class': 'form-control input-number value="1" min="1" max="10"',
                                                'placeholder': 'e.g: 4422 Faroe Place'}))

    country_address = forms.CharField(label="Country address:", required=True, max_length=100,
                                      widget=forms.Select(choices=COUNTRY))

    city_address = forms.CharField(label="City address:", required=False, max_length=100,
                                   widget=forms.TextInput(
                                       attrs={'type': 'text',
                                              'class': 'form-control input-number value="1" min="1" max="10"',
                                              'placeholder': 'e.g: Rockville'}))

    state_address = forms.CharField(label="State address:", required=True, max_length=2,
                                    widget=forms.Select(choices=US_STATES))

    zip_code_address = forms.CharField(label="zip code address:", required=True, max_length=5,
                                       widget=forms.TextInput(
                                           attrs={'type': 'text', 'class': 'form-control',
                                                  'placeholder': 'e.g: 20850'}))

    website_url = forms.URLField(initial='http://', label="Website URL:", required=False,
                                 widget=forms.TextInput(
                                     attrs={'type': 'url', 'class': 'form-control'}
                                 ))

    summary = forms.CharField(label="Summary:", required=False,
                              widget=forms.Textarea(
                                  attrs={'type': 'text', 'class': 'form-control',
                                         'placeholder': 'Summary About this Project'}))

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'About this Project'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}
                                ))

    is_wage_scale = forms.CharField(label="Regular Project/Activate WAGE SCALE", required=False,
                                    widget=forms.CheckboxInput(
                                        attrs={'class': 'js-switch'}
                                    ))

    def __init__(self, companies_id, user_id,  *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if companies_id:
            self.helper.form.fields['companies'].queryset = Company.objects.filter(id=companies_id)
            self.helper.form.fields['category_projects'].queryset = CategoryProject.objects.filter(companies_id=companies_id)
            self.helper.form.fields['general_contractors'].queryset = GeneralContractor.objects.filter(companies_id=companies_id)

        self.helper.layout = Layout(
            Fieldset('<strong>General Information</strong>',
                     Div(
                         PrependedText('companies', ''),
                         PrependedText('category_projects', ''),
                         PrependedText('general_contractors', ''),
                         css_class='col-md-6'
                     ),
                     Div(
                         PrependedText('name', '<i class="glyphicon glyphicon-home"></i>'),
                         PrependedText('po', '<i class="fa fa-bookmark"></i>'),
                         PrependedText('cost', '$', '.00'),

                         css_class='col-md-6'
                     ),
                     ),

            Fieldset('<strong>Other General Information</strong>',
                     Div(
                         PrependedText('start_date', '<i class="glyphicon glyphicon-calendar"></i>'),
                         PrependedText('finish_date', '<i class="glyphicon glyphicon-calendar"></i>'),
                         css_class='col-md-6'
                     ),
                     Div(

                         PrependedText('num_stars', ''),
                         PrependedText('complexity', ''),
                         css_class='col-md-6'
                     ),
                     ),

            Fieldset('<strong>Contact Information</strong>',
                     Div(
                         PrependedText('email', '<i class="glyphicon glyphicon-envelope"></i>'),
                         PrependedText('website_url', '<i class="glyphicon glyphicon-link"></i>'),

                         css_class='col-md-6'
                     ),
                     Div(
                         PrependedText('phone_number', '<i class="glyphicon glyphicon-phone-alt"></i>'),
                         PrependedText('phone_number_ext', '<i class="glyphicon glyphicon-phone-alt"></i>'),
                         css_class='col-md-6'
                     ),

                     ),

            Fieldset('<br><i class="glyphicon glyphicon-map-marker"></i> <strong>Location Information</strong>',
                     Div(

                         PrependedText('street_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         PrependedText('city_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         PrependedText('state_address', ''),

                         css_class='col-md-6'
                     ),
                     Div(
                         PrependedText('zip_code_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         PrependedText('country_address', ''),

                         css_class='col-md-6'
                     ),
                     ),

            Fieldset('<i class="fa fa-align-left"></i> Summary of Project',
                     Div(
                         PrependedText('summary', '<i class="fa fa-align-left"></i>'),
                         css_class='col-md-12'
                     ),
                     ),

            Fieldset('<i class="fa fa-align-left"></i> Notes Information',
                     Div(
                         PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                         PrependedText('is_active', ''),
                         PrependedText('is_wage_scale', ''),
                         css_class='col-md-12'
                     ),
                     ),
        )


class DailyProduceForm(forms.ModelForm):

    class Meta:
        model = DailyProduce

        fields = [
            'project_contractor_employees_field', 'project_manday_schedules', 'created_at', 'work_hours',
            'production','type_of_work', 'notes',
        ]

    widgets = {
        'created_at': DatePickerInput().start_of('event days'),
    }


    project_contractor_employee_fields = forms.Select()

    project_manday_schedules = forms.Select()

    created_at =  forms.DateTimeField(widget=DateTimePickerInput())

    work_hours = forms.DecimalField(label="Hours", max_digits=4, decimal_places=2, required=True,
                                    widget=forms.TextInput(
                                        attrs={'type': 'number', 'class': 'form-control input-number value="1" min="1" max="16"', 'placeholder': 'Hours worked'}
                                    ))


    production = forms.DecimalField(label="Production", max_digits=8, decimal_places=2, required=True,
                                    widget=forms.TextInput(
                                        attrs={'type': 'text', 'class': 'form-control input-number value="1" min="1" max="16"',
                                               'placeholder': 'Production worked'}
                                    ))

    type_of_work = forms.Select()

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control',
                                       'placeholder': 'Notes About this Labor.'}))


    def __init__(self, project_id, *args, **kwargs):
        super(DailyProduceForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-daily-produce-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if project_id:
            self.helper.form.fields['project_contractor_employees_field'].queryset = ProjectContractorEmployeeField.objects.all().filter(
                project_contractors__projects_id=project_id)
            self.helper.form.fields['project_manday_schedules'].queryset = ProjectManDaySchedule.objects.filter(projects=project_id).order_by('code_labor')

        self.helper.layout = Layout(
            Fieldset('',
                     Div(
                         PrependedText('project_contractor_employees_field', ''),
                         PrependedText('project_manday_schedules', ''),
                         PrependedText('created_at', ''),
                         PrependedText('work_hours', '<i class="fa fa-clock-o"></i>'),
                         PrependedText('production', '<i class="fa fa-gear"></i>'),
                         PrependedText('type_of_work', ''),
                         css_class='col-md-12'
                     ),
                     ),
            Fieldset('',
                     Div(
                         PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                         css_class='col-md-12'
                     ),
                     ),
        )


class DailyProduceUpdateForm(forms.ModelForm):

    class Meta:
        model = DailyProduce
        fields = [
            'project_contractor_employees_field', 'project_manday_schedules', 'created_at', 'work_hours',
            'production','type_of_work', 'notes',
        ]

    project_contractor_employee_fields = forms.Select()

    project_manday_schedules = forms.Select()

    created_at =  forms.DateTimeField(widget=DateTimePickerInput())


    work_hours = forms.DecimalField(label="Hours:",
                                    widget=forms.TextInput(
                                        attrs={'type': 'number', 'lang':"en-150",
                                               'class': 'form-control input-md min="0" max="100" value="0.0" step="0.1"',
                                               'placeholder': 'Hours worked'}))

    production = forms.CharField(label="Production:",
                                 widget=forms.TextInput(
                                     attrs={'type': 'number',
                                            'class': 'form-control input-md min="0" value="0.0" step="0.01"',
                                            'placeholder': 'Production employee.'}))

    type_of_work = forms.Select()

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control',
                                       'placeholder': 'Notes About this Labor.'}))


    def __init__(self, project_manday_schedules, project_contractor_employees_field, unit, production, *args, **kwargs):
        super(DailyProduceUpdateForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-daily-produce-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit',
                                     css_class='btn-success btn btn-default source',
                                     onclick="new PNotify({title: 'Successfully saved.',\
                                        text: 'The current record was saved successfully!',\
                                        type: 'success',styling: 'bootstrap3'});"))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if project_manday_schedules:
            self.helper.form.fields['project_contractor_employees_field'].queryset = ProjectContractorEmployeeField.objects.filter(id=project_contractor_employees_field)
            self.helper.form.fields['project_manday_schedules'].queryset = ProjectManDaySchedule.objects.filter(id=project_manday_schedules)
            self.helper.form.fields['production'].widget.attrs['placeholder'] = unit

        self.helper.layout = Layout(
            Fieldset('',
                     Div(
                         PrependedText('project_contractor_employees_field', ''),
                         PrependedText('project_manday_schedules', ''),
                         # PrependedText('today', ''),
                         # PrependedText('week', ''),
                         PrependedText('created_at', ''),
                         PrependedText('work_hours', '<i class="fa fa-clock-o"></i>'),
                         PrependedText('production', '<i class="fa fa-gear"></i> '+' '+production+ ' '+unit),
                         PrependedText('type_of_work', ''),
                         css_class='col-md-12'
                     ),

                     ),
            Fieldset('',
                     Div(
                         PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                         css_class='col-md-12'
                     ),
                     ),
        )


# ========================
# Form: ProjectEmployeeCreate
# ========================

class ProjectEmployeeCreateForm(forms.ModelForm):
    class Meta:
        model = ProjectEmployee
        fields = ['projects', 'employees', 'notes', 'is_leader', 'is_active', ]

    projects = forms.Select()
    employees = forms.Select()

    notes = forms.CharField(label="NOTES About the activities to be carried out for the project:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'About this Employee'}))

    is_leader = forms.CharField(label="Not Leader/Leader", required=False,
                                widget=forms.CheckboxInput(
                                    attrs={'type': "checkbox", 'class': "js-switch", 'style': "display: none;",
                                           'data-switchery': "true"}
                                ))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}
                                ))

    def __init__(self,  companies_id, projects_id, employees, employees_id, *args, **kwargs):
        super(ProjectEmployeeCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-team-member-project-form'
        self.helper.form_method = 'POST'

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if projects_id and  companies_id:
            self.helper.form.fields['projects'].queryset = Project.objects.filter(id=projects_id)
            self.helper.form.fields['employees'].queryset = Employee.objects.filter(id=employees_id)

        self.helper.layout = Layout(
            Fieldset('',
                     Div(

                         PrependedText('projects', ''),
                         PrependedText('employees', ''),
                         css_class='col-md-12'
                     ),
                     ),

            Fieldset('',
                     Div(
                         PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                         PrependedText('is_leader', ''),
                         PrependedText('is_active', ''),
                         css_class='col-md-12'
                     ),
                     ),
            FormActions(

                Submit('save', 'Save changes'),
            ),
        )


# ========================
# Form: ProjectEmployee
# ========================

class ProjectEmployeeUpdateForm(forms.ModelForm):
    class Meta:
        model = ProjectEmployee
        fields = ['projects', 'employees', 'notes', 'is_leader', 'is_active', ]

    projects = forms.Select()
    employees = forms.Select()

    notes = forms.CharField(label="NOTES About the activities to be carried out for the project:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'About this Employee'}))

    is_leader = forms.CharField(label="Not Leader/Leader", required=False,
                                widget=forms.CheckboxInput(
                                    attrs={'type': "checkbox", 'class': "js-switch", 'style': "display: none;",
                                           'data-switchery': "true"}
                                ))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}
                                ))

    def __init__(self, companies_id, projects_id, employees, employees_id, *args, **kwargs):
        super(ProjectEmployeeUpdateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-team-member-project-form'
        self.helper.form_method = 'POST'

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'


        # if projects_id and  companies_id:
        #     self.helper.form.fields['projects'].queryset = Project.objects.filter(id=projects_id)
        if 'initial' in kwargs:
            # self.helper.form.fields['employees'].queryset = ProjectEmployee.objects.filter(id=self.instance.companies_id)
            self.helper.form.fields['projects'].queryset = Project.objects.filter(id=projects_id)

        self.helper.layout = Layout(
            Fieldset('',
                     Div(

                         PrependedText('projects', ''),
                         PrependedText('employees', ''),
                         css_class='col-md-12'
                     ),
                     ),

            Fieldset('',
                     Div(
                         PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                         PrependedText('is_leader', ''),
                         PrependedText('is_active', ''),
                         css_class='col-md-12'
                     ),
                     ),
            FormActions(

                Submit('save', 'Save changes'),
            ),
        )

        # self.fields['projects'].widget.attrs['disabled'] = True
        # self.fields['projects'].widget.attrs['required'] = False

        # self.fields['employees'].widget.attrs['disabled'] = True

# ========================
# Form: ProjectContractor
# ========================

class ProjectContractorForm(forms.ModelForm):
    class Meta:
        model = ProjectContractor
        fields = ['projects', 'contractors', 'notes', 'is_active', ]

    projects = forms.Select()

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control',
                                       'placeholder': 'Notes About this Assing Contractor to Project'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}
                                ))

    def __init__(self, projects_id, companies_id, contractors_id, *args, **kwargs):

        super(ProjectContractorForm, self).__init__(*args, **kwargs)
        # companies_id, projects_id, contractors_id
        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-contractor-form'
        self.helper.form_method = 'POST'
        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        self.helper.layout = Layout(
            Fieldset('',
                     Fieldset('',
                              Div(
                                  PrependedText('projects', ''),
                                  PrependedText('contractors', ''),
                                  css_class='col-md-12'
                              )),
                     Fieldset('',
                              Div(
                                  PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                                  PrependedText('is_active', ''),
                                  css_class='col-md-12'
                              ),
                              ),
                     FormActions(

                         Submit('save', 'Save changes'),
                     ),
                     )
        )


# ===============================
# Form: Project Man Day Schedule
# ===============================

class ProjectManDayScheduleForm(forms.ModelForm):
    class Meta:
        model = ProjectManDaySchedule

        fields = ['projects', 'cost_codes', 'project_floor_phases', 'name', 'unit', 'code_labor', 'code_material',
                  'production', 'man_days_allotted', 'regular_cost', 'overtime_cost', 'wage_cost', 'wage_overtime_cost', 'notes',
                  'is_active',
                  ]

    cost_codes = forms.Select()

    name = forms.CharField(label="Cost Code Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'",
                                      'placeholder': 'Cost Code Name'}
                           ))

    unit = forms.CharField(label="Unit:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'placeholder': 'code labor name(s) e.g 111100.000.1'}))

    code_labor = forms.CharField(label="Code Labor:", required=True, max_length=100,
                                 widget=forms.TextInput(
                                     attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                            'placeholder': 'code labor name(s) e.g 111100.000.1',
                                            'data-inputmask': "'mask':'99999.999.9'"}))

    code_material = forms.CharField(label="Code Material:", required=True, max_length=100,
                                    widget=forms.TextInput(
                                        attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                               'placeholder': 'code labor name(s) e.g 111100.000.2',
                                               'data-inputmask': "'mask':'99999.999.9'"}))

    production = forms.CharField(label="Production:", required=True, max_length=25,
                                 widget=forms.TextInput(
                                     attrs={'type': 'number', 'name': 'fed', 'class': 'form-control', 'required': True,
                                            'placeholder': 'Producion daily per Man Day'}))

    man_days_allotted = forms.CharField(label="Man Days Allotted:", required=True, max_length=25,
                                        widget=forms.TextInput(
                                            attrs={'type': 'number', 'name': 'fed', 'class': 'form-control',
                                                   'required': True,
                                                   'placeholder': 'Man Days Allotted for this Floor'}))

    regular_cost = forms.CharField(label="Regular cost:", required=False, max_length=100,
                                   widget=forms.TextInput(
                                       attrs={'type': 'text',
                                              'class': 'form-control input-number value="1" min="1" max="10"',
                                              'placeholder': 'Regular cost'}))

    overtime_cost = forms.CharField(label="Overtime cost:", required=False, max_length=100,
                                    widget=forms.TextInput(
                                        attrs={'type': 'text',
                                               'class': 'form-control input-number value="1" min="1" max="10"',
                                               'placeholder': 'Overtime cost'}))

    wage_cost = forms.CharField(label="Wage cost:", required=True, max_length=5,
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'Wage cost'}))

    wage_overtime_cost = forms.CharField(label="Wage overtime cost:", required=False, max_length=100,
                                         widget=forms.TextInput(
                                             attrs={'type': 'number', 'required': True,
                                                    'placeholder': 'Wage overtime cost'}))

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'About this code cost'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    def __init__(self, action, project_id, project_floor_phases_id, cost_codes_id, *args, **kwargs):
        super(ProjectManDayScheduleForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-man-day-schedule-form'
        self.helper.form_method = 'POST'

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if action == 'create':
            self.helper.form.fields['projects'].queryset = Project.objects.all().filter(id=project_id)
            self.helper.form.fields['project_floor_phases'].queryset = ProjectFloorPhase.objects.all().filter(
                projects_id=project_id)
            self.helper.form.fields['cost_codes'].queryset = CostCode.objects.all().filter(id=cost_codes_id)

        else:
            self.helper.form.fields['projects'].queryset = Project.objects.all().filter(id=project_id)
            self.helper.form.fields['project_floor_phases'].queryset = ProjectFloorPhase.objects.all().filter(
                id=project_floor_phases_id, projects_id=project_id)
            self.helper.form.fields['cost_codes'].queryset = CostCode.objects.all().filter(id=cost_codes_id)


        self.helper.layout = Layout(
            Fieldset('<i class="fa fa-info-circle"></i> General Information <!-- {{ user.perfil.companies.id }} -->',
                     Div(
                         PrependedText('projects', ''),
                         PrependedText('cost_codes', ''),
                         PrependedText('name', '<i class="fa fa-gear"></i>'),
                         PrependedText('unit', '<i class="fa fa-gear"></i>'),
                         PrependedText('code_labor', '<i class="glyphicon glyphicon-asterisk"></i>'),
                         PrependedText('code_material', '<i class="glyphicon glyphicon-asterisk"></i>'),
                         css_class='col-md-6'
                     ),

                     Div(

                         PrependedText('project_floor_phases', ''),
                         PrependedText('man_days_allotted', '<i class="fa fa-gear"></i>'),
                         PrependedText('production', '<i class="fa fa-gear"></i>'),

                         css_class='col-md-6'
                     ),
                     ),

            Fieldset('<i class="fa fa-calculator"></i> Cost Information',
                     Div(
                         PrependedText('regular_cost', '<i class="fa fa-dollar"></i>'),
                         PrependedText('overtime_cost', '<i class="fa fa-dollar"></i>'),
                         css_class='col-md-6'
                     ),
                     Div(
                         PrependedText('wage_cost', '<i class="fa fa-dollar"></i>'),
                         PrependedText('wage_overtime_cost', '<i class="fa fa-dollar"></i>'),

                         css_class='col-md-6'
                     ),
                     ),

            Fieldset('<i class="fa fa-align-left"></i> Notes Information',
                     Div(
                         PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                         PrependedText('is_active', ''),
                         css_class='col-md-12'
                     ),
                     ),
            FormActions(

                Submit('save', 'Save changes'),

            ),
        )


# ================================== #
# Form: Project Contractor Employee  #
# ================================== #

class ProjectContractorEmployeeFieldForm(forms.ModelForm):
    class Meta:
        model = ProjectContractorEmployeeField
        fields = ['project_contractors', 'contractor_employees_field', 'notes', 'is_active', ]

    project_contractors = forms.Select()
    contractor_employees_field = forms.Select()

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control',
                                       'placeholder': 'Notes about the relationship between the employee and the contractor.'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}
                                ))

    def __init__(self, project_contractors, contractor_employees_field, *args, **kwargs):
        super(ProjectContractorEmployeeFieldForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-contractor-employee-field-form'
        self.helper.form_method = 'POST'

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if project_contractors:
            self.helper.form.fields['project_contractors'].queryset = ProjectContractor.objects.all().filter(id=project_contractors)
            self.helper.form.fields['contractor_employees_field'].queryset = ContractorEmployeeField.objects.all().filter(id=contractor_employees_field)


        self.helper.layout = Layout(
            Fieldset('<i class="fa fa-info-circle"></i> General Information <!-- {{ user.perfil.companies.id }} -->',

                     Div(

                         PrependedText('project_contractors', ''),
                         PrependedText('contractor_employees_field', ''),
                         css_class='col-md-12'
                     )
                     ),

            Fieldset('',
                     Div(
                         PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                         PrependedText('is_active', ''),
                         css_class='col-md-12'
                     ),
                     ),
            FormActions(

                Submit('save', 'Save changes'),

            ),
        )


# ==================================
# Form: Project Floor Phase
# ==================================

class ProjectFloorPhaseForm(forms.ModelForm):
    class Meta:
        model = ProjectFloorPhase
        fields = ['projects', 'name', 'notes', 'is_active', ]

    projects = forms.Select()

    name = forms.CharField(label="Floor or Phase Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'",
                                      'placeholder': 'Floor or Phase Name'}
                           ))

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control',
                                       'placeholder': 'About this floor or phase'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}
                                ))

    def __init__(self, companies_id, projects_id, *args, **kwargs):
        super(ProjectFloorPhaseForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-floor-or-phase-form'
        self.helper.form_method = 'POST'

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if companies_id:
            self.helper.form.fields['projects'].queryset = Project.objects.all().filter(id=projects_id)


        self.helper.layout = Layout(
            Div(
                PrependedText('projects', ''),

                PrependedText('name', '<i class="glyphicon glyphicon-user"></i>'),
                PrependedText('notes', ''),
                PrependedText('is_active', ''),

                css_class='col-md-12'
            ),
            FormActions(

                Submit('save', 'Save changes'),

            ),
        )

# ==================================
# Form: Project Floor Phase
# ==================================

class ProjectFloorPhaseAreaForm(forms.ModelForm):
    class Meta:
        model = ProjectFloorPhaseArea
        fields = ['project_floor_phases', 'name', 'notes', 'is_active', ]

    project_floor_phases = forms.Select()

    name = forms.CharField(label="Area of Floor or Phase Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'",
                                      'placeholder': 'Floor or Phase Name'}
                           ))

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control',
                                       'placeholder': 'About this floor or phase'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}
                                ))

    def __init__(self, *args, **kwargs):
        super(ProjectFloorPhaseAreaForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-floor-or-phase-area-form'
        self.helper.form_method = 'POST'

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        self.helper.layout = Layout(
            Div(
                PrependedText('project_floor_phases', ''),

                PrependedText('name', '<i class="glyphicon glyphicon-user"></i>'),
                PrependedText('notes', ''),
                PrependedText('is_active', ''),

                css_class='col-md-12'
            ),
            FormActions(

                Submit('save', 'Save changes'),

            ),
        )


# ==================================
# Form: Project Floor Phase
# ==================================

class ProjectFloorPhaseAreaForm(forms.ModelForm):
    class Meta:
        model = ProjectFloorPhaseArea
        fields = ['project_floor_phases', 'name', 'notes', 'is_active', ]

    project_floor_phases = forms.Select()

    name = forms.CharField(label="Area of Floor or Phase Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'",
                                      'placeholder': 'Floor or Phase Name'}
                           ))

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control',
                                       'placeholder': 'About this floor or phase'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}
                                ))

    def __init__(self, *args, **kwargs):
        super(ProjectFloorPhaseAreaForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-floor-or-phase-area-form'
        self.helper.form_method = 'POST'

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        self.helper.layout = Layout(
            Div(
                PrependedText('project_floor_phases', ''),

                PrependedText('name', '<i class="glyphicon glyphicon-user"></i>'),
                PrependedText('notes', ''),
                PrependedText('is_active', ''),

                css_class='col-md-12'
            ),
            FormActions(

                Submit('save', 'Save changes'),

            ),
        )

class ProjectDetailDateForm(forms.ModelForm):
    class Meta:
        fields = ['StartDate', 'EndDate', ]

    start_date = forms.CharField(label="Start Date:", max_length=10,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control', 'data-inputmask': "'mask':'9999-99-99'",
                                            'placeholder': '2018-12-01 (example format)'}))

    end_date = forms.CharField(label="Start Date:", max_length=10,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control', 'data-inputmask': "'mask':'9999-99-99'",
                                          'placeholder': '2018-12-01 (example format)'}))

    def __init__(self, *args, **kwargs):
        super(ProjectDetailDateForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-detail-date-form'
        self.helper.form_method = 'POST'

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        self.helper.layout = Layout(
            Div(
                PrependedText('start_date', ''),
                PrependedText('end_date', ''),

                css_class='col-md-12'
            ),
            FormActions(

                Submit('save', 'Save changes'),

            ),
        )


# ==================================
# Form: Project Floor Phase
# ==================================

class ProjectFileForm(forms.ModelForm):
    class Meta:
        model = ProjectFile
        fields = ['projects', 'name', 'project_files', 'notes', 'is_active', ]

    projects = forms.Select()

    name = forms.CharField(label="File Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'",
                                      'placeholder': 'Floor or Phase Name'}
                           ))

    project_files = forms.CharField(label="File Name:", required=True, max_length=100,
                                    widget=forms.TextInput(
                                        attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                               'data-validate-length-range': "'6'", 'data-validate-words': "'2'",
                                               'placeholder': 'Floor or Phase Name'}
                                    ))

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control',
                                       'placeholder': 'About this floor or phase'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}
                                ))

    def __init__(self, *args, **kwargs):
        super(ProjectFileForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-project-floor-or-phase-form'
        self.helper.form_method = 'POST'

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        self.helper.layout = Layout(
            Div(
                PrependedText('projects', ''),

                PrependedText('name', '<i class="glyphicon glyphicon-user"></i>'),
                PrependedText('notes', ''),
                PrependedText('is_active', ''),

                css_class='col-md-12'
            ),
            FormActions(

                Submit('save', 'Save changes'),

            ),
        )


