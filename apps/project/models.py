from django.db import models

from apps.company.models import Company, CategoryProject, CostCode
from apps.employee.models import Employee
from apps.contractor.models import Contractor, ContractorEmployeeField
from apps.general_contractor.models import GeneralContractor
import datetime
from django.contrib.auth.models import User


# Create your models here.
PROJECT_ROL = (
        ('1', 'LABOR'), ('2', 'FOREMAN'), ('3', 'SUPERINTENDENT'), ('4', 'PROJECT MANAGEMENT'), ('5', 'CARPENTER')
    )

COMPLEXITY = (
        ('L', 'LOW'), ('M', 'MEDIUM'), ('H', 'HIGH')
    )

NUM_START = (
        ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5')
    )

class Project(models.Model):
    general_contractors = models.ForeignKey(GeneralContractor, on_delete=models.CASCADE)

    ProjectEmployees = models.ManyToManyField(
        Employee,
        through='ProjectEmployee'
    )

    project_mandays_schedules = models.ManyToManyField(
        CostCode,
        through='ProjectMandaySchedule'
    )

    project_contractors = models.ManyToManyField(
        Contractor,
        through='ProjectContractor'
    )

    companies = models.ForeignKey(Company, on_delete=models.CASCADE)
    category_projects = models.ForeignKey(CategoryProject, on_delete=models.CASCADE)

    name = models.CharField(max_length=100, blank=True, unique=True)

    po = models.CharField(max_length=10, blank=True, unique=True)
    cost = models.DecimalField(max_digits=12, decimal_places=2)

    email = models.CharField(max_length=75, blank=True)
    website_url = models.CharField(max_length=75, blank=True)
    cloud_file = models.CharField(max_length=255, blank=True)

    phone_number = models.CharField(max_length=14, blank=True)
    phone_number_ext = models.CharField(max_length=5, blank=True)

    country_address = models.CharField(max_length=100, blank=True)

    street_address = models.CharField(max_length=100, blank=True)
    city_address = models.CharField(max_length=50, blank=True)

    state_address = models.CharField(max_length=50, blank=True)
    zip_code_address = models.CharField(max_length=10, blank=True)

    start_date = models.DateField(null=True)
    finish_date = models.DateField(null=True)
    
    num_stars = models.CharField(max_length=1, blank=True, null=True, choices=NUM_START)
    complexity = models.CharField(max_length=1, blank=True, null=True, choices=COMPLEXITY)

    summary = models.TextField(blank=True, null=True)
    notes = models.TextField(max_length=255, blank=True, null=True)

    logo = models.ImageField(upload_to='./static/images/project-logo/'.format(po), blank=True, null=True)

    is_active = models.BooleanField(default=True)
    is_wage_scale = models.BooleanField(default=True, blank=True)


    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project'
        verbose_name = 'project'
        verbose_name_plural = 'projects'

        ordering = ('created_at',)

    def __str__(self):
        return '{}'.format(self.name)


class ProjectEmployee(models.Model):
    projects = models.ForeignKey(Project, on_delete=models.CASCADE)
    employees = models.ForeignKey(Employee, on_delete=models.CASCADE)

    notes = models.TextField(max_length=255, blank=True, null=True)

    is_leader = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_employee'
        verbose_name = 'project_employee'
        verbose_name_plural = 'project_employees'

    def __str__(self):
        return '{} {}'.format(self.employees, self.projects)


class ProjectContractor(models.Model):

    projects = models.ForeignKey(Project, on_delete=models.CASCADE)
    contractors = models.ForeignKey(Contractor, on_delete=models.CASCADE)

    notes = models.TextField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_contractor'
        verbose_name = 'project_contractor'
        verbose_name_plural = 'project_contractors'

    def __str__(self):
        return '{}'.format(self.contractors)


class ProjectFloorPhase(models.Model):
    projects = models.ForeignKey(Project, on_delete=models.CASCADE)

    name = models.CharField(max_length=50, blank=True)
    notes = models.TextField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_floor_phase'
        verbose_name = 'project_floor_phase'
        verbose_name_plural = 'project_floor_phases'

    def __str__(self):
        return ' {}'.format(self.name)


class ProjectFloorPhaseArea(models.Model):
    project_floor_phases = models.ForeignKey(ProjectFloorPhase, on_delete=models.CASCADE)

    name = models.CharField(max_length=50, blank=True)
    notes = models.TextField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_floor_phase_area'
        verbose_name = 'project_floor_phase_area'
        verbose_name_plural = 'project_floor_phase_areas'

    def __str__(self):
        return '{} - {}'.format(self.project_floor_phases, self.name)


class ProjectFloorPhaseAreaRoom(models.Model):
    project_floor_phases_areas = models.ForeignKey(ProjectFloorPhaseArea, on_delete=models.CASCADE)

    name = models.CharField(max_length=50, blank=True)
    notes = models.TextField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_floor_phase_area_room'
        verbose_name = 'project_floor_phase_area_room'
        verbose_name_plural = 'project_floor_phase_area_rooms'


    def __str__(self):
        return '{} - {}'.format(self.project_floor_phases_areas, self.name)


class ProjectFloorPhaseAreaRoomDamage(models.Model):
    STATUS_DAMAGE = (('O', 'Open'), ('D', 'Done'))

    project_floor_phases_area_rooms = models.ForeignKey(ProjectFloorPhaseAreaRoom, on_delete=models.CASCADE)

    name = models.CharField(max_length=50, blank=True)

    notes = models.TextField(max_length=255, blank=True, null=True)

    date_of_damage = models.DateField(("Date"), default=datetime.date.today)

    picture_of_damage = models.ImageField(upload_to='./static/images/damage/', blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_floor_phase_area_room_damage'
        verbose_name = 'project_floor_phase_area_room_damage'
        verbose_name_plural = 'project_floor_phase_area_room_damages'

    def __str__(self):
        return '{} - {}'.format(self.project_floor_phases_area_rooms, self.name)


class ProjectManDaySchedule(models.Model):
    UNITS = (('SF', 'SF - (Square Foot)'), ('LF', 'LF - (Linear Foot)'), ('EA', 'EA'))

    projects = models.ForeignKey(Project, on_delete=models.CASCADE)
    cost_codes = models.ForeignKey(CostCode,  related_name="project_manday_schedule", on_delete=models.CASCADE)
    project_floor_phases = models.ForeignKey(ProjectFloorPhase, on_delete=models.CASCADE)

    name = models.CharField(max_length=100, unique=False, blank=True)
    unit = models.CharField(max_length=2, blank=True, choices=UNITS)

    code_labor = models.CharField(max_length=20, unique=False, blank=True)
    code_material = models.CharField(max_length=20, unique=False, blank=True)

    production = models.DecimalField(max_digits=6, decimal_places=2)

    man_days_allotted = models.DecimalField(max_digits=6, decimal_places=2)

    regular_cost = models.DecimalField(max_digits=6, decimal_places=2)
    overtime_cost = models.DecimalField(max_digits=6, decimal_places=2)

    wage_cost = models.DecimalField(max_digits=6, decimal_places=2)
    wage_overtime_cost = models.DecimalField(max_digits=6, decimal_places=2)

    notes = models.TextField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_manday_schedule'
        verbose_name = 'project_manday_schedule'
        verbose_name_plural = 'project_manday_schedules'

    def __str__(self):
        return '{} - {}'.format(self.cost_codes, self.project_floor_phases)


class ProjectContractorEmployeeField(models.Model):
    daily_produce = models.ManyToManyField(
        ProjectManDaySchedule,
        through='DailyProduce'
    )

    project_contractors = models.ForeignKey(ProjectContractor, on_delete=models.CASCADE)
    contractor_employees_field = models.ForeignKey(ContractorEmployeeField, on_delete=models.CASCADE)

    notes = models.TextField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_contractor_employee_field'
        verbose_name = 'project_contractor_employee_field'
        verbose_name_plural = 'project_contractor_employees_field'

    def __str__(self):
        return '{} - {} {}'.format(self.contractor_employees_field, self.project_contractors, self.project_contractors)


class DailyProduce(models.Model):
    project_contractor_employees_field = models.ForeignKey(ProjectContractorEmployeeField, on_delete=models.CASCADE)
    project_manday_schedules = models.ForeignKey(ProjectManDaySchedule, on_delete=models.CASCADE)

    DAYS_OF_WEEK = (
        ('Mon', 'Monday'),
        ('Tus', 'Tuesday'),
        ('Wed', 'Wednesday'),
        ('Thu', 'Thursday'),
        ('Fri', 'Friday'),
        ('Sat', 'Saturday'),
        ('Sun', 'Sunday'),
    )

    TYPE_OF_WORK = (
        ('1', 'CONTRACT'),
        ('2', 'TICKET'),
        ('3', 'CHANGE'),
    )

    week = models.CharField(max_length=2, blank=True)
    today = models.CharField(max_length=3, choices=DAYS_OF_WEEK)
    labor_date = models.DateField(null=False)

    work_hours = models.DecimalField(max_digits=8, decimal_places=2)
    production = models.DecimalField(max_digits=8, decimal_places=2)

    type_of_work = models.CharField(max_length=1, choices=TYPE_OF_WORK)

    notes = models.TextField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)

    created_at = models.DateTimeField(null=False)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'daily_produce'
        verbose_name = 'daily_produce'
        verbose_name_plural = 'daily_produces'

    def __str__(self):
        return '{} - {}'.format(self.project_contractor_employees_field, self.project_manday_schedules)


class ProjectMandayStart(models.Model):

    projects = models.ForeignKey(Project, on_delete=models.CASCADE)

    project_manday_schedule_id = models.PositiveIntegerField()
    project_floor_phases_id = models.PositiveIntegerField()
    employee_field_id = models.PositiveIntegerField()

    employee_field_name = models.CharField(max_length=75, blank=True)
    contractor_name = models.CharField(max_length=75, blank=True)
    floor_phase_name = models.CharField(max_length=25, blank=True)
    category_cost_code_name = models.CharField(max_length=50, blank=True)
    cost_codes_name = models.CharField(max_length=50, blank=True)
    category_employees_field_name = models.CharField(max_length=75, blank=True)

    is_active = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_manday_start'
        verbose_name = 'project_manday_start'
        verbose_name_plural = 'project_manday_starts'


class ProjectFile(models.Model):
    projects = models.ForeignKey(Project, on_delete=models.CASCADE)

    name = models.CharField(max_length=50, blank=True)
    notes = models.TextField(max_length=255, blank=True, null=True)
    project_files = models.FileField(upload_to='./files/projects', blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_file'
        verbose_name = 'project_file'
        verbose_name_plural = 'project_files'

    def __str__(self):
        return '{} - {}'.format(self.projects, self.name)