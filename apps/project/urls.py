"""
URLs Project
"""
from django.contrib.auth.decorators import login_required

from django.conf.urls import url

from apps.project.views import ProjectCreate, ProjectDelete, ProjectList, ProjectUpdate, ProjectDetail, \
    ProjectDailyProduceCreate, ProjectEmployeeList, ProjectEmployeeCreate, ProjectEmployeeDelete, ProjectEmployeeUpdate, \
    ProjectContractorList, ProjectContractorCreate, ProjectContractorDelete, ProjectContractorUpdate, \
    ProjectManDayScheduleList, ProjectManDayScheduleCreate, ProjectManDayScheduleUpdate, ProjectManDayScheduleDelete, \
    ProjectContractorEmployeeFieldList, ProjectContractorEmployeeFieldCreate, \
    ProjectContractorEmployeeFieldUpdate,ProjectContractorEmployeeFieldDailyProduceList, \
    ProjectFloorPhaseList, ProjectFloorPhaseCreate, ProjectFloorPhaseAreaCreate, ProjectFloorPhaseUpdate, get_data, GeneratePdf, ChartData, HomeView, ReportePersonasPDF, \
    ProjectManDayScheduleReport, ProjectManDayScheduleLaborReport, ProjectDailyProduceUpdate, ProjectActivityLaborReport, ProjectDailyProduceDelete, ProjectAssignToUserList

urlpatterns = [
    ###############
    # URLs PROJECT
    ###############

    url(r'^(?P<pk>\d+)/create/(?P<user_id>\d+)/$',
        login_required(ProjectCreate.as_view()), name='project_create'),

    url(r'^(?P<companies_id>\d+)/list/(?P<user_id>\d+)/$',
        login_required(ProjectList.as_view()), name='project_list'),

    url(r'^(?P<companies_id>\d+)/update/(?P<pk>\d+)/(?P<user_id>\d+)/$',
        login_required(ProjectUpdate.as_view()), name='project_update'),

    url(r'^delete/(?P<pk>\d+)/$',
        login_required(ProjectDelete.as_view()), name='project_delete'),

    url(r'^(?P<pk>\d+)/detail/date/$', login_required(ProjectDetail.as_view()), name='project_detail'),


    # DATA Graph
    url(r'^home/$', HomeView.as_view(), name='home'),
    url(r'^(?P<pk>\d+)/api/data/$', login_required(get_data), name='api-data'),
    url(
        r'^(?P<pk>\d+)/api/chart/data/(?P<years>[0-9]{4})-(?P<months>[0-9]{2})-(?P<days>[0-9]{2})/(?P<yeare>[0-9]{4})-(?P<monthe>[0-9]{2})-(?P<daye>[0-9]{2})/$',
        login_required(ChartData.as_view())),

    # PROJECT REPORTS
    url(r'^(?P<companies_id>\d+)/detail/(?P<pk>\d+)/date/$', login_required(GeneratePdf.as_view()), name='project_contractors_report'),

    url(r'^(?P<companies_id>\d+)/detail/(?P<pk>\d+)/r/$', login_required(ReportePersonasPDF.as_view()),
        name='project_personas_report'),

    url(r'^(?P<companies_id>\d+)/(?P<pk>\d+)/report/$', login_required(ProjectManDayScheduleReport.as_view()),
        name='project_manday_schedule_report'),

    url(r'^(?P<companies_id>\d+)/(?P<pk>\d+)/man_days_floor_rpt/$', login_required(ProjectManDayScheduleLaborReport.as_view()),
        name='project_manday_schedule_labor_report'),

    url(r'^(?P<companies_id>\d+)/(?P<pk>\d+)/activity_labor_rpt/$',
        login_required(ProjectActivityLaborReport.as_view()),
        name='project_activity_labor_report'),

    url(r'^rp_pdf/$',login_required(ReportePersonasPDF.as_view()), name="reporte_personas_pdf"),

    ##############################
    # URLs PROJECT DAILY PRODUCED
    ##############################
    # CREATE
    url(r'^project_daily_produced_create/(?P<pk>\d+)/$',
        login_required(ProjectDailyProduceCreate.as_view()), name='project_daily_produced_create'),

    # DELETE
    url(r'^(?P<project_id>\d+)/project_daily_produced_delete/(?P<pk>\d+)/$',
        login_required(ProjectDailyProduceDelete.as_view()), name='project_daily_produced_delete'),

    # UPDATE
    url(r'^(?P<project_id>\d+)/(?P<project_contractor_employees_field_id>\d+)/(?P<project_manday_schedules_id>\d+)/project_daily_produced_update/(?P<pk>\d+)/$',
        login_required(ProjectDailyProduceUpdate.as_view()), name='project_daily_produced_update'),

    url(r'^(?P<pk>\d+)/project_contractor_employee_field_daily_produce_list/(?P<contractors_id>\d+)/$',
        login_required(ProjectContractorEmployeeFieldDailyProduceList.as_view()),
        name='project_contractor_employee_field_daily_produce_list'),


    # ########################### #
    # URLs PROJECT TEAM MEMBERS   #
    # ########################### #

    url(r'^(?P<companies_id>\d+)/project_employee_list/(?P<pk>\d+)/$',
        login_required(ProjectEmployeeList.as_view()), name='project_employee_list'),

    url(r'^(?P<companies_id>\d+)/(?P<project_id>\d+)/project_employee_create/(?P<pk>\d+)/$',
        login_required(ProjectEmployeeCreate.as_view()), name='project_employee_create'),

    url(r'^(?P<companies_id>\d+)/(?P<project_id>\d+)/project_employee_update/(?P<pk>\d+)/$',
        login_required(ProjectEmployeeUpdate.as_view()), name='project_employee_update'),

    url(r'^(?P<project_id>\d+)/(?P<pk>\d+)/project_employee_delete/$',
        login_required(ProjectEmployeeDelete.as_view()), name='project_employee_delete'),

    # ########################### #
    # URLs ASSIGN PROJECT TO USER #
    # ########################### #

    url(r'^(?P<companies_id>\d+)/project_assign_project_to_user_list/$',
        login_required(ProjectAssignToUserList.as_view()), name='project_assign_project_to_user_list'),

    ##############################
    # URLs PROJECT CONTRACTOR
    ##############################
    url(r'^(?P<companies_id>\d+)/(?P<pk>\d+)/project_contractor_list/$',
        login_required(ProjectContractorList.as_view()), name='project_contractor_list'),

    url(r'^(?P<companies_id>\d+)/(?P<project_id>\d+)/project_contractor_create/(?P<pk>\d+)/$',
        login_required(ProjectContractorCreate.as_view()), name='project_contractor_create'),

    url(r'^(?P<companies_id>\d+)/(?P<project_id>\d+)/project_contractor_update/(?P<pk>\d+)/$',
        login_required(ProjectContractorUpdate.as_view()), name='project_contractor_update'),

    url(r'^(?P<project_id>\d+)/project_contractor_delete/(?P<pk>\d+)/$',
        login_required(ProjectContractorDelete.as_view()), name='project_contractor_delete'),

    #################################
    # URLs PROJECT MAN DAYS SCHEDULE
    #################################

    url(r'^(?P<companies_id>\d+)/(?P<pk>\d+)/project_manday_schedule_list/$',
        login_required(ProjectManDayScheduleList.as_view()), name='project_manday_schedule_list'),

    url(r'^(?P<companies_id>\d+)/(?P<project_id>\d+)/project_manday_schedule_create/(?P<pk>\d+)/$',
        login_required(ProjectManDayScheduleCreate.as_view()), name='project_manday_schedule_create'),

    url(r'^(?P<companies_id>\d+)/(?P<project_id>\d+)/project_manday_schedule_update/(?P<pk>\d+)/$',
        login_required(ProjectManDayScheduleUpdate.as_view()), name='project_manday_schedule_update'),

    url(r'^(?P<project_id>\d+)/project_manday_schedule_delete/(?P<pk>\d+)/$',
        login_required(ProjectManDayScheduleDelete.as_view()), name='project_manday_schedule_delete'),

    ##########################
    # URLs PROJECT FLOOR PHASE
    ##########################

    url(r'^(?P<companies_id>\d+)/project_floor_phase_list/(?P<pk>\d+)/$',
        login_required(ProjectFloorPhaseList.as_view()), name='project_floor_phase_list'),

    url(r'^(?P<companies_id>\d+)/project_floor_phase_create/(?P<pk>\d+)/$',
        login_required(ProjectFloorPhaseCreate.as_view()), name='project_floor_phase_create'),

    url(r'^(?P<companies_id>\d+)/(?P<project_id>\d+)/project_floor_phase_update/(?P<pk>\d+)/$',
        login_required(ProjectFloorPhaseUpdate.as_view()), name='project_floor_phase_update'),

    ##########################
    # URLs PROJECT FLOOR AREA
    ##########################
    url(r'^(?P<pk>\d+)/project_floor_phase_area_create/$',
        login_required(ProjectFloorPhaseAreaCreate.as_view()), name='project_floor_phase_area_create'),

    #########################################
    # URLs PROJECT CONTRACTOR EMPLOYEE FIELD
    #########################################

    url(r'^(?P<pk>\d+)/project_contractor_employee_field_list/(?P<contractors_id>\d+)/$',
        login_required(ProjectContractorEmployeeFieldList.as_view()), name='project_contractor_employee_field_list'),

    url(r'^(?P<project_contractor_id>\d+)/project_contractor_employee_field_create/(?P<contractor_id>\d+)/(?P<pk>\d+)/$',
        login_required(ProjectContractorEmployeeFieldCreate.as_view()), name='project_contractor_employee_field_create'),

    url(r'^(?P<project_contractor_id>\d+)/project_contractor_employee_field_update/(?P<pk>\d+)/$',
        login_required(ProjectContractorEmployeeFieldUpdate.as_view()), name='project_contractor_employee_field_update'),

]
