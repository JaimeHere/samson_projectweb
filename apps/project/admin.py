from django.contrib import admin
from apps.project.models import Project, ProjectEmployee, ProjectManDaySchedule, ProjectFloorPhase, ProjectContractor, \
    ProjectContractorEmployeeField, DailyProduce, ProjectFloorPhaseArea, ProjectFloorPhaseAreaRoom, ProjectFloorPhaseAreaRoomDamage, ProjectMandayStart, ProjectFile


# Register your models here.

admin.site.register(Project)
admin.site.register(ProjectEmployee)
admin.site.register(ProjectContractor)
admin.site.register(ProjectContractorEmployeeField)
admin.site.register(ProjectManDaySchedule)
admin.site.register(ProjectFloorPhase)
admin.site.register(ProjectFloorPhaseArea)
admin.site.register(ProjectFloorPhaseAreaRoom)
admin.site.register(ProjectFloorPhaseAreaRoomDamage)
admin.site.register(DailyProduce)
admin.site.register(ProjectMandayStart)
admin.site.register(ProjectFile)