import datetime
import datetime as dt
from datetime import datetime, timedelta
from datetime import date
import calendar

from django.urls import reverse_lazy
from django.db.models import Sum
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView, TemplateView, View
from django.db.models import F, Q, Sum, Avg, Max, Min, Case, When, Count

from samson_project.utils import render_to_pdf  # created in step 4

# Import Graph
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse, HttpResponse, request, HttpRequest
from django.shortcuts import render
# End Import Graph

# Importamos settings para poder tener a la mano la ruta de la carpeta media
from django.conf import settings
from io import BytesIO

from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.platypus import Table
from reportlab.platypus import TableStyle
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, PageBreak
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import mm, inch

from django import forms
from bootstrap_datepicker_plus import DatePickerInput

from apps.project.forms import ProjectForm, DailyProduceForm, DailyProduceUpdateForm, ProjectEmployeeCreateForm, \
    ProjectEmployeeUpdateForm, \
    ProjectManDayScheduleForm, ProjectContractorForm, ProjectContractorEmployeeFieldForm, ProjectFloorPhaseForm, \
    ProjectFloorPhaseAreaForm, ProjectDetailDateForm

from apps.contractor.models import ContractorEmployeeField, EmployeeField

from apps.project.models import Project, DailyProduce, Employee, ProjectEmployee, \
    ProjectContractor, Contractor, CostCode, ProjectManDaySchedule, ProjectFloorPhase, \
    ProjectContractorEmployeeField, ProjectFloorPhaseArea, ProjectFloorPhaseAreaRoom, ProjectFloorPhaseAreaRoomDamage, \
    ProjectMandayStart

from apps.company.models import Company, CategoryProject
from apps.user_profile.models import UserProfile, UserProject


# Create your views here.

# ########################
# PROJECT DAILY PRODUCED #
# ########################

# CREATE
# ==========
class ProjectDailyProduceCreate(CreateView):
    model = DailyProduce
    form_class = DailyProduceForm
    template_name = 'apps/project/project_daily_produce/project_daily_produce_form.html'

    def form_valid(self, form):
        form.instance.create_user = self.request.user

        project_daily_produced = form.save(commit=False)  # Para obtener el ID

        # Update Field
        project_daily_produced.labor_date = project_daily_produced.created_at
        project_daily_produced.updated_at = project_daily_produced.created_at
        project_daily_produced.week = project_daily_produced.created_at.isocalendar()[1]
        project_daily_produced.today = project_daily_produced.created_at.strftime("%A")[:3]

        # Register Save
        project_daily_produced.save()

        return super(ProjectDailyProduceCreate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(ProjectDailyProduceCreate, self).get_initial()

        initial.update(
            {
                'project_id': self.kwargs['pk'],
            }
        )
        return initial

    def get_form_kwargs(self):
        kwargs = super(ProjectDailyProduceCreate, self).get_form_kwargs()

        kwargs.update(
            {
                'project_id': self.kwargs['pk'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_daily_produced_create',
                                kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('project:project_daily_produced_create', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        context = super(ProjectDailyProduceCreate, self).get_context_data(**kwargs)

        context['project_info'] = Project.objects.get(id=self.kwargs['pk'])

        context.update({'project_id': self.kwargs['pk']})

        return context


# UPDATE
# ==========
class ProjectDailyProduceUpdate(UpdateView):
    model = DailyProduce
    form_class = DailyProduceUpdateForm
    template_name = 'apps/project/project_daily_produce/project_daily_produce_form.html'

    def form_valid(self, form):
        form.instance.create_user = self.request.user

        project_daily_produced = form.save(commit=False)  # Para obtener el ID

        # Update Field
        project_daily_produced.labor_date = project_daily_produced.created_at
        project_daily_produced.updated_at = project_daily_produced.created_at
        project_daily_produced.week = project_daily_produced.created_at.isocalendar()[1]
        project_daily_produced.today = project_daily_produced.created_at.strftime("%A")[:3]

        # Register Save
        project_daily_produced.save()

        return super(ProjectDailyProduceUpdate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(ProjectDailyProduceUpdate, self).get_initial()

        initial.update(
            {
                'project_id': self.kwargs['project_id'],
                'project_contractor_employees_field': self.kwargs['project_contractor_employees_field_id'],
                'project_manday_schedules': self.kwargs['project_manday_schedules_id'],
                'dairy_produce': self.kwargs['pk'],
            }
        )
        return initial

    def get_form_kwargs(self):
        kwargs = super(ProjectDailyProduceUpdate, self).get_form_kwargs()
        info = DailyProduce.objects.select_related().filter(id=self.kwargs['pk']).values(
            'project_manday_schedules__unit', 'project_manday_schedules__production')

        kwargs.update(
            {
                'project_contractor_employees_field': self.kwargs['project_contractor_employees_field_id'],
                'project_manday_schedules': self.kwargs['project_manday_schedules_id'],
                'unit': info[0]['project_manday_schedules__unit'],
                'production': str(info[0]['project_manday_schedules__production']),

            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_detail', kwargs={'pk': self.kwargs['project_id']})
        else:
            return reverse_lazy('project:project_detail', args=(self.object.id,))


    def get_context_data(self, **kwargs):
        context = super(ProjectDailyProduceUpdate, self).get_context_data(**kwargs)
        context['project_info'] = Project.objects.get(id=self.kwargs['project_id'])
        context.update({'project_id': self.kwargs['project_id']})

        return context

class ProjectDailyProduceDelete(DeleteView):
    model = DailyProduce
    template_name = 'apps/project/project_daily_produce/project_daily_produce_delete.html'
    def get_context_data(self, **kwargs):

        context = super(ProjectDailyProduceDelete, self).get_context_data(**kwargs)

        context['DeleteRegister'] = DailyProduce.objects.get(pk=self.kwargs['pk'])

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_detail', kwargs={'pk': self.kwargs['project_id']})
        else:
            return reverse_lazy('project:project_detail', args=(self.object.id,))

#
# Project Contractor Employee Field daily PRODUCED
# ===========================
class ProjectContractorEmployeeFieldDailyProduceList(ListView):
    model = ProjectContractorEmployeeField
    template_name = 'apps/project/project_contractor_employee_field/project_contractor_employee_field_list_daily_produce.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectContractorEmployeeFieldDailyProduceList, self).get_context_data(**kwargs)

        # Empployee Assigned
        list_assigned = ProjectContractorEmployeeField.objects.select_related().all(). \
            filter(project_contractors__contractors_id=self.kwargs['contractors_id']). \
            filter(project_contractors__projects_id=self.kwargs['pk'])

        context['ProjectContractorAssignedEmployeeField'] = list_assigned

        # Send Informacion
        context['information'] = ProjectContractor.objects.select_related('projects').all().get(id=self.kwargs['pk'])

        # Send ID
        context['project_contractor_id'] = self.kwargs

        context['filter_models'] = (
            {'project_contractor_id': self.kwargs, 'contractors_id': self.kwargs['contractors_id']})

        return context


# ##################### #
# Views: PROJECTS CRUD  #
# ##################### #

# CREATE
# =============================
class ProjectCreate(CreateView):
    model = Project
    form_class = ProjectForm
    template_name = 'apps/project/project_form.html'
    success_url = reverse_lazy('project:project_list')

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(ProjectCreate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(ProjectCreate, self).get_initial()
        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        initial.update(
            {
                'companies_id': companies_id['id'],
                'companies': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(ProjectCreate, self).get_form_kwargs()
        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
                'user_id': self.kwargs['user_id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_list',
                                kwargs={'companies_id': self.kwargs['pk'], 'user_id': self.kwargs['user_id']})
        else:
            return reverse_lazy('project:project_list', args=(self.object.id,))


# LIST
# ====================
class ProjectList(ListView):
    model = Project
    # template_name = 'apps/project/project_list.html'
    template_name = 'apps/project/projects.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectList, self).get_context_data(**kwargs)

        # ADMIN = 1
        # STAFF = 2ProjectContractorCost
        # SUPERINTENDENT = 3
        # PME = 4
        # FOREMAN = 5

        user_rol = UserProfile.objects.all().get(username_id=self.kwargs['user_id'])
        project_user = UserProject.objects.all().filter(userprofile_id=user_rol).values('projects_id')

        if user_rol.role in [1]:
            project_list = Project.objects.select_related().filter(companies_id=self.kwargs['companies_id'])

        else:
            project_list = Project.objects.select_related().all().filter(companies_id=self.kwargs['companies_id'],
                                                                         id__in=project_user, is_active=True)

        context['ProjectList'] = project_list
        return context


# UPDATE
# =====================
class ProjectUpdate(UpdateView):
    model = Project
    form_class = ProjectForm
    template_name = 'apps/project/project_form.html'
    success_url = reverse_lazy('project:project_list')

    def form_valid(self, form):
        form.instance.update_user = self.request.user
        return super(ProjectUpdate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(ProjectUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
                'user_id': self.kwargs['user_id']
            }
        )
        return initial

    def get_form_kwargs(self):
        kwargs = super(ProjectUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
                'user_id': self.kwargs['user_id']
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_list',
                                kwargs={'companies_id': self.kwargs['companies_id'], 'user_id': self.kwargs['user_id']})
        else:
            return reverse_lazy('project:project_list', args=(self.object.id,))


# DELETE
# =====================
class ProjectDelete(DeleteView):
    model = Project
    template_name = 'apps/project/project_delete.html'
    success_url = reverse_lazy('project:project_list')


# DETAIL
# =============================
class ProjectDetail(DetailView):
    date = datetime.now()
    model = Project
    template_name = 'apps/project/project_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectDetail, self).get_context_data(**kwargs)

        project_info = Project.objects.all().values('is_wage_scale', 'start_date', 'finish_date').get(
            id=self.kwargs['pk'])

        start_date = self.request.GET.get('dates')
        finish_date = self.request.GET.get('datee')

        if start_date is None:
            project_today = date.today()
            what_day = calendar.day_name[project_today.weekday()]

            if what_day == "Saturday":
                start_date = project_today - timedelta(days=5)
                finish_date = project_today

            if what_day == "Friday":
                start_date = project_today - timedelta(days=4)
                finish_date = project_today

            if what_day == "Thursday":
                start_date = project_today - timedelta(days=3)
                finish_date = project_today + timedelta(days=1)

            if what_day == "Wednesday":
                start_date = project_today - timedelta(days=2)
                finish_date = project_today + timedelta(days=2)

            if what_day == "Tuesday":
                start_date = project_today - timedelta(days=1)
                finish_date = project_today + timedelta(days=3)

            if what_day == "Monday":
                start_date = project_today
                finish_date = project_today + timedelta(days=5)

        data = ({
            'dates': start_date,
            'datee': finish_date
        })

        context['ProjectStartEndDate'] = data

        total_man_days_allotted = 0
        total_contractors = 0
        total_employees_field = 0
        total_man_days_remaining = 0
        total_man_days_spent = 0
        project_progress = 0
        regular_cost = 0

        # Verify if project is Wage Scale
        if project_info['is_wage_scale']:
            type_wage = 'project_manday_schedules__wage_cost'
        else:
            type_wage = 'project_manday_schedules__regular_cost'

        context['project_info'] = project_info

        # FLOOR PHASES LIST
        #project_floor_phases =

        context['ProjectFloorPhaseList'] = ProjectFloorPhaseAreaRoomDamage.objects.all().select_related().filter(
            project_floor_phases_area_rooms__project_floor_phases_areas__project_floor_phases__projects_id=self.kwargs[
                'pk'])

        # Project Employee
        #employee_project =
        context['ProjectEmployeeProject'] = ProjectEmployee.objects.select_related('projects', 'employees').filter(
            projects_id=self.kwargs['pk'])

        # Employee Team
        # project_employee_team =
        context['ProjectEmployeeTeams'] = ProjectEmployee.objects.select_related('projects', 'employees').filter(
            projects_id=self.kwargs['pk'])

        ############################

        # ****  MAN DAYS SCHEDULE ***** #
        if self.request.GET.get('table_mds'):

            project_man_day_schedule = DailyProduce.objects. \
                filter(
                project_manday_schedules__projects_id=self.kwargs['pk']
            ). \
                values('project_manday_schedules__project_floor_phases__name',
                       'project_manday_schedules__cost_codes__name',
                       'project_manday_schedules__unit',
                       'project_manday_schedules__code_labor',
                       'project_manday_schedules__code_material',
                       'project_manday_schedules__production',
                       'project_manday_schedules__man_days_allotted', type_wage). \
                annotate(
                man_days_spent=Sum('work_hours'),
                project_production=Sum('production'),
                man_days_remaining=((F('project_manday_schedules__man_days_allotted') * 8) - Sum('work_hours')) / 8
            ). \
                order_by('project_manday_schedules__cost_codes__name')

        else:
            project_man_day_schedule = DailyProduce.objects. \
                filter(
                project_manday_schedules__projects_id=self.kwargs['pk']
            ). \
                values('project_manday_schedules__project_floor_phases__name',
                       'project_manday_schedules__cost_codes__name',
                       'project_manday_schedules__unit',
                       'project_manday_schedules__code_labor',
                       'project_manday_schedules__code_material',
                       'project_manday_schedules__production',
                       'project_manday_schedules__man_days_allotted', type_wage). \
                annotate(
                man_days_spent=Sum('work_hours'),
                project_production=Sum('production'),
                man_days_remaining=((F('project_manday_schedules__man_days_allotted') * 8) - Sum('work_hours')) / 8
            ). \
                order_by('project_manday_schedules__cost_codes__name')

        context['projectManDayScheduleSelected'] = project_man_day_schedule

        # **** END MAN DAYS SCHEDULE **** #

        context['projectDailyProduce'] = DailyProduce.objects. \
            filter(
            project_manday_schedules__projects_id=self.kwargs['pk']
        ).annotate(
            to_produce=((F('project_manday_schedules__production') / 8) * Sum('work_hours'))
        ). \
            order_by('project_manday_schedules__cost_codes__name')


        # LIST CONTRACTORS IN PROJECTS
        context['ProjectContractorList'] = ProjectContractor.objects.all(). \
            select_related().filter(projects_id=self.kwargs['pk'])

        # ** Contractors List with Hours and Cost
        project_contractors = DailyProduce.objects. \
            values('project_contractor_employees_field__project_contractors__contractors__name'). \
            filter(project_manday_schedules__projects_id = self.kwargs['pk'],
                   labor_date__range = (start_date, finish_date)). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),

                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                ),
            ),
            hours=Sum('work_hours')
        )

        contractors_list = ProjectContractor.objects.all().select_related().filter(projects_id=self.kwargs['pk'])

        if project_contractors.count() > 0:
            context['ProjectContractorCost'] = project_contractors.exclude(id__in=contractors_list)
        else:
            context['ProjectContractorList'] = ProjectContractor.objects.all().select_related().filter(
                projects_id=self.kwargs['pk'])

        # { MAN DAYS CALCULATOR
        total_employees_field = ProjectContractorEmployeeField.objects.all(). \
            filter(project_contractors__projects_id=self.kwargs['pk']).count()

        total_contractors = ProjectContractor.objects.all(). \
            filter(projects_id=self.kwargs['pk']).count()

        total_floors = ProjectFloorPhase.objects.all().filter(projects_id=self.kwargs['pk']).count()

        total_man_days = ProjectManDaySchedule.objects.all().filter(projects_id=self.kwargs['pk']).count()

        man_days_allotted = ProjectManDaySchedule.objects.filter(projects__id=self.kwargs['pk']). \
            aggregate(total_man_days_allotted=Sum('man_days_allotted'))

        total_regular_cost = ProjectManDaySchedule.objects.filter(projects__id=self.kwargs['pk']). \
            aggregate(total_regular_cost=Sum('regular_cost') * 2)

        if man_days_allotted['total_man_days_allotted'] is not None:
            total_man_days_allotted = man_days_allotted['total_man_days_allotted']
        else:
            total_man_days_allotted = 0

        daily_work_hours = DailyProduce.objects.filter(project_manday_schedules__projects_id=self.kwargs['pk'],
                                                       labor_date__range=(start_date, finish_date)). \
            aggregate(total_daily_work_hours=Sum('work_hours'))

        if daily_work_hours['total_daily_work_hours'] is not None:
            # if man_days_allotted.exists() and daily_work_hours.exists():
            total_man_days_spent = (daily_work_hours['total_daily_work_hours'] / 8)
            total_man_days_remaining = total_man_days_allotted - total_man_days_spent
            project_progress = round((total_man_days_spent / total_man_days_allotted) * 100, 2)
        else:
            daily_work_hours['total_daily_work_hours'] = 0

        data = ({
            'project_progress': project_progress,
            'total_contractors': total_contractors,
            'total_employees_field': total_employees_field,
            'man_days_allotted': total_man_days_allotted,
            'man_days_spent': round(total_man_days_spent, 2),
            'man_days_remaining': round(total_man_days_remaining, 2),
            'total_regular_cost': total_regular_cost['total_regular_cost'],
            'total_floors': total_floors,
            'total_man_days': total_man_days,
            'start_date': start_date,
            'finish_date': finish_date
        })

        context['ProjectManDaysDetail'] = data
        # } END MAN DAY CALCULATE

        # START FIRST SOURCE
        project_first_source = DailyProduce.objects.values('project_contractor_employees_field__contractor_employees_field__employees_field__state_address'). \
            filter(project_manday_schedules__projects_id=self.kwargs['pk'],
                   labor_date__range=(start_date, finish_date)). \
            aggregate(
            total_hours=Sum('work_hours'),
            total_dc=Sum(
                Case(
                    When(project_contractor_employees_field__contractor_employees_field__employees_field__state_address = "DC",
                         then=F('work_hours')
                         )
                ),
            ),
            total_not_dc=Sum(
                Case(
                    When(
                        project_contractor_employees_field__contractor_employees_field__employees_field__state_address = "VA",
                        then=F('work_hours')
                    )
                ),
            )
        )

        employees_field_first_source = ProjectContractorEmployeeField.objects.select_related().all(). \
            filter(project_contractors__projects_id=self.kwargs['pk']). \
            aggregate(
            total_employees_field=Count('contractor_employees_field__employees_field'),
            total_employee_field_dc=Count(
                Case(
                    When(
                        contractor_employees_field__employees_field__state_address = "DC",
                        then='contractor_employees_field__employees_field__state_address'
                    )
                )
            )
        )

        first_data = ({
            'total_hours': project_first_source['total_hours'],
            'total_dc':project_first_source['total_dc'],
            'total_not_dc':project_first_source['total_not_dc'],
            'total_employees_field': employees_field_first_source['total_employees_field'],
            'total_employee_field_dc': employees_field_first_source['total_employee_field_dc']
        })

        context['ProjectFirstSource'] = first_data
        # END FIRST SOURCE

        employees_field_first_source_labor = DailyProduce.objects.values(
            'project_contractor_employees_field__contractor_employees_field__employees_field__category_employees_field__name').filter(
            project_manday_schedules__projects_id=self.kwargs['pk']).annotate(
                total_hours=Sum('work_hours'),
                total_dc=Sum(
                    Case(
                        When(
                            project_contractor_employees_field__contractor_employees_field__employees_field__state_address="DC",
                            then=F('work_hours')
                        )
                    ),
                ),
                total_not_dc=Sum(
                    Case(
                        When(
                            project_contractor_employees_field__contractor_employees_field__employees_field__state_address="VA",
                            then=F('work_hours')
                        )
                    ),
                )
        )


        context['ProjectFirstSourceLabor'] = employees_field_first_source_labor

        employees_field_first_source = ProjectContractorEmployeeField.objects.select_related().all(). \
            filter(project_contractors__projects_id=self.kwargs['pk']). \
            aggregate(
            total_employees_field=Count('contractor_employees_field__employees_field'),
            total_employee_field_dc=Count(
                Case(
                    When(
                        contractor_employees_field__employees_field__state_address="DC",
                        then='contractor_employees_field__employees_field__state_address'
                    )
                )
            )
        )

        first_data = ({
            'total_hours': project_first_source['total_hours'],
            'total_dc': project_first_source['total_dc'],
            'total_not_dc': project_first_source['total_not_dc'],
            'total_employees_field': employees_field_first_source['total_employees_field'],
            'total_employee_field_dc': employees_field_first_source['total_employee_field_dc']
        })

        context['ProjectFirstSource'] = first_data
        # END FIRST SOURCE

        # [ CONTRACTORS Information
        contractor_cost_hours = DailyProduce.objects. \
            values('project_contractor_employees_field__project_contractors__contractors__name'). \
            filter(
            project_manday_schedules__projects_id=self.kwargs['pk']
        ). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            hours=Sum('work_hours')
        )
        context['CompanyContractor'] = contractor_cost_hours.order_by('hours')

        # [ CATEGORY LABOR Information
        company_category_labor = DailyProduce.objects. \
            values('project_manday_schedules__cost_codes__category_cost_codes__name'). \
            filter(
            project_manday_schedules__projects_id=self.kwargs['pk']). \
            annotate(cost=Sum(
            Case(
                When(project_manday_schedules__projects__is_wage_scale=False,
                     then=F('work_hours') * F('project_manday_schedules__regular_cost')
                     ),
                When(project_manday_schedules__projects__is_wage_scale=True,
                     then=F('work_hours') * F('project_manday_schedules__wage_cost')
                     )
            )
        ),
            hours=Sum('work_hours')
        )
        context['CompanyCategoryLabor'] = company_category_labor.order_by('hours')

        # [ CATEGORY EMPLOYEE FIELD Information
        company_category_employee_field = DailyProduce.objects. \
            values(
            'project_contractor_employees_field__contractor_employees_field__employees_field__category_employees_field__name'). \
            filter(
            project_manday_schedules__projects_id=self.kwargs['pk']). \
            annotate(cost=Sum(
            Case(
                When(project_manday_schedules__projects__is_wage_scale=False,
                     then=F('work_hours') * F('project_manday_schedules__regular_cost')
                     ),
                When(project_manday_schedules__projects__is_wage_scale=True,
                     then=F('work_hours') * F('project_manday_schedules__wage_cost')
                     )
            )
        ),
            hours=Sum('work_hours')
        )
        context['CompanyCategoryEmployeeField'] = company_category_employee_field.order_by('hours')

        # { START LABOR
        data = ProjectMandayStart.objects.select_related(). \
            filter(projects_id=self.kwargs['pk'], is_active=True)

        context['ProjectLaborStart'] = data

        # EMPLOYEE ASSIGNED
        list_assigned = ProjectContractorEmployeeField.objects.select_related().all(). \
            filter(project_contractors__projects_id=self.kwargs['pk'])

        context['ProjectContractorAssignedEmployeeField'] = list_assigned

        return context

    success_url = reverse_lazy('project:project_detail')


class HomeView(APIView):
    def get(self, request, *args, **kwargs):
        return render(request, 'apps/project/charts.html', {"customers": 10})


def get_data(request, *args, **kwargs):
    data = {
        "Man Day": 100,
        "Cost": 10,
    }
    print(data)
    return JsonResponse(data)  # http response



#############
# Graficar datos
class ChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, *args, **kwargs):

        start_date = self.kwargs['years'] + '-' + self.kwargs['months'] + '-' + self.kwargs['days']
        finish_date = self.kwargs['yeare'] + '-' + self.kwargs['monthe'] + '-' + self.kwargs['daye']

        print("DATES CHART ****** ", start_date, finish_date, self.kwargs)

        project_info = Project.objects.all().values('is_wage_scale', 'start_date', 'finish_date').get(
            id=self.kwargs['pk'])

        # Verify if project is Wage Scale
        if project_info['is_wage_scale']:
            type_wage = 'project_manday_schedules__wage_cost'
        else:
            type_wage = 'project_manday_schedules__regular_cost'

        data = DailyProduce.objects.filter(project_manday_schedules__projects_id=self.kwargs['pk']). \
            values('labor_date', 'work_hours', 'production', 'project_manday_schedules__production', type_wage). \
            filter(labor_date__range=(start_date, finish_date)).order_by('labor_date')

        # Init variables
        data_date = []
        man_days = []
        wage_cost = []
        hours = 0
        production_sum = 0
        manday_schedules__production_sum = 0
        man_day_cal = 0
        production = []
        manday_schedules__production = []
        cost = 0
        percent_sum = 0
        percent_val = 0
        to_produce = 0

        percent = []
        a = 0
        # Get labels X
        for k, v in enumerate(data):
            if v['labor_date'] not in data_date:
                data_date.append(v['labor_date'])

        # Get valors Y
        for idx, item in enumerate(data_date):
            for idx, item2 in enumerate(data):
                if item == item2['labor_date']:
                    hours = hours + item2['work_hours']
                    cost = cost + (item2[type_wage] * item2['work_hours'])

                    to_produce = (item2['project_manday_schedules__production'] / 8) * item2['work_hours']
                    production_sum = production_sum + item2['production']

                    manday_schedules__production_sum = manday_schedules__production_sum + to_produce

            man_day_cal = hours / 8
            man_days.append(round(man_day_cal,2))
            wage_cost.append(round(cost,2))
            percent_val = (production_sum / manday_schedules__production_sum) * 100
            percent.append(round(percent_val,2))

            hours = 0
            man_day_cal = 0
            cost = 0
            production_sum = 0
            to_produce = 0
            manday_schedules__production_sum = 0

        # End Calculator valors Y

        # Send Data to Graph
        data = {
            "labels_graph": data_date,
            "default_man_days": man_days,
            "default_project_cost": wage_cost,
            "default_production_percent": percent
        }
        return Response(data)



################################# #
# PROJECTS TO USERS
################################# #

# LIST
# =================
class ProjectAssignToUserList(ListView):
    model = Project
    template_name = 'apps/project/project_assign_project_to_user/project_assign_project_to_user_list.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectAssignToUserList, self).get_context_data(**kwargs)
        assigned = UserProject.objects.select_related().all().values('projects__name','userprofile__username__email').order_by('userprofile__username__email')
        context['AssignedToUser'] = assigned

        return context


################################# #
# PROJECTS TO MEMBERS (EMPLOYEE)  #
################################# #

# LIST
# =================
class ProjectEmployeeList(ListView):
    model = Project
    template_name = 'apps/project/project_employee/project_employee_list.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectEmployeeList, self).get_context_data(**kwargs)

        context['project_info'] = Project.objects.get(id=self.kwargs['pk'], companies_id=self.kwargs['companies_id'])

        assigned = ProjectEmployee.objects.select_related().all().values('employees_id').filter(
            projects_id=self.kwargs['pk'], employees__companies_id=self.kwargs['companies_id'])
        ready = Employee.objects.exclude(id__in=[item['employees_id'] for item in assigned]).filter(
            companies_id=self.kwargs['companies_id'])
        context['EmployeeList'] = ready

        a = Employee.objects.all().values('id').filter(companies_id=self.kwargs['companies_id'])
        r = ProjectEmployee.objects.select_related().filter(employees_id__in=[item['id'] for item in a]).filter(
            projects_id=self.kwargs['pk'], employees__companies_id=self.kwargs['companies_id'])

        context['EmployeeAssigned'] = r

        return context


# CREATE
# ================
class ProjectEmployeeCreate(CreateView):
    model = ProjectEmployee
    form_class = ProjectEmployeeCreateForm
    template_name = 'apps/project/project_employee/project_employee_form.html'

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(ProjectEmployeeCreate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(ProjectEmployeeCreate, self).get_initial()

        # Enviamos projects y employess a formulario.
        initial.update(
            {
                'companies_id': self.kwargs['companies_id'],
                'projects_id': self.kwargs['project_id'],
                'projects': self.kwargs['project_id'],
                'employees': self.kwargs['pk']
            }
        )
        return initial

    def get_form_kwargs(self):
        kwargs = super(ProjectEmployeeCreate, self).get_form_kwargs()

        if self.kwargs['pk']:
            project_id = Project.objects.values('id').get(id=self.kwargs['project_id'])
            kwargs.update(
                {
                    'companies_id': self.kwargs['companies_id'],
                    'projects_id': self.kwargs['project_id'],
                    'employees': self.kwargs['pk'],
                    'employees_id': self.kwargs['pk']
                })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_employee_list',
                                kwargs={
                                    'pk': self.kwargs['project_id'],
                                    'companies_id': self.kwargs['companies_id'],
                                })
        else:
            return reverse_lazy('project:project_employee_list', args=(self.object.id,))


# UPDATE
# =======
class ProjectEmployeeUpdate(UpdateView):
    model = ProjectEmployee
    form_class = ProjectEmployeeUpdateForm
    template_name = 'apps/project/project_employee/project_employee_form.html'

    def form_valid(self, form):
        form.instance.update_user = self.request.user
        return super(ProjectEmployeeUpdate, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(ProjectEmployeeUpdate, self).get_form_kwargs()

        employee_id = ProjectEmployee.objects.values('id').get(id=self.kwargs['pk'])
        print('Employee ID UP', employee_id)

        kwargs.update(
            {
                'companies_id': self.kwargs['companies_id'],
                'projects_id': self.kwargs['project_id'],
                'employees': employee_id,
                'employees_id': self.kwargs['pk']
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_employee_list',
                                kwargs={
                                    'companies_id': self.kwargs['companies_id'],
                                    'pk': self.kwargs['project_id'],
                                })
        else:
            return reverse_lazy('project:project_employee_list', args=(self.object.id,))


# DELETE
# ================
class ProjectEmployeeDelete(DeleteView):
    model = ProjectEmployee
    template_name = 'apps/project/project_employee/project_employee_delete.html'

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_employee_list', kwargs={'pk': self.kwargs['project_id']})
        else:
            return reverse_lazy('project:project_employee_list', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        context = super(ProjectEmployeeDelete, self).get_context_data(**kwargs)
        context.update({'project_id': self.kwargs['project_id']})

        return context


# ########################## #
# Views: PROJECT CONTRACTOR  #
# ########################## #

# LIST
# ============
class ProjectContractorList(DetailView):
    model = Project
    template_name = 'apps/project/project_contractor/project_contractor_list.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectContractorList, self).get_context_data(**kwargs)

        context['project_info'] = Project.objects.all().get(id=self.kwargs['pk'])

        assigned = ProjectContractor.objects.all().values('contractors_id'). \
            filter(projects_id=self.kwargs['pk'], contractors__companies_id=self.kwargs['companies_id'])

        ready = Contractor.objects.exclude(id__in=[item['contractors_id'] for item in assigned]).filter(
            companies_id=self.kwargs['companies_id'])
        context['ContractorList'] = ready

        a = Contractor.objects.all().values('id')
        r = ProjectContractor.objects.select_related().filter(projects_id=self.kwargs['pk'],
                                                              contractors_id__in=[item['id'] for item in a]).filter(
            contractors__companies_id=self.kwargs['companies_id'])

        context['ContractorAssigned'] = r

        return context


# CREATE
# ============
class ProjectContractorCreate(CreateView):
    model = ProjectContractor
    form_class = ProjectContractorForm
    template_name = 'apps/project/project_contractor/project_contractor_form.html'

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(ProjectContractorCreate, self).form_valid(form)

    def get_initial(self):
        initial = super(ProjectContractorCreate, self).get_initial()

        # Enviamos projects y employess a formulario.
        initial.update(
            {
                'projects': self.kwargs['project_id'],
                'contractors': self.kwargs['pk']
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(ProjectContractorCreate, self).get_form_kwargs()
        kwargs.update(
            {
                'companies_id': self.kwargs['companies_id'],
                'projects_id': self.kwargs['project_id'],
                'contractors_id': self.kwargs['pk']
            })

        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ProjectContractorCreate, self).get_context_data(**kwargs)
        context.update(
            {
                'project_id': self.kwargs['project_id'],
                'companies_id': self.kwargs['companies_id'],

            })

        return context

    def get_success_url(self, **kwargs):
        print(self.kwargs)

        if kwargs != None:
            print('CUAL ES: ', self.kwargs)
            return reverse_lazy('project:project_contractor_list', kwargs={
                'pk': self.kwargs['project_id'],
                'companies_id': self.kwargs['companies_id'],

            })
        else:
            return reverse_lazy('project:project_contractor_list', args=(self.object.id,))


# UPDATE
# =============
class ProjectContractorUpdate(UpdateView):
    model = ProjectContractor
    form_class = ProjectContractorForm
    template_name = 'apps/project/project_contractor/project_contractor_form.html'

    def form_valid(self, form):
        form.instance.update_user = self.request.user
        return super(ProjectContractorUpdate, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(ProjectContractorUpdate, self).get_form_kwargs()
        a = ProjectContractor.objects.get(id=self.kwargs['pk'])
        print(a)
        kwargs.update(
            {
                'companies_id': self.kwargs['companies_id'],
                'projects_id': self.kwargs['project_id'],
                'contractors_id': self.kwargs['pk']
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_contractor_list', kwargs={
                'pk': self.kwargs['project_id'],
                'companies_id': self.kwargs['companies_id'],

            })
        else:
            return reverse_lazy('project:project_contractor_list', args=(self.object.id,))


# DELETE
# ============
class ProjectContractorDelete(DeleteView):
    model = ProjectContractor
    template_name = 'apps/project/project_contractor/project_contractor_delete.html'

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_contractor_list', kwargs={'pk': self.kwargs['project_id']})
        else:
            return reverse_lazy('project:project_contractor_list', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        context = super(ProjectContractorDelete, self).get_context_data(**kwargs)
        context.update({
            'project_id': self.kwargs['project_id']
        })

        return context


# ##################################
# Views: MODULE MAN DAY SCHEDULE  ##
# ##################################

# LIST
# ================
class ProjectManDayScheduleList(ListView):
    model = CostCode
    template_name = 'apps/project/project_manday_schedule/project_manday_schedule_list.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectManDayScheduleList, self).get_context_data(**kwargs)

        # OJO cost_code_ready = CostCode.objects.exclude(id__in=lista)
        cost_code_ready = CostCode.objects.all().filter(category_cost_codes__companies_id=self.kwargs['companies_id'])
        context['CodeCostList'] = cost_code_ready

        # Man Days Schedule Selections
        project_man_days_list = ProjectManDaySchedule.objects.select_related('cost_codes', 'project_floor_phases'). \
            all().filter(projects_id=self.kwargs['pk']).order_by('project_floor_phases')

        context['projectManDayScheduleSelected'] = project_man_days_list

        # Filter Project for PK
        project_list = Project.objects.get(id=self.kwargs['pk'])
        context['projectSelected'] = project_list

        return context


# CREATE
# ==================
class ProjectManDayScheduleCreate(CreateView):
    model = ProjectManDaySchedule
    form_class = ProjectManDayScheduleForm
    template_name = 'apps/project/project_manday_schedule/project_mds_form.html'

    def form_valid(self, form):
        if form.is_valid():
            form.instance.create_user = self.request.user
        return super(ProjectManDayScheduleCreate, self).form_valid(form)

    def get_initial(self):
        initial = super(ProjectManDayScheduleCreate, self).get_initial()

        cost_code_selected = CostCode.objects.all().annotate(). \
            values('id', 'name', 'unit', 'code_labor', 'code_material', \
                   'production', 'regular_cost', 'overtime_cost', 'wage_cost', \
                   'wage_overtime_cost'). \
            filter(id=self.kwargs['pk'])

        project_floor_phases = ProjectFloorPhase.objects.filter(projects_id=self.kwargs['project_id']).all()

        if project_floor_phases.exists():
            project_floor_phases_data = project_floor_phases
        else:
            project_floor_phases_data = 0

        initial.update(
            {
                'projects': Project.objects.get(id=self.kwargs['project_id']),
                'name': cost_code_selected[0].pop('name'),
                'cost_codes': cost_code_selected[0].pop('id'),
                'unit': cost_code_selected[0].pop('unit'),
                'code_labor': cost_code_selected[0].pop('code_labor'),
                'code_material': cost_code_selected[0].pop('code_material'),
                'production': cost_code_selected[0].pop('production'),
                'regular_cost': cost_code_selected[0].pop('regular_cost'),
                'overtime_cost': cost_code_selected[0].pop('overtime_cost'),
                'wage_cost': cost_code_selected[0].pop('wage_cost'),
                'wage_overtime_cost': cost_code_selected[0].pop('wage_overtime_cost')
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(ProjectManDayScheduleCreate, self).get_form_kwargs()
        cost_code_selected = CostCode.objects.all().annotate(). \
            values('id', 'name', 'unit', 'code_labor', 'code_material', \
                   'production', 'regular_cost', 'overtime_cost', 'wage_cost', \
                   'wage_overtime_cost'). \
            filter(id=self.kwargs['pk'])

        kwargs.update(
            {
                'project_floor_phases_id': self.kwargs['project_id'],
                'project_id': self.kwargs['project_id'],
                'cost_codes_id': cost_code_selected[0].pop('id'),
                'action': 'create'

            })

        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ProjectManDayScheduleCreate, self).get_context_data(**kwargs)

        project_info = Project.objects.get(id=self.kwargs['project_id'])
        context['project_info'] = project_info

        context.update({'project_id': self.kwargs['project_id']})

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_manday_schedule_list', kwargs={
                'pk': self.kwargs['project_id'],
                'companies_id': self.kwargs['companies_id']
            })
        else:
            return reverse_lazy('project:project_manday_schedule_list', args=(self.object.id,))


# UPDATE
# =============
class ProjectManDayScheduleUpdate(UpdateView):
    model = ProjectManDaySchedule
    form_class = ProjectManDayScheduleForm
    template_name = 'apps/project/project_manday_schedule/project_mds_form.html'

    def form_valid(self, form):
        form.instance.update_user = self.request.user
        return super(ProjectManDayScheduleUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ProjectManDayScheduleUpdate, self).get_context_data(**kwargs)
        project_info = Project.objects.get(id=self.kwargs['project_id'])
        context['project_info'] = project_info
        context.update({'project_id': self.kwargs['project_id']})

        return context

    def get_form_kwargs(self):
        kwargs = super(ProjectManDayScheduleUpdate, self).get_form_kwargs()

        project_man_days_schedule_selected = ProjectManDaySchedule.objects.all().annotate(). \
            values('id', 'name', 'unit', 'code_labor', 'code_material', \
                   'production', 'regular_cost', 'overtime_cost', 'wage_cost', \
                   'wage_overtime_cost', 'project_floor_phases_id', 'cost_codes_id'). \
            filter(projects_id=self.kwargs['project_id'], id=self.kwargs['pk'])

        print(project_man_days_schedule_selected[0].pop('code_labor'))

        kwargs.update(
            {
                'project_id': self.kwargs['project_id'],
                'cost_codes_id': project_man_days_schedule_selected[0].pop('cost_codes_id'),
                'project_floor_phases_id': project_man_days_schedule_selected[0].pop('project_floor_phases_id'),
                'action': 'update'
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_manday_schedule_list', kwargs={
                'pk': self.kwargs['project_id'],
                'companies_id': self.kwargs['companies_id']
            })
        else:
            return reverse_lazy('project:project_manday_schedule_list', args=(self.object.id,))


# DELETE
# ====================
class ProjectManDayScheduleDelete(DeleteView):
    model = ProjectManDaySchedule
    template_name = 'apps/project/project_manday_schedule/project_mds_delete.html'

    def get_success_url(self, **kwargs):

        if kwargs != None:
            return reverse_lazy('project:project_manday_schedule_list', kwargs={'pk': self.kwargs['project_id']})
        else:
            return reverse_lazy('project:project_manday_schedule_list', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        context = super(ProjectManDayScheduleDelete, self).get_context_data(**kwargs)
        context.update({'project_id': self.kwargs['project_id']})
        return context


# ##################################################
# Views: MODULE PROJECT CONTRACTOR EMPLOYEE FIELD  #
# ##################################################

# LIST
# ================
class ProjectContractorEmployeeFieldList(ListView):
    model = ProjectContractorEmployeeField
    template_name = 'apps/project/project_contractor_employee_field/project_contractor_employee_field_list.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectContractorEmployeeFieldList, self).get_context_data(**kwargs)

        # OJO Employee Assigned to Contractors filter and get only Employee
        list = ProjectContractorEmployeeField.objects.all(). \
            annotate().values('contractor_employees_field_id'). \
            filter(project_contractors_id=self.kwargs['pk'])

        project_contractor_employee_list = ContractorEmployeeField.objects. \
            select_related().exclude(id__in=list). \
            filter(contractors_id=self.kwargs['contractors_id'])

        context['projectContractorEmployeeFieldList'] = project_contractor_employee_list

        # Empployee Assigned
        list_assigned = ProjectContractorEmployeeField.objects.select_related().all().filter(
            project_contractors_id=self.kwargs['pk'])
        context['ProjectContractorAssignedEmployeeField'] = list_assigned

        # Send Informacion
        context['information'] = ProjectContractor.objects.select_related('projects').all().get(id=self.kwargs['pk'])

        # Send ID
        # context['project_contractor_id'] = self.kwargs

        context['filter_models'] = ({
            'project_contractor_id': self.kwargs['pk'],
            'contractors_id': self.kwargs['contractors_id']
        })

        return context


# CREATE
# ================
class ProjectContractorEmployeeFieldCreate(CreateView):
    model = ProjectContractorEmployeeField
    form_class = ProjectContractorEmployeeFieldForm
    template_name = 'apps/project/project_contractor_employee_field/project_contractor_employee_field_form.html'

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(ProjectContractorEmployeeFieldCreate, self).form_valid(form)

    def get_initial(self):
        initial = super(ProjectContractorEmployeeFieldCreate, self).get_initial()
        initial.update(
            {
                'project_contractors': self.kwargs['project_contractor_id'],
                'contractor_employees_field': self.kwargs['pk'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(ProjectContractorEmployeeFieldCreate, self).get_form_kwargs()
        kwargs.update(
            {
                'project_contractors': self.kwargs['project_contractor_id'],
                'contractor_employees_field': self.kwargs['pk'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_contractor_employee_field_list',
                                kwargs={
                                    'pk': self.kwargs['project_contractor_id'],
                                    'contractors_id': self.kwargs['contractor_id']
                                }
                                )
        else:
            return reverse_lazy('project:project_contractor_employee_field_list',
                                args=(self.object.id, self.object.contractors_id))


# UPDATE
# ==============

class ProjectContractorEmployeeFieldUpdate(UpdateView):
    model = ProjectContractorEmployeeField
    form_class = ProjectContractorEmployeeFieldForm
    template_name = 'apps/project/project_contractor_employee_field/project_contractor_employee_field_form.html'

    def form_valid(self, form):
        form.instance.update_user = self.request.user
        return super(ProjectContractorEmployeeFieldUpdate, self).form_valid(form)

    def get_success_url(self, **kwargs):
        print(self.kwargs)

        if kwargs != None:
            return reverse_lazy('project:project_contractor_list', kwargs={'pk': self.kwargs['project_id']})
        else:
            return reverse_lazy('project:project_contractor_list', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        context = super(ProjectContractorEmployeeFieldUpdate, self).get_context_data(**kwargs)
        context.update({'project_id': self.kwargs['project_id']})
        return context


# ####################################
# Views: MODULE PROJECT FLOOR PHASE  #
# ####################################

# LIST
# ============
class ProjectFloorPhaseList(ListView):
    model = ProjectFloorPhase
    template_name = 'apps/project/project_floor_phase/project_floor_phase_list.html'
    success_url = reverse_lazy('project:project_detail')

    # Pasar parametros a la plantilla usando context
    def get_context_data(self, **kwargs):
        context = super(ProjectFloorPhaseList, self).get_context_data(**kwargs)

        # Floor Phases List
        project_floor_phases = ProjectFloorPhase.objects.filter(projects_id=self.kwargs['pk'],
                                                                projects__companies_id=self.kwargs['companies_id'])

        context['ProjectFloorPhaseList'] = project_floor_phases

        project_info = Project.objects.get(id=self.kwargs['pk'], companies_id=self.kwargs['companies_id'])

        context['project_info'] = project_info

        return context


# CREATE
# ============
class ProjectFloorPhaseCreate(CreateView):
    model = ProjectFloorPhase
    form_class = ProjectFloorPhaseForm
    template_name = 'apps/project/project_floor_phase/project_floor_phase_form.html'

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(ProjectFloorPhaseCreate, self).form_valid(form)

    def get_initial(self):
        initial = super(ProjectFloorPhaseCreate, self).get_initial()
        initial.update(
            {'projects': self.kwargs['pk']})

        return initial

    def get_form_kwargs(self):
        kwargs = super(ProjectFloorPhaseCreate, self).get_form_kwargs()

        kwargs.update(
            {
                'companies_id': self.kwargs['companies_id'],
                'projects_id': self.kwargs['pk'],

            })

        return kwargs

    def get_context_data(self, **kwargs):
        context = super(ProjectFloorPhaseCreate, self).get_context_data(**kwargs)
        project_info = Project.objects.get(id=self.kwargs['pk'], companies_id=self.kwargs['companies_id'])
        context['project_info'] = project_info

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_floor_phase_list', kwargs={
                'pk': self.kwargs['pk'],
                'companies_id': self.kwargs['companies_id']
            })
        else:
            return reverse_lazy('project:project_floor_phase_list', args=(self.object.id,))


# UPDATE
# ===========
class ProjectFloorPhaseUpdate(UpdateView):
    model = ProjectFloorPhase
    form_class = ProjectFloorPhaseForm
    template_name = 'apps/project/project_floor_phase/project_floor_phase_form.html'

    def form_valid(self, form):
        form.instance.update_user = self.request.user
        return super(ProjectFloorPhaseUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ProjectFloorPhaseUpdate, self).get_context_data(**kwargs)

        project_info = Project.objects.get(id=self.kwargs['project_id'], companies_id=self.kwargs['companies_id'])
        context['project_info'] = project_info

        return context

    def get_initial(self):
        initial = super(ProjectFloorPhaseUpdate, self).get_initial()

        initial.update(
            {'projects_id': self.kwargs['project_id']})

        return initial

    def get_form_kwargs(self):
        kwargs = super(ProjectFloorPhaseUpdate, self).get_form_kwargs()

        kwargs.update(
            {
                'companies_id': self.kwargs['companies_id'],
                'projects_id': self.kwargs['project_id'],

            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_floor_phase_list',
                                kwargs={'pk': self.kwargs['project_id'], 'companies_id': self.kwargs['companies_id']})
        else:
            return reverse_lazy('project:project_floor_phase_list', args=(self.object.id,))


# DELETE
# ==============
class ProjectFloorPhaseDelete(DeleteView):
    model = ProjectFloorPhase
    template_name = 'apps/project/project_floor_phase/project_floor_phase_delete.html'

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_contractor_list', kwargs={'pk': self.kwargs['project_id']})
        else:
            return reverse_lazy('project:project_contractor_list', args=(self.object.id,))

    def get_context_data(self, **kwargs):
        context = super(ProjectFloorPhaseDelete, self).get_context_data(**kwargs)
        context.update(
            {
                'project_id': self.kwargs['project_id']
            })
        return context


# ########################################
# Views: MODULE PROJECT FLOOR PHASE AREA #
# ########################################

# CREATE
# ===========
class ProjectFloorPhaseAreaCreate(CreateView):
    model = ProjectFloorPhaseArea
    form_class = ProjectFloorPhaseAreaForm
    template_name = 'apps/project/project_floor_phase_area/project_floor_phase_area_form.html'

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(ProjectFloorPhaseAreaCreate, self).form_valid(form)

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('project:project_floor_phase_list', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('project:project_floor_phase_list', args=(self.object.id,))

    def get_initial(self):
        initial = super(ProjectFloorPhaseAreaCreate, self).get_initial()
        initial.update(
            {
                'projects': self.kwargs['pk']
            }
        )
        return initial

    def get_context_data(self, **kwargs):
        context = super(ProjectFloorPhaseAreaCreate, self).get_context_data(**kwargs)
        context['project_id'] = self.kwargs['pk']

        return context


########################################################################################################################
# REPORT AREA
# TESTING REPORTLAB
########################################################################################################################
class PageNumCanvas(canvas.Canvas):
    """
    http://code.activestate.com/recipes/546511-page-x-of-y-with-reportlab/
    http://code.activestate.com/recipes/576832/
    """

    # ----------------------------------------------------------------------
    def __init__(self, *args, **kwargs):
        """Constructor"""
        canvas.Canvas.__init__(self, *args, **kwargs)
        self.pages = []

    # ----------------------------------------------------------------------
    def showPage(self):
        """
        On a page break, add information to the list
        """
        self.pages.append(dict(self.__dict__))
        self._startPage()

    # ----------------------------------------------------------------------
    def save(self):
        """
        Add the page number to each page (page x of y)
        """
        page_count = len(self.pages)

        for page in self.pages:
            self.__dict__.update(page)
            self.draw_page_number(page_count)
            canvas.Canvas.showPage(self)

        canvas.Canvas.save(self)

    # ----------------------------------------------------------------------
    def draw_page_number(self, page_count):
        """
        Add the page number
        """
        page = "Page %s of %s" % (self._pageNumber, page_count)
        self.setFont("Helvetica", 9)
        self.drawRightString(195 * mm, 272 * mm, page)


# ----------------------------------------------------------------------
def CreateMultiPage(View):
    """
    Create a multi-page document
    """
    doc = SimpleDocTemplate("doc_page_num_v2.pdf", pagesize=letter,
                            rightMargin=72, leftMargin=72,
                            topMargin=72, bottomMargin=18)

    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))

    Story = []

    magName = "Pythonista"
    issueNum = 12
    subPrice = "99.00"
    limitedDate = "03/05/2010"
    freeGift = "tin foil hat"
    full_name = "Marvin Jones"
    address_parts = ["411 State St.", "Reno, NV 80158"]

    for page in range(5):
        # Create return address
        ptext = '<font size=12>%s</font>' % full_name
        Story.append(Paragraph(ptext, styles["Normal"]))

        for part in address_parts:
            ptext = '<font size=12>%s</font>' % part.strip()
            Story.append(Paragraph(ptext, styles["Normal"]))

        Story.append(Spacer(1, 12))
        ptext = '<font size=12>Dear %s:</font>' % full_name.split()[0].strip()
        Story.append(Paragraph(ptext, styles["Normal"]))
        Story.append(Spacer(1, 12))

        ptext = """<font size=12>We would like to welcome you to our subscriber base 
        for %s Magazine! You will receive %s issues at the excellent introductory 
        price of $%s. Please respond by %s to start receiving your subscription 
        and get the following free gift: %s.</font>"""
        ptext = ptext % (magName, issueNum, subPrice, limitedDate, freeGift)
        Story.append(Paragraph(ptext, styles["Justify"]))
        Story.append(Spacer(1, 12))

        ptext = '<font size=12>Thank you very much and we look forward to serving you.</font>'
        Story.append(Paragraph(ptext, styles["Justify"]))
        Story.append(Spacer(1, 12))
        ptext = '<font size=12>Sincerely,</font>'
        Story.append(Paragraph(ptext, styles["Normal"]))
        Story.append(Spacer(1, 48))
        ptext = '<font size=12>Ima Sucker</font>'
        Story.append(Paragraph(ptext, styles["Normal"]))
        Story.append(Spacer(1, 12))
        Story.append(PageBreak())

    doc.build(Story, canvasmaker=PageNumCanvas)


class ReporteEmpleadoPDF(View):
    def addPageNumber(canvas, doc):
        """
        Add the page number
        """
        page_num = canvas.getPageNumber()
        text = "Page #%s" % page_num
        canvas.drawRightString(200 * mm, 20 * mm, text)

    # ----------------------------------------------------------------------
    def createMultiPage(self):
        """
        Create a multi-page document
        """
        doc = SimpleDocTemplate("doc_page_num.pdf", pagesize=letter,
                                rightMargin=72, leftMargin=72,
                                topMargin=72, bottomMargin=18)
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))

        Story = []

        magName = "Pythonista"
        issueNum = 12
        subPrice = "99.00"
        limitedDate = "03/05/2010"
        freeGift = "tin foil hat"
        full_name = "Marvin Jones"
        address_parts = ["411 State St.", "Reno, NV 80158"]

        for page in range(5):
            # Create return address
            ptext = '<font size=12>%s</font>' % full_name
            Story.append(Paragraph(ptext, styles["Normal"]))
            for part in address_parts:
                ptext = '<font size=12>%s</font>' % part.strip()
                Story.append(Paragraph(ptext, styles["Normal"]))

            Story.append(Spacer(1, 12))
            ptext = '<font size=12>Dear %s:</font>' % full_name.split()[0].strip()
            Story.append(Paragraph(ptext, styles["Normal"]))
            Story.append(Spacer(1, 12))

            ptext = """<font size=12>We would like to welcome you to our subscriber base 
            for %s Magazine! You will receive %s issues at the excellent introductory 
            price of $%s. Please respond by %s to start receiving your subscription 
            and get the following free gift: %s.</font>"""
            ptext = ptext % (magName, issueNum, subPrice, limitedDate, freeGift)
            Story.append(Paragraph(ptext, styles["Justify"]))
            Story.append(Spacer(1, 12))

            ptext = '<font size=12>Thank you very much and we look forward to serving you.</font>'
            Story.append(Paragraph(ptext, styles["Justify"]))
            Story.append(Spacer(1, 12))
            ptext = '<font size=12>Sincerely,</font>'
            Story.append(Paragraph(ptext, styles["Normal"]))
            Story.append(Spacer(1, 48))
            ptext = '<font size=12>Ima Sucker</font>'
            Story.append(Paragraph(ptext, styles["Normal"]))
            Story.append(Spacer(1, 12))
            Story.append(PageBreak())
            doc.build(Story, onFirstPage=self.addPageNumber, onLaterPages=self.addPageNumber)


class ReportePersonasPDF(View):

    def cabecera(self, pdf):
        # Utilizamos el archivo logo_django.png que está guardado en la carpeta media/imagenes
        company_logo = settings.MEDIA_ROOT + 'images/samson.png'

        # Definimos el tamaño de la imagen a cargar y las coordenadas correspondientes
        pdf.drawImage(company_logo, 40, 750, 120, 90, preserveAspectRatio=True)

        # Establecemos el tamaño de letra en 16 y el tipo de letra Helvetica

        pdf.setFont("Helvetica", 16)

        # Dibujamos una cadena en la ubicación X,Y especificada
        pdf.drawString(230, 790, u"Dynamic Contracting Inc.")
        pdf.setFont("Helvetica", 14)
        pdf.drawString(200, 770, u"User Profile")

    @staticmethod
    def _header_footer(canvas, doc):
        # Save the state of our canvas so we can draw on it
        canvas.saveState()
        styles = getSampleStyleSheet()

        # Header
        header = Paragraph('This is a multi-line header.  It goes on every page.   ' * 5, styles['Normal'])
        w, h = header.wrap(doc.width, doc.topMargin)
        header.drawOn(canvas, doc.leftMargin, doc.height + doc.topMargin - h)

        # Footer
        footer = Paragraph('This is a multi-line footer.  It goes on every page.   ' * 5, styles['Normal'])
        w, h = footer.wrap(doc.width, doc.bottomMargin)
        footer.drawOn(canvas, doc.leftMargin, h)

        # Release the canvas
        canvas.restoreState()

    def tabla(self, pdf, y):
        # Creamos una tupla de encabezados para neustra tabla
        encabezados = ('DNI', 'Nombre', 'Apellido Paterno')

        # Creamos una lista de tuplas que van a contener a las personas
        detalles = [(user_list.companies, user_list.username, user_list.role) for user_list in
                    UserProfile.objects.all()]

        # Establecemos el tamaño de cada una de las columnas de la tabla
        detalle_orden = Table([encabezados] + detalles, colWidths=[5 * cm, 5 * cm, 5 * cm])

        # Aplicamos estilos a las celdas de la tabla
        detalle_orden.setStyle(
            TableStyle(
                [
                    # La primera fila(encabezados) va a estar centrada
                    ('ALIGN', (0, 0), (2, 0), 'CENTER'),

                    # Los bordes de todas las celdas serán de color negro y con un grosor de 1
                    ('GRID', (0, 0), (-1, -1), 1, colors.black),

                    # El tamaño de las letras de cada una de las celdas será de 10
                    ('FONTSIZE', (0, 0), (-1, -1), 10),
                ]
            ))

        # Establecemos el tamaño de la hoja que ocupará la tabla
        detalle_orden.wrapOn(pdf, 800, 600)

        # Definimos la coordenada donde se dibujará la tabla
        detalle_orden.drawOn(pdf, 60, y)

    def get(self, request, *args, **kwargs):
        magName = "Pythonista"
        issueNum = 12
        subPrice = "99.00"
        limitedDate = "03/05/2010"
        freeGift = "tin foil hat"
        full_name = "Marvin Jones"
        address_parts = ["411 State St.", "Reno, NV 80158"]

        for page in range(5):

            # Create the HrrpResponse headers with PDF
            response = HttpResponse(content_type='application/pdf')
            # response['Content-Disposition'] = 'attachment; filename=Eche-student-report.pdf'

            # Create the PDF object, using the BytesIO object as its "file."
            buffer = BytesIO()
            c = canvas.Canvas(buffer, pagesize=A4)

            c.setTitle("Example Reportlab")

            # Header
            c.setLineWidth(.3)
            c.setFont('Helvetica', 22)
            c.drawString(30, 750, magName)  # (x de der a izq,y de abajo hacia arri,texto)

            c.setFont('Helvetica', 12)
            c.drawString(30, 735, 'Employee Report')

            c.setFont('Helvetica-Bold', 12)
            c.drawString(480, 750, limitedDate)

            # Start X, height end y height
            c.line(460, 747, 560, 747)

            c.setTitle('Title')
            c.setSubject('Title 2')
            c.setAuthor('Alex G. Hurtado')
            c.setCreator('SAMSON')

            # Students table
            students = [
                {'#': '1',
                 'name': 'Pepe Grillo Pepe Grillo Pepe Grillo Pepe Grillo Pepe Grillo Pepe Grillo Pepe Grillo ',
                 'b1': '4.4', 'b2': '4.4', 'b3': '4.4', 'total': '4.4'},
                {'#': '2', 'name': 'Gepetto', 'b1': '4.5', 'b2': '4.5', 'b3': '4.5', 'total': '4.5'},
                {'#': '3', 'name': 'El Gato', 'b1': '4.6', 'b2': '4.6', 'b3': '4.6', 'total': '4.6'},
                {'#': '4', 'name': 'El Zorro', 'b1': '4.7', 'b2': '4.7', 'b3': '4.7', 'total': '4.7'},
                {'#': '5', 'name': 'Pinocho', 'b1': '4.8', 'b2': '4.8', 'b3': '4.8', 'total': '4.8'},
                {'#': '6', 'name': 'Ada Madrina', 'b1': '4.9', 'b2': '4.9', 'b3': '4.9', 'total': '4.9'},
                {'#': '7', 'name': 'El Burro', 'b1': '4.95', 'b2': '4.95', 'b3': '4.95', 'total': '4.95'},
            ]

            # Table header
            styles = getSampleStyleSheet()

            # Configure style and word wrap
            styleBH = styles['BodyText']
            styleBH.alignment = TA_CENTER
            styleBH.fontSize = 10
            styles.wordWrap = 'LTR'

            numero = Paragraph('''No.''', styleBH)
            alumno = Paragraph('''Alumno''', styleBH)

            b1 = Paragraph('''BIM1''', styleBH)
            b2 = Paragraph('''BIM2''', styleBH)
            b3 = Paragraph('''BIM3''', styleBH)
            total = Paragraph('''TOTAL''', styleBH)

            data = []
            data.append([numero, alumno, b1, b2, b3, total])

            # Table
            stylesN = styles['BodyText']
            styleBH.alignment = TA_CENTER
            styleBH.fontSize = 7

            high = 650
            for student in students:
                this_student = [student['#'], Paragraph(student['name'], styles['Normal']), student['b1'],
                                student['b2'], student['b3'],
                                student['total']]

                data.append(this_student)
                high = high - 18

            # Table size
            width, height = A4

            table = Table(data, colWidths=[1.9 * cm, 9.5 * cm, 1.9 * cm, 1.9 * cm, 1.9 * cm, 1.9 * cm])

            table.setStyle(TableStyle([
                ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                ('BOX', (0, 0), (-1, -1), 2.25, colors.black),
            ]))

            # Pdf size
            table.wrapOn(c, width, height)
            table.drawOn(c, 30, high)

            c.setFont('Helvetica', 12)
            c.drawString(230, 35, 'Employee Report')

            c.showPage()  # Save page

            # save pdf
            c.save()

            # get the value of BytesIO buffer and write response
            pdf = buffer.getvalue()
            buffer.close()

            response.write(pdf)

        return response


class GeneratePdf(View):

    def get(self, request, *args, **kwargs):
        start_date = request.GET.get('dates')
        end_date = request.GET.get('datee')
        company = Company.objects.get(id=self.kwargs['companies_id'])
        project = Project.objects.get(id=self.kwargs['pk'])

        data = DailyProduce.objects. \
            values('project_contractor_employees_field__project_contractors__contractors__name'). \
            filter(project_contractor_employees_field__project_contractors__contractors__companies__id=self.kwargs[
            'companies_id']). \
            filter(project_manday_schedules__projects_id=self.kwargs['pk'], labor_date__range=(start_date, end_date)). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            hours=Sum('work_hours')
        )

        total_cost = data.all().aggregate(total_cost=Sum('cost'))
        total_hours = data.all().aggregate(total_hours=Sum('hours'))

        start_date = dt.datetime.strptime(start_date, "%Y-%m-%d")
        end_date = dt.datetime.strptime(end_date, "%Y-%m-%d")
        report_date = datetime.now()

        data = {
            'contractors': data,
            'total_cost': total_cost,
            'total_hours': total_hours,
            'project_info': project,
            'company_info': company,
            'start_date': start_date,
            'end_date': end_date,
            'report_date': report_date,
        }

        pdf = render_to_pdf('apps/project/project_reports/project_contractors_report.html', data)

        return HttpResponse(pdf, content_type='application/pdf')


class ProjectManDayScheduleReport(View):

    def get(self, request, *args, **kwargs):
        company = Company.objects.get(id=self.kwargs['companies_id'])
        project = Project.objects.get(id=self.kwargs['pk'])

        cost_code_ready = CostCode.objects.all().filter(category_cost_codes__companies_id=1)
        # Man Days Schedule Selections
        data = ProjectManDaySchedule.objects.select_related('cost_codes', 'project_floor_phases'). \
            all().filter(projects_id=self.kwargs['pk']).order_by('project_floor_phases')

        total_man_days = data.all().aggregate(total_man_days=Sum('man_days_allotted'))
        total_regular_cost = data.all().aggregate(total_regular_cost=Sum('regular_cost'))
        total_wage_cost = data.all().aggregate(total_wage_cost=Sum('wage_cost'))

        # total_hours = data.all().aggregate(total_hours=Sum('hours'))

        report_date = datetime.now()

        start_date = project.start_date
        finish_date = project.finish_date

        data = {
            'mandays': data,
            'total_man_days': total_man_days,
            'total_regular_cost': total_regular_cost,
            'total_wage_cost': total_wage_cost,
            'project_info': project,
            'company_info': company,
            'start_date': start_date,
            'finish_date': finish_date,
            'report_date': report_date,
        }

        pdf = render_to_pdf('apps/project/project_reports/project_manday_schedule_report.html', data)

        return HttpResponse(pdf, content_type='application/pdf')


# MAN DAYS FLOOR AND LABOR REPORT
class ProjectManDayScheduleLaborReport(View):

    def get(self, request, *args, **kwargs):
        from datetime import datetime
        company = Company.objects.get(id=self.kwargs['companies_id'])
        project = Project.objects.get(id=self.kwargs['pk'])

        start_date = self.request.GET.get('dates')
        finish_date = self.request.GET.get('datee')


        project = Project.objects.get(id=self.kwargs['pk'])

        if self.request.GET.get('dates') == True:

            data = DailyProduce.objects. \
                filter(
                project_manday_schedules__projects_id=self.kwargs['pk'],
                labor_date__range=(start_date, finish_date)
            ). \
                values('project_manday_schedules__project_floor_phases__name',
                       'project_manday_schedules__cost_codes__name',
                       'project_manday_schedules__unit',
                       'project_manday_schedules__code_labor',
                       'project_manday_schedules__code_material',
                       'project_manday_schedules__production',
                       'project_manday_schedules__man_days_allotted', 'project_manday_schedules__regular_cost',
                       'project_manday_schedules__wage_cost'). \
                annotate(
                cost=Sum(
                    Case(
                        When(project_manday_schedules__projects__is_wage_scale=False,
                             then=F('work_hours') * F('project_manday_schedules__regular_cost')
                             ),
                        When(project_manday_schedules__projects__is_wage_scale=True,
                             then=F('work_hours') * F('project_manday_schedules__wage_cost')
                             )
                    )
                ),
                estimate_cost=Sum(
                    Case(
                        When(project_manday_schedules__projects__is_wage_scale=False,
                             then=F('project_manday_schedules__man_days_allotted') * F(
                                 'project_manday_schedules__regular_cost') * 8
                             ),
                        When(project_manday_schedules__projects__is_wage_scale=True,
                             then=F('project_manday_schedules__man_days_allotted') * F(
                                 'project_manday_schedules__wage_cost') * 8
                             )
                    )
                ),
                man_days_spent=Sum('work_hours'),
                man_days_remaining=((F('project_manday_schedules__man_days_allotted') * 8) - Sum('work_hours')) / 8,
                project_production=Sum('production')). \
                order_by('project_manday_schedules__cost_codes__name')
        else:
            data = DailyProduce.objects. \
                filter(
                project_manday_schedules__projects_id=self.kwargs['pk']
            ). \
                values('project_manday_schedules__project_floor_phases__name',
                       'project_manday_schedules__cost_codes__name',
                       'project_manday_schedules__unit',
                       'project_manday_schedules__code_labor',
                       'project_manday_schedules__code_material',
                       'project_manday_schedules__production',
                       'project_manday_schedules__man_days_allotted', 'project_manday_schedules__regular_cost',
                       'project_manday_schedules__wage_cost'). \
                annotate(
                cost=Sum(
                    Case(
                        When(project_manday_schedules__projects__is_wage_scale=False,
                             then=F('work_hours') * F('project_manday_schedules__regular_cost')
                             ),
                        When(project_manday_schedules__projects__is_wage_scale=True,
                             then=F('work_hours') * F('project_manday_schedules__wage_cost')
                             )
                    )
                ),
                estimate_cost=Sum(
                    Case(
                        When(project_manday_schedules__projects__is_wage_scale=False,
                             then=F('project_manday_schedules__man_days_allotted') * F(
                                 'project_manday_schedules__regular_cost') * 8
                             ),
                        When(project_manday_schedules__projects__is_wage_scale=True,
                             then=F('project_manday_schedules__man_days_allotted') * F(
                                 'project_manday_schedules__wage_cost') * 8
                             )
                    )
                ),
                man_days_spent=Sum('work_hours'),
                man_days_remaining=((F('project_manday_schedules__man_days_allotted') * 8) - Sum('work_hours')) / 8,
                project_production=Sum('production')). \
                order_by('project_manday_schedules__cost_codes__name')

        report_date = datetime.now()

        total_man_days = data.all().aggregate(total_man_days=Sum('project_manday_schedules__man_days_allotted'))
        total_regular_cost = data.all().aggregate(total_regular_cost=Sum('project_manday_schedules__regular_cost'))
        total_wage_cost = data.all().aggregate(total_wage_cost=Sum('project_manday_schedules__wage_cost'))
        total_man_days_spent = data.all().aggregate(total_man_days_spent=Sum('man_days_spent'))
        total_cost = data.all().aggregate(total_cost=Sum('cost'))
        total_estimate_cost = data.all().aggregate(total_estimate_cost=Sum('estimate_cost'))

        data = {
            'mandays': data,
            'project_info': project,
            'company_info': company,
            'start_date': datetime.strptime(start_date, '%Y-%m-%d'),
            'finish_date': datetime.strptime(finish_date, '%Y-%m-%d'),
            'report_date': report_date,
            'total_man_days': total_man_days,
            'total_man_days_spent': total_man_days_spent,
            'total_cost': total_cost,
            'total_estimate_cost': total_estimate_cost
        }
        print(data)

        pdf = render_to_pdf('apps/project/project_reports/project_manday_schedule_labor_report.html', data)

        return HttpResponse(pdf, content_type='application/pdf')


class ProjectActivityLaborReport(View):

    def get(self, request, *args, **kwargs):
        from datetime import datetime
        company = Company.objects.get(id=self.kwargs['companies_id'])
        project = Project.objects.get(id=self.kwargs['pk'])

        start_date = self.request.GET.get('dates')
        finish_date = self.request.GET.get('datee')

        project = Project.objects.get(id=self.kwargs['pk'])

        data = DailyProduce.objects.all(). \
            select_related().filter(project_manday_schedules__projects_id=self.kwargs['pk']).order_by('-created_at'). \
            filter(labor_date__range=(start_date, finish_date))

        report_date = datetime.now()

        data = {
            'projectDailyProduce': data,
            'project_info': project,
            'company_info': company,
            'start_date': datetime.strptime(start_date, '%Y-%m-%d'),
            'finish_date': datetime.strptime(finish_date, '%Y-%m-%d'),
            'report_date': report_date,

        }

        pdf = render_to_pdf('apps/project/project_reports/project_activity_labor_report.html', data)

        return HttpResponse(pdf, content_type='application/pdf')


def load_cities(request):
    country_id = request.GET.get('country')
    return render(request, 'apps/project/project_daily_produce/dropdown_list_options.html', {'cities': 1})