from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from apps.company.forms import CompanyForm, CategoryProjectForm, CategoryEmployeeForm, CategoryCostCodeForm, CostCodeForm, CategoryEmployeeFieldForm
from apps.company.models import Company, CategoryProject, CategoryEmployee, CategoryCostCode, CostCode, CategoryEmployeeField
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from apps.project.models import ProjectContractor, ProjectContractorEmployeeField, ProjectMandayStart, ProjectEmployee, ProjectManDaySchedule, DailyProduce, Project
from apps.employee.models import Employee
from apps.contractor.models import EmployeeField

from apps.user_profile.models import UserProfile
# Import Graph
from django.db.models import F, Sum, Avg, Max, Min, Case, When, Value
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse, HttpResponse, request, HttpRequest, HttpResponseRedirect
from django.shortcuts import render


# Create your views here.
# ==============
# COMPANIES
# ==============
class CompanyCreate(CreateView):
    model = Company
    form_class = CompanyForm
    template_name = 'apps/company/company_form.html'
    success_url = reverse_lazy('company:company_list')

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(CompanyCreate, self).form_valid(form)

# ========================================
class CompanyList(ListView):
    model = Company
    template_name = 'apps/company/company_list.html'

    def get_context_data(self, **kwargs):
        context = super(CompanyList, self).get_context_data(**kwargs)

        company_list = Company.objects.all()

        context['companyList'] = company_list

        return context


# ========================================
class CompanyUpdate(UpdateView):
    model = Company
    form_class = CompanyForm
    template_name = 'apps/company/company_form.html'
    success_url = reverse_lazy('company:company_list')

    def form_valid(self, form):
        form.instance.update_user = self.request.user
        return super(CompanyUpdate, self).form_valid(form)

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:company_dashboard', kwargs={
                'pk': self.kwargs['pk']
            })
        else:
            return reverse_lazy('company:company_dashboard', args=(self.object.id,))


#######################
## CATEGORY PROJECTS ##
#######################

# LIST
# ============
class CategoryProjectList(ListView):
    model = CategoryProject
    template_name = 'apps/company/category_project/category_project_list.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryProjectList, self).get_context_data(**kwargs)

        category_project_list = CategoryProject.objects.all(). \
            filter(companies_id=self.kwargs['pk'])

        context['CategoryProjectList'] = category_project_list

        return context

# CREATE
# ====================
class CategoryProjectCreate(CreateView):
    model = CategoryProject
    form_class = CategoryProjectForm
    template_name = 'apps/company/category_project/category_project_form.html'
    success_url = reverse_lazy('company:category_project_list')

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(CategoryProjectCreate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(CategoryProjectCreate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        initial.update(
            {
                'companies_id': companies_id['id'],
                'companies': companies_id['id']
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(CategoryProjectCreate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_project_list', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('company:category_project_list', args=(self.object.id,))

# UPDATE
# =================
class CategoryProjectUpdate(UpdateView):
    model = CategoryProject
    form_class = CategoryProjectForm
    template_name = './apps/company/category_project/category_project_form.html'
    success_url = reverse_lazy('company:category_project_list')

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(CategoryProjectUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(CategoryProjectUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_project_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('company:category_project_list', args=(self.object.id,))

# DELETE
# =============
class CategoryProjectDelete(DeleteView):
    model = CategoryProject
    template_name = 'apps/company/category_project/category_project_delete.html'
    success_url = reverse_lazy('company:category_project_list')


    def get_context_data(self, **kwargs):
        context = super(CategoryProjectDelete, self).get_context_data(**kwargs)

        context['DeleteRegisterExist'] = Project.objects.all().filter(companies_id=self.kwargs['companies_id'],category_projects_id=self.kwargs['pk'])

        register_delete = CategoryProject.objects.all().get(companies_id=self.kwargs['companies_id'], pk=self.kwargs['pk'])

        context['DeleteRegister'] = register_delete

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_project_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('company:category_project_list', args=(self.object.id,))


#####################
# CATEGORY EMPLOYEE #
#####################

# LIST
# =======

class CategoryEmployeeList(ListView):
    model = CategoryEmployee
    template_name = 'apps/company/category_employee/category_employee_list.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryEmployeeList, self).get_context_data(**kwargs)

        # Empployee Assigned
        category_employee_list = CategoryEmployee.objects.all(). \
            filter(companies_id=self.kwargs['pk'])

        context['CategoryEmployeeList'] = category_employee_list

        return context

# CREATE
# =========
class CategoryEmployeeCreate(CreateView):
    model = CategoryEmployee
    form_class = CategoryEmployeeForm
    template_name = 'apps/company/category_employee/category_employee_form.html'
    success_url = reverse_lazy('company:category_employee_list')

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(CategoryEmployeeCreate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(CategoryEmployeeCreate, self).get_initial()

        if self.kwargs['pk']:
            companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
            initial.update(
                {
                    'companies_id': companies_id['id'],
                    'companies': companies_id['id']
                }
            )
        return initial

    def get_form_kwargs(self):
        kwargs = super(CategoryEmployeeCreate, self).get_form_kwargs()

        if self.kwargs['pk']:
            companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
            kwargs.update(
                {
                    'companies_id': companies_id['id'],
                })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_employee_list', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('company:category_employee_list', args=(self.object.id,))

# UPDATE
# =========
class CategoryEmployeeUpdate(UpdateView):
    model = CategoryEmployee
    form_class = CategoryEmployeeForm
    template_name = 'apps/company/category_employee/category_employee_form.html'
    success_url = reverse_lazy('company:category_employee_list')

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(CategoryEmployeeUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(CategoryEmployeeUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_employee_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('company:category_employee_list', args=(self.object.id,))

# DELETE
# =============
class CategoryEmployeeDelete(DeleteView):
    model = CategoryEmployee
    template_name = 'apps/company/category_employee/category_employee_delete.html'
    success_url = reverse_lazy('company:category_employee_list')

    def get_context_data(self, **kwargs):
        context = super(CategoryEmployeeDelete, self).get_context_data(**kwargs)

        context['DeleteRegisterExist'] = Employee.objects.filter(companies_id=self.kwargs['companies_id'], category_employees_id=self.kwargs['pk'])
        context['DeleteRegister'] = CategoryEmployee.objects.get(companies_id=self.kwargs['companies_id'], pk=self.kwargs['pk'])

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_employee_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('company:category_employee_list', args=(self.object.id,))

# #################### #
# CATEGORY COST CODE   #
# #################### #

# LIST
# ============
class CategoryCostCodeList(ListView):
    model = CategoryCostCode
    template_name = 'apps/company/category_cost_code/category_cost_code_list.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryCostCodeList, self).get_context_data(**kwargs)

        # Empployee Assigned
        category_cost_code = CategoryCostCode.objects.all(). \
            filter(companies_id=self.kwargs['pk'])

        context['CategoryCostCodeFieldList'] = category_cost_code

        return context

# CREATE
# ===========

class CategoryCostCodeCreate(CreateView):
    model = CategoryCostCode
    form_class = CategoryCostCodeForm
    template_name = 'apps/company/category_cost_code/category_cost_code_form.html'
    success_url = reverse_lazy('company:category_cost_code_list')

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(CategoryCostCodeCreate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(CategoryCostCodeCreate, self).get_initial()
        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        initial.update(
            {
                'companies_id': companies_id['id'],
                'companies': companies_id['id']
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(CategoryCostCodeCreate, self).get_form_kwargs()
        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_cost_code_list', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('company:category_cost_code_list', args=(self.object.id,))

# UPDATE
# ========================================

class CategoryCostCodeUpdate(UpdateView):
    model = CategoryCostCode
    form_class = CategoryCostCodeForm
    template_name = 'apps/company/category_cost_code/category_cost_code_form.html'
    success_url = reverse_lazy('company:category_cost_code_list')

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(CategoryCostCodeUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(CategoryCostCodeUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_cost_code_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('company:category_cost_code_list', args=(self.object.id,))

# DELETE
# =====================
class CategoryCostCodeDelete(DeleteView):
    model = CategoryCostCode
    template_name = 'apps/company/category_cost_code/category_cost_code_delete.html'

    def get_context_data(self, **kwargs):

        context = super(CategoryCostCodeDelete, self).get_context_data(**kwargs)

        context['DeleteRegisterExist'] = CostCode.objects.filter(category_cost_codes_id=self.kwargs['pk'])
        context['DeleteRegister'] = CategoryCostCode.objects.get(companies_id=self.kwargs['companies_id'], pk=self.kwargs['pk'])

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_cost_code_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('company:category_cost_code_list', args=(self.object.id,))


########################
# COST CODE DATABASE   #
########################

# LIST
# =================
class CostCodeList(ListView):
    model = CostCode
    template_name = 'apps/company/cost_code/cost_code_list.html'

    def get_context_data(self, **kwargs):
        context = super(CostCodeList, self).get_context_data(**kwargs)

        # Empployee Assigned
        cost_code_list = CostCode.objects.select_related().all(). \
            filter(category_cost_codes__companies_id=self.kwargs['pk'])

        context['CostCodeFieldList'] = cost_code_list
        return context

# CREATE
# =================
class CostCodeCreate(CreateView):
    model = CostCode
    form_class = CostCodeForm
    template_name = 'apps/company/cost_code/cost_code_form.html'
    success_url = reverse_lazy('company:cost_code_list')

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(CostCodeCreate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(CostCodeCreate, self).get_initial()
        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        initial.update(
            {
                'companies_id': companies_id['id'],
                'companies': companies_id['id']
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(CostCodeCreate, self).get_form_kwargs()
        companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:cost_code_list', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('company:cost_code_list', args=(self.object.id,))

# UPDATE
# ===============
class CostCodeUpdate(UpdateView):
    model = CostCode
    form_class = CostCodeForm
    template_name = 'apps/company/cost_code/cost_code_form.html'
    success_url = reverse_lazy('company:cost_code_list')

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(CostCodeUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(CostCodeUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:cost_code_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('company:cost_code_list', args=(self.object.id,))
# ========================================

class CostCodeDelete(DeleteView):
    model = CostCode
    template_name = 'apps/company/cost_code/cost_code_delete.html'
    def get_context_data(self, **kwargs):

        context = super(CostCodeDelete, self).get_context_data(**kwargs)

        context['DeleteRegisterExist'] = ProjectManDaySchedule.objects.filter(cost_codes_id=self.kwargs['pk'])
        context['DeleteRegister'] = CostCode.objects.get(pk=self.kwargs['pk'])

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:cost_code_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('company:cost_code_list', args=(self.object.id,))
# ========================================

###############################
## CATEGORY EMPLOYEE FIELD   ##
###############################

# LIST
# ===========
class CategoryEmployeeFieldList(ListView):
    model = CategoryEmployeeField
    template_name = 'apps/company/category_employee_field/category_employee_field_list.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryEmployeeFieldList, self).get_context_data(**kwargs)

        context['CategoryEmployeeFieldList'] = CategoryEmployeeField.objects.all(). \
            filter(companies_id=self.kwargs['pk'])
        return context

# CREATE
# =============
class CategoryEmployeeFieldCreate(CreateView):
    model = CategoryEmployeeField
    form_class = CategoryEmployeeFieldForm
    template_name = 'apps/company/category_employee_field/category_employee_field_form.html'

    def form_valid(self, form):
        form.instance.create_user = self.request.user
        return super(CategoryEmployeeFieldCreate, self).form_valid(form)

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(CategoryEmployeeFieldCreate, self).get_initial()

        if self.kwargs['pk']:
            companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
            initial.update(
                {
                    'companies_id': companies_id['id'],
                    'companies': companies_id['id']
                }
            )
        return initial

    def get_form_kwargs(self):
        kwargs = super(CategoryEmployeeFieldCreate, self).get_form_kwargs()

        if self.kwargs['pk']:
            companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
            kwargs.update(
                {
                    'companies_id': companies_id['id'],
                })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_employee_field_list', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('company:category_employee_field_list', args=(self.object.id,))

# UPDATE
# ===========
class CategoryEmployeeFieldUpdate(UpdateView):
    model = CategoryEmployeeField
    form_class = CategoryEmployeeFieldForm
    template_name = 'apps/company/category_employee_field/category_employee_field_form.html'
    success_url = reverse_lazy('company:category_employee_field_list')

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(CategoryEmployeeFieldUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(CategoryEmployeeFieldUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_employee_field_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('company:category_employee_field_list', args=(self.object.id,))

# DELETE
# ===============
class CategoryEmployeeFieldDelete(DeleteView):
    model = CategoryEmployeeField
    template_name = 'apps/company/category_employee_field/category_employee_field_delete.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryEmployeeFieldDelete, self).get_context_data(**kwargs)

        context['DeleteRegisterExist'] = EmployeeField.objects.filter(category_employees_field_id=self.kwargs['pk'])
        context['DeleteRegister'] = CategoryEmployeeField.objects.get(companies_id=self.kwargs['companies_id'], pk=self.kwargs['pk'])

        return context


    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('company:category_employee_field_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('company:category_employee_field_list', args=(self.object.id,))

#######################
## Company Dashboard ##
#######################

# Project DETAIL
# ========================================

# Create your views here.
def index(request):
    return render(request, 'base/index.html')

class CompanyPending(ListView):
    model = Company
    template_name = 'apps/company/pending.html'

    def get_context_data(self, **kwargs):
        context = super(CompanyPending, self).get_context_data(**kwargs)

        context['user_information'] = UserProfile.objects.get(username_id=self.kwargs['pk'])


        return context


class CompanyDashboard(ListView):
    # date = datetime.datetime.now()
    model = Company
    template_name = 'apps/company/index.html'

    def get_context_data(self, **kwargs):
        context = super(CompanyDashboard, self).get_context_data(**kwargs)

        total_man_days_allotted = 0
        total_contractors = 0
        total_employees_field = 0
        total_man_days_remaining = 0
        total_man_days_spent = 0
        total_cost_man_days_spend = 0
        total_cost_man_days_alloted = 0
        project_progress = 0
        regular_cost = 0
        total_works_hours = 0
        total_hours = 0

        # Project Employee
        employee_project = ProjectEmployee.objects.select_related('projects', 'employees')
        context['ProjectEmployeeProject'] = employee_project

        # Daily Produce
        context['projectDailyProduce'] = DailyProduce.objects.all(). \
            select_related().order_by('-created_at')

        # Contractors in Projects
        context['ProjectContractorList'] = ProjectContractor.objects.all(). \
            select_related()

        company_cost_man_days = ProjectManDaySchedule.objects. \
            values('projects__name'). \
            filter(projects__general_contractors__companies__id=self.kwargs['pk']). \
            annotate(
            cost_md=Sum(
                Case(
                    When(projects__is_wage_scale=False,
                         then=F('man_days_allotted') * 8 * F('regular_cost')
                         ),
                    When(projects__is_wage_scale=True,
                         then=F('man_days_allotted') * 8 * F('wage_cost')
                         )
                )
            ),
            production=Sum('production')
        )
        if company_cost_man_days.count() >0:
            total_cost_man_days_alloted = company_cost_man_days[0]['cost_md']

        #[ Contractors Information
        projects_cost_hours = DailyProduce.objects. \
            values('project_contractor_employees_field__project_contractors__projects__name', 'project_contractor_employees_field__project_contractors__projects__id', 'project_contractor_employees_field__project_contractors__projects__po'). \
            filter(project_contractor_employees_field__project_contractors__contractors__companies__id=self.kwargs['pk']). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            hours=Sum('work_hours')
        )
        context['CompanyProjects'] = projects_cost_hours.order_by('hours')


        #[ CONTRACTORS Information
        contractor_cost_hours = DailyProduce.objects. \
            values('project_contractor_employees_field__project_contractors__contractors__name'). \
            filter(project_contractor_employees_field__project_contractors__contractors__companies__id=self.kwargs['pk']). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            hours=Sum('work_hours')
        )
        context['CompanyContractor'] = contractor_cost_hours.order_by('hours')

        #[ CATEGORY LABOR Information
        company_category_labor= DailyProduce.objects. \
            values('project_manday_schedules__cost_codes__category_cost_codes__name'). \
            filter(project_contractor_employees_field__project_contractors__contractors__companies__id=self.kwargs['pk']). \
            annotate(cost=Sum(
            Case(
                When(project_manday_schedules__projects__is_wage_scale=False,
                     then=F('work_hours') * F('project_manday_schedules__regular_cost')
                     ),
                When(project_manday_schedules__projects__is_wage_scale=True,
                     then=F('work_hours') * F('project_manday_schedules__wage_cost')
                     )
            )
        ),
            hours=Sum('work_hours')
        )
        context['CompanyCategoryLabor'] = company_category_labor.order_by('hours')

        # [ CATEGORY EMPLOYEE FIELD Information
        company_category_employee_field = DailyProduce.objects. \
            values('project_contractor_employees_field__contractor_employees_field__employees_field__category_employees_field__name'). \
            filter(project_contractor_employees_field__project_contractors__contractors__companies__id=self.kwargs['pk']). \
            annotate(cost=Sum(
            Case(
                When(project_manday_schedules__projects__is_wage_scale=False,
                     then=F('work_hours') * F('project_manday_schedules__regular_cost')
                     ),
                When(project_manday_schedules__projects__is_wage_scale=True,
                     then=F('work_hours') * F('project_manday_schedules__wage_cost')
                     )
            )
        ),
            hours=Sum('work_hours')
        )
        context['CompanyCategoryEmployeeField'] = company_category_employee_field.order_by('hours')



        #} Company Cost Hours Information
        company_cost_hours = DailyProduce.objects. \
            values('project_contractor_employees_field__project_contractors__contractors__companies__name'). \
            filter(project_contractor_employees_field__project_contractors__contractors__companies__id=self.kwargs['pk']). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            cost_md=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            hours=Sum('work_hours')
        )



        if company_cost_hours.count() > 0:
            if company_cost_hours[0]['cost'] is not None and company_cost_hours[0]['hours'] is not None:
                total_cost_man_days_spend = company_cost_hours[0]['cost']
                total_hours = company_cost_hours[0]['hours']+0


        #{ MAN DAYS CALCULATOR
        total_projects = Project.objects.all().filter(companies_id=self.kwargs['pk']).count()
        total_works_hours = DailyProduce.objects.aggregate(total_works_hours=Sum('work_hours'))

        total_employees_field = ProjectContractorEmployeeField.objects.all().count()

        total_contractors = ProjectContractor.objects.all().count()

        man_days_allotted = ProjectManDaySchedule.objects.filter(projects__companies_id=self.kwargs['pk']). \
            aggregate(total_man_days_allotted=Sum('man_days_allotted'))

        total_regular_cost = ProjectManDaySchedule.objects.filter(projects__companies_id=self.kwargs['pk']). \
            aggregate(total_regular_cost=Sum('regular_cost') * 2)

        if man_days_allotted['total_man_days_allotted'] is not None:
            total_man_days_allotted = man_days_allotted['total_man_days_allotted']
        else:
            total_man_days_allotted = 0

        daily_work_hours = DailyProduce.objects. \
            filter(project_contractor_employees_field__project_contractors__contractors__companies__id=self.kwargs['pk']). \
            aggregate(total_daily_work_hours=Sum('work_hours'))

        if daily_work_hours['total_daily_work_hours'] is not None:
            # if man_days_allotted.exists() and daily_work_hours.exists():
            total_man_days_spent = (daily_work_hours['total_daily_work_hours'] / 8)
            total_man_days_remaining = total_man_days_allotted - total_man_days_spent
            project_progress = round((total_man_days_spent / total_man_days_allotted) * 100, 2)
        else:
            daily_work_hours['total_daily_work_hours'] = 0


        data = ({
            'total_projects': total_projects,
            'total_contractors': total_contractors,
            'total_employees_field': total_employees_field,
            'man_days_allotted': total_man_days_allotted,
            'man_days_spent': round(total_man_days_spent, 2),
            'man_days_remaining': round(total_man_days_remaining, 2),
            'total_cost_man_days_spend': total_cost_man_days_spend,
            'total_cost_man_days_alloted': total_cost_man_days_alloted,

            'total_works_hours': total_hours
        })

        context['CompanyDetail'] = data
        # #} END MAN DAY CALCULATE

        return context

    success_url = reverse_lazy('company:index')

# ========================================
def get_data(request, *args, **kwargs):
    data = {
        "Man Day": 100,
        "Cost": 10,
    }
    return JsonResponse(data)  # http response

# ========================================
# Graficar datos
class CompanyChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, *args, **kwargs):
        contractor_graph = DailyProduce.objects.values('project_contractor_employees_field__project_contractors__projects__po'). \
            filter(project_contractor_employees_field__project_contractors__contractors__companies__id=self.kwargs['pk']). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                ),
            ), hours=Sum('work_hours')).values_list('project_contractor_employees_field__project_contractors__projects__po', 'cost', 'hours')

        l = []
        c = []
        h = []
        for i, item in enumerate(contractor_graph):
            label = contractor_graph[i][0]
            cost = contractor_graph[i][1]
            hours = contractor_graph[i][2]/8
            l.append(label)
            c.append(cost)
            h.append(hours)


        #Company Cost Mad Day Spend Information
        total_cost_man_days_spend = 0
        company_cost_hours = DailyProduce.objects. \
            values('project_contractor_employees_field__project_contractors__contractors__companies__name'). \
            filter(project_contractor_employees_field__project_contractors__contractors__companies__id=self.kwargs['pk']). \
            annotate(
            cost=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            cost_md=Sum(
                Case(
                    When(project_manday_schedules__projects__is_wage_scale=False,
                         then=F('work_hours') * F('project_manday_schedules__regular_cost')
                         ),
                    When(project_manday_schedules__projects__is_wage_scale=True,
                         then=F('work_hours') * F('project_manday_schedules__wage_cost')
                         )
                )
            ),
            hours=Sum('work_hours')
        )

        if company_cost_hours.count() > 0:
            if company_cost_hours[0]['cost'] is not None and company_cost_hours[0]['hours'] is not None:
                total_cost_man_days_spend = company_cost_hours[0]['cost']
                total_hours = company_cost_hours[0]['hours'] + 0

        # Company Cost Man Days Information
        total_cost_man_days_allotted = 0
        company_cost_man_days = ProjectManDaySchedule.objects. \
            values('projects__name'). \
            filter(projects__general_contractors__companies__id=self.kwargs['pk']). \
            annotate(
            cost_md=Sum(
                Case(
                    When(projects__is_wage_scale=False,
                         then=F('man_days_allotted') * 8 * F('regular_cost')
                         ),
                    When(projects__is_wage_scale=True,
                         then=F('man_days_allotted') * 8 * F('wage_cost')
                         )
                )
            ),
            production=Sum('production')
        )
        if company_cost_man_days.count() > 0:
            total_cost_man_days_allotted = company_cost_man_days[0]['cost_md']

        labels_graph_pie = ['Cost Man Days Spend','Cost Man Days Alloted']
        chart_graph_pie = [ total_cost_man_days_spend, total_cost_man_days_allotted]

        # Send Data to Graph
        data = {
            "labels_graph": l,
            "default_man_days": h,
            "default_project_cost": c,
            "labels_graph_pie": labels_graph_pie,
            "chart_graph_pie": chart_graph_pie,
        }
        return Response(data)


def RedirectDashboard(request, **kwargs):
    # REDIRECCIONAR A DASHBOAR O PROFILE
    dashboard = ''
    if request.user.id is not None:

        a = UserProfile.objects.values('companies_id').get(username_id=request.user.id)

        if a['companies_id']:

            if request.user.userprofile.get_role_display() is "Administrator" or request.user.is_staff:

                dashboard = HttpResponseRedirect(reverse_lazy('company:company_dashboard',
                                                              kwargs={
                                                                  'pk': request.user.userprofile.companies.id
                                                              }))
            else:
                # To Project List
                dashboard = HttpResponseRedirect(reverse_lazy('project:project_list',
                                                              kwargs={
                                                                  'companies_id': request.user.userprofile.companies.id,
                                                                  'user_id': request.user.id
                                                              }))

        else:
            dashboard = HttpResponseRedirect(reverse_lazy('company:company_pending',
                                                          kwargs={
                                                              'pk': request.user.id
                                                          }))

    else:
        dashboard = HttpResponseRedirect(reverse_lazy('company:company_dashboard',
                                              kwargs={
                                                  'pk': request.user.userprofile.companies.id
                                              }))


    return dashboard


