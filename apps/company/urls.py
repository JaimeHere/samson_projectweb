"""
URLs Company
"""
from django.contrib.auth.decorators import login_required

from django.conf.urls import url
from apps.company.views import index, CompanyCreate, CompanyList, CompanyUpdate,  \
    CategoryProjectCreate, CategoryProjectList, CategoryProjectDelete, CategoryProjectUpdate, \
    CategoryEmployeeCreate, CategoryEmployeeList, CategoryEmployeeDelete, CategoryEmployeeUpdate, \
    CategoryCostCodeCreate, CategoryCostCodeList, CategoryCostCodeDelete, CategoryCostCodeUpdate,\
    CostCodeCreate, CostCodeList, CostCodeDelete, CostCodeUpdate, \
    CategoryEmployeeFieldCreate, CategoryEmployeeFieldList, CategoryEmployeeFieldDelete, CategoryEmployeeFieldUpdate, CompanyChartData, get_data, CompanyDashboard, CompanyPending

urlpatterns = [
    url(r'^$', index, name='index'),
    # URLs: Company
    url(r'^create$', login_required(CompanyCreate.as_view()), name='company_create'),
    url(r'^list$', login_required(CompanyList.as_view()), name='company_list'),
    url(r'^update/(?P<pk>\d+)/$', login_required(CompanyUpdate.as_view()), name='company_update'),
    #url(r'^delete/(?P<pk>\d+)/$', login_required(CompanyDelete.as_view()), name='company_delete'),


    # URLs: Category Project
    url(r'^(?P<pk>\d+)/cp_create/$', login_required(CategoryProjectCreate.as_view()), name='category_project_create'),
    url(r'^(?P<pk>\d+)/cp_list/$', login_required(CategoryProjectList.as_view()), name='category_project_list'),
    url(r'^(?P<companies_id>\d+)/cp_update/(?P<pk>\d+)/$', login_required(CategoryProjectUpdate.as_view()), name='category_project_update'),
    url(r'^(?P<companies_id>\d+)/cp_delete/(?P<pk>\d+)/$', login_required(CategoryProjectDelete.as_view()), name='category_project_delete'),


    # URLs: Category Employee
    url(r'^(?P<pk>\d+)/ce_create/$', login_required(CategoryEmployeeCreate.as_view()), name='category_employee_create'),
    url(r'^(?P<pk>\d+)/ce_list/$', login_required(CategoryEmployeeList.as_view()), name='category_employee_list'),
    url(r'^(?P<companies_id>\d+)/ce_update/(?P<pk>\d+)/$', login_required(CategoryEmployeeUpdate.as_view()), name='category_employee_update'),
    url(r'^(?P<companies_id>\d+)/ce_delete/(?P<pk>\d+)/$', login_required(CategoryEmployeeDelete.as_view()), name='category_employee_delete'),


    # URLs: Category Cost code
    url(r'^(?P<pk>\d+)/ccc_create/$', login_required(CategoryCostCodeCreate.as_view()), name='category_cost_code_create'),
    url(r'^(?P<pk>\d+)/ccc_list/$', login_required(CategoryCostCodeList.as_view()), name='category_cost_code_list'),
    url(r'^(?P<companies_id>\d+)/ccc_update/(?P<pk>\d+)/$', login_required(CategoryCostCodeUpdate.as_view()), name='category_cost_code_update'),
    url(r'^(?P<companies_id>\d+)/ccc_delete/(?P<pk>\d+)/$', login_required(CategoryCostCodeDelete.as_view()), name='category_cost_code_delete'),


    # URLs: Cost code
    url(r'^(?P<pk>\d+)/cc_create/$', login_required(CostCodeCreate.as_view()), name='cost_code_create'),
    url(r'^(?P<pk>\d+)/cc_list/$', login_required(CostCodeList.as_view()), name='cost_code_list'),
    url(r'^(?P<companies_id>\d+)/cc_update/(?P<pk>\d+)/$', login_required(CostCodeUpdate.as_view()), name='cost_code_update'),
    url(r'^(?P<companies_id>\d+)/cc_delete/(?P<pk>\d+)/$', login_required(CostCodeDelete.as_view()), name='cost_code_delete'),


    # URLs: Category Employee Field
    url(r'^(?P<pk>\d+)/cef_create/$', login_required(CategoryEmployeeFieldCreate.as_view()), name='category_employee_field_create'),
    url(r'^(?P<pk>\d+)/cef_list/$', login_required(CategoryEmployeeFieldList.as_view()), name='category_employee_field_list'),
    url(r'^(?P<companies_id>\d+)/cef_update/(?P<pk>\d+)/$', login_required(CategoryEmployeeFieldUpdate.as_view()), name='category_employee_field_update'),
    url(r'^(?P<companies_id>\d+)/cef_delete/(?P<pk>\d+)/$', login_required(CategoryEmployeeFieldDelete.as_view()), name='category_employee_field_delete'),



    # DATA Graph
    url(r'^(?P<pk>\d+)/api/data/$', login_required(get_data), name='api-data'),
    url(r'^(?P<pk>\d+)/api/chart/data/$', login_required(CompanyChartData.as_view())),



    url(r'^(?P<pk>\d+)/dashboard/', login_required(CompanyDashboard.as_view()), name='company_dashboard'),
    url(r'^(?P<pk>\d+)/pending/', login_required(CompanyPending.as_view()), name='company_pending'),

    ###################


]
