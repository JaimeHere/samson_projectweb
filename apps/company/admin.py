from django.contrib import admin

from apps.company.models import Company, CategoryProject, CategoryCostCode, CostCode, CategoryEmployee, CategoryEmployeeField

admin.site.register(Company)
admin.site.register(CategoryEmployee)
admin.site.register(CategoryEmployeeField)
admin.site.register(CategoryCostCode)
admin.site.register(CostCode)
admin.site.register(CategoryProject)

