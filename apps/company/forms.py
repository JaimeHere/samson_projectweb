from django import forms
from apps.company.models import Company, CategoryProject, CategoryEmployee, CategoryCostCode, CostCode, CategoryEmployeeField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset
from crispy_forms.bootstrap import PrependedText, Div, InlineRadios, TabHolder, Tab


US_STATES = (
    ('AL', 'Alabama'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('CA', 'California'), ('CO', 'Colorado'),
    ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'),
    ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'),
    ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'),
    ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'),
    ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'),
    ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'),
    ('PA', 'Pennsylvania'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'),
    ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VA', 'Virginia'),
    ('WA', 'Washington'),
    ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming'))

COUNTRY = (
    ('US', 'United States'), ('CAD', 'Canada'))



# =================
# Company Form
# =================

class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company

        fields = ['name', 'ceo', 'president', 'cbe', 'fed', 'email', 'website_url', 'phone_number', 'phone_number_ext',
                  'city_address', 'country_address', 'state_address', 'zip_code_address', 'street_address', 'is_active',
                  ]

    name = forms.CharField(label="Company Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'", 'placeholder': 'Company Name'}
                               ))

    ceo = forms.CharField(label="CEO Name:", required=True, max_length=100,
                          widget=forms.TextInput(
                              attrs={'type': 'text', 'class': 'form-control', 'required': True, 'placeholder': 'both name(s) e.g Jon Doe'}))

    president = forms.CharField(label="President Name:", required=True, max_length=100,
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'class': 'form-control', 'required': True, 'placeholder': 'both name(s) e.g Jon Doe'}))

    cbe = forms.CharField(label="CBE:", required=True, max_length=50,
                          widget=forms.TextInput(
                              attrs={'type': 'text', 'name': 'cbe', 'class': 'form-control', 'required': True, 'placeholder': 'CBE Register'}))

    fed = forms.CharField(label="FED:", required=True, max_length=25,
                          widget=forms.TextInput(
                              attrs={'type': 'text', 'name': 'fed', 'class': 'form-control', 'required': True, 'placeholder': 'FED Register'}))

    phone_number = forms.CharField(label="Phone Number:", max_length=14,
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control', 'data-inputmask': "'mask':'(999) 999-9999'", 'placeholder':'(___) ___-____'}))

    phone_number_ext = forms.CharField(label="Phone number ext:", max_length=5,
                                       widget=forms.TextInput(
                                           attrs={'type': 'number', 'class': 'form-control input-number value="1" min="1" max="10"', 'placeholder': 'Phone number ext.'}))

    country_address = forms.CharField(label="Country address:", required=True, max_length=100,
                                    widget=forms.Select(choices=COUNTRY))

    street_address = forms.CharField(label="Street address:", required=False, max_length=150,
                                     widget=forms.TextInput(
                                         attrs={'type': 'text', 'class': 'form-control input-number value="1" min="1" max="10"', 'placeholder': 'e.g: 4422 Faroe Place'}))

    city_address = forms.CharField(label="City address:", required=False, max_length=100,
                                   widget=forms.TextInput(
                                       attrs={'type': 'text', 'class': 'form-control input-number value="1" min="1" max="10"', 'placeholder': 'e.g: Rockville'}))

    state_address = forms.CharField(label="State address:", required=True, max_length=2,
                                    widget=forms.Select(choices=US_STATES))

    zip_code_address = forms.CharField(label="zip code address:", required=True, max_length=5,
                                       widget=forms.TextInput(
                                           attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'e.g: 20850'}))

    email = forms.EmailField(label="email:", required=False, max_length=100,
                             widget=forms.TextInput(
                                 attrs={'type': 'email', 'required': True, 'placeholder': 'jon.doe@example.com'}))

    website_url = forms.URLField(initial='http://', label="Website URL:", required=False,
                                 widget=forms.TextInput(
                                     attrs={'type': 'url', 'class': 'form-control', 'placeholder': 'http://www.example.com'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    def __init__(self, *args, **kwargs):
        super(CompanyForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-company-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        self.helper.layout = Layout(
            Fieldset('<i class="fa fa-info-circle"></i> General Information',
                    Div(
                        PrependedText('name',  '<i class="glyphicon glyphicon-home"></i>', 'style="background: red;"'),
                        PrependedText('ceo', '<i class="glyphicon glyphicon-user"></i>'),
                        PrependedText('president', '<i class="glyphicon glyphicon-user"></i>'),
                        PrependedText('cbe', '<i class="glyphicon glyphicon-asterisk"></i>'),
                        css_class='col-md-6'
                    ),

                    Div(
                       PrependedText('fed', '<i class="glyphicon glyphicon-asterisk"></i>'),
                       PrependedText('email', '<i class="glyphicon glyphicon-envelope"></i>'),
                       PrependedText('website_url', '<i class="glyphicon glyphicon-link"></i>'),
                       PrependedText('phone_number', '<i class="glyphicon glyphicon-phone-alt"></i>',
                                     placeholder='(___) ___-____'),
                       css_class='col-md-6'
                    ),
            ),

            Fieldset('<i class="fa fa-map-marker"></i> Location Information',
                       Div(
                           PrependedText('phone_number_ext', '<i class="glyphicon glyphicon-phone-alt"></i>'),
                           PrependedText('street_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                           PrependedText('city_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                           css_class='col-md-6'
                       ),
                       Div(
                           PrependedText('state_address', ''),
                           PrependedText('zip_code_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                           PrependedText('country_address', ''),

                           PrependedText('is_active', ''),
                           css_class='col-md-6'
                       ),
             )

        )

# =================
# Category Project
# =================

class CategoryProjectForm(forms.ModelForm):
    class Meta:
        model = CategoryProject

        fields = ['companies', 'name', 'is_active',
                  ]
    name = forms.CharField(label="Category Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'", 'placeholder': 'Category Project Name'}
                               ))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    def __init__(self, companies_id, *args, **kwargs):
        super(CategoryProjectForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-category-project-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if companies_id:
            self.helper.form.fields['companies'].queryset = Company.objects.filter(pk=companies_id)


        self.helper.layout = Layout(
            Fieldset('',
                    Div(
                        PrependedText('companies', ''),
                        PrependedText('name', '<i class="glyphicon glyphicon-home"></i>'),
                        PrependedText('is_active', ''),
                        css_class='col-md-12'
                    ),
            ),
        )

# ================= #
# Category Employee #
# ================= #

class CategoryEmployeeForm(forms.ModelForm):
    class Meta:
        model = CategoryEmployee

        fields = ['companies', 'name', 'is_active',
                  ]

    companies = forms.Select()

    name = forms.CharField(label="Category Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'", 'placeholder': 'Category Employee Name'}
                               ))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    def __init__(self, companies_id, *args, **kwargs):
        super(CategoryEmployeeForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-category-employee-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if companies_id:
            self.helper.form.fields['companies'].queryset = Company.objects.filter(pk=companies_id)

        self.helper.layout = Layout(
            Fieldset('',
                    Div(
                        PrependedText('companies', ''),
                        PrependedText('name', '<i class="glyphicon glyphicon-home"></i>'),
                        PrependedText('is_active', ''),


                        css_class='col-md-12'
                    ),
            ),
        )


# =================
# CategoryCostCode
# =================

class CategoryCostCodeForm(forms.ModelForm):
    class Meta:
        model = CategoryCostCode

        fields = ['companies', 'name', 'is_active',
                  ]

    companies = forms.Select()

    name = forms.CharField(label="Category Cost Code Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'", 'placeholder': 'Category Cost Code Name eg: CEILINGS, FINISHES, etc'}
                               ))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    def __init__(self, companies_id, *args, **kwargs):
        super(CategoryCostCodeForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-category-cost_code-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        if companies_id:
            self.helper.form.fields['companies'].queryset = Company.objects.filter(pk=companies_id)

        self.helper.layout = Layout(
            Fieldset('',
                    Div(
                        PrependedText('companies', ''),
                        PrependedText('name', '<i class="glyphicon glyphicon-home"></i>'),
                        PrependedText('is_active', ''),

                        css_class='col-md-12'
                    ),
            ),
        )

# =================
# Cost Code Form
# =================

class CostCodeForm(forms.ModelForm):
    UNITS = (('SF', 'SF - (Square Foot)'), ('LF', 'LF - (Linear Foot)'), ('EA', 'EA'))

    class Meta:
        model = CostCode


        fields = ['category_cost_codes', 'name', 'unit', 'code_labor', 'code_material', 'production', \
                  'regular_cost', 'overtime_cost', 'wage_cost', 'wage_overtime_cost', 'notes', 'is_active',
                ]

    name = forms.CharField(label="Cost Code Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'", 'placeholder': 'Cost Code Name'}
                               ))

    unit = forms.CharField(label="Unit:", required=True, max_length=100,
                           widget=forms.Select(choices=UNITS))


    code_labor = forms.CharField(label="Code Labor:", required=True, max_length=100,
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'class': 'form-control', 'required': True, 'placeholder': 'code labor name(s) e.g 111100.000.1', 'data-inputmask': "'mask':'99999.999.9'"}))

    code_material = forms.CharField(label="Code Material:", required=True, max_length=100,
                                 widget=forms.TextInput(
                                     attrs={'type': 'text', 'class': 'form-control', 'required': True, 'placeholder': 'code labor name(s) e.g 111100.000.2', 'data-inputmask': "'mask':'99999.999.9'"}))

    production = forms.CharField(label="Production by Man Day:", required=True, max_length=25,
                          widget=forms.TextInput(
                              attrs={'type': 'number', 'name': 'fed', 'class': 'form-control', 'required': True, 'placeholder': 'Producion daily per Man Day'}))


    regular_cost = forms.CharField(label="Regular cost:", required=False, max_length=100,
                                   widget=forms.TextInput(
                                       attrs={'type': 'text', 'class': 'form-control input-number value="1" min="1" max="10"', 'placeholder': 'Regular cost'}))

    overtime_cost = forms.CharField(label="Overtime cost:", required=False, max_length=100,
                                   widget=forms.TextInput(
                                       attrs={'type': 'text',
                                              'class': 'form-control input-number value="1" min="1" max="10"',
                                              'placeholder': 'Overtime cost'}))

    wage_cost = forms.CharField(label="Wage cost:", required=True, max_length=5,
                                       widget=forms.TextInput(
                                           attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'Wage cost'}))

    wage_overtime_cost = forms.CharField(label="Wage overtime cost:", required=False, max_length=100,
                             widget=forms.TextInput(
                                 attrs={'type': 'number', 'required': True, 'placeholder': 'Wage overtime cost'}))

    notes = forms.CharField(label="Notes", required=False,
                                 widget=forms.TextInput(
                                     attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'About this code cost'}))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    def __init__(self, companies_id, *args, **kwargs):
        super(CostCodeForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-cost-code-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'

        if companies_id:
            self.helper.form.fields['category_cost_codes'].queryset = CategoryCostCode.objects.filter(companies_id=companies_id)

        self.helper.layout = Layout(

            Fieldset('<i class="fa fa-info-circle"></i> General Information',
                    Div(
                        PrependedText('category_cost_codes', ''),
                        PrependedText('name', '<i class="glyphicon glyphicon-user"></i>'),
                        PrependedText('unit', ''),
                        css_class='col-md-6'
                    ),

                    Div(
                        PrependedText('code_labor', '<i class="glyphicon glyphicon-asterisk"></i>'),
                        PrependedText('code_material', '<i class="glyphicon glyphicon-asterisk"></i>'),
                        PrependedText('production', '<i class="glyphicon glyphicon-link"></i>'),
                        css_class='col-md-6'
                    ),
            ),

            Fieldset('<i class="fa fa-calculator"></i> Cost Information',
                       Div(
                           PrependedText('regular_cost', '<i class="fa fa-dollar"></i>'),
                           PrependedText('overtime_cost', '<i class="fa fa-dollar"></i>'),
                           PrependedText('wage_cost', '<i class="fa fa-dollar"></i>'),
                           css_class='col-md-6'
                       ),
                       Div(

                           PrependedText('wage_overtime_cost', '<i class="fa fa-dollar"></i>'),
                           PrependedText('notes', '<i class="fa fa-align-justify"></i>'),
                           PrependedText('is_active', ''),
                           css_class='col-md-6'
                       ),
             ),

        )



# ================= #
# Category Employee #
# ================= #

class CategoryEmployeeFieldForm(forms.ModelForm):
    class Meta:
        model = CategoryEmployeeField

        fields = ['companies', 'name', 'is_active',
                  ]

    companies = forms.Select()

    name = forms.CharField(label="Category Name:", required=True, max_length=100,
                           widget=forms.TextInput(
                               attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                      'data-validate-length-range': "'6'", 'data-validate-words': "'2'", 'placeholder': 'Category Employee Field Name e.g: Labor, Skill Labor, Mechanic, etc'}
                               ))

    is_active = forms.CharField(label="Deactivate/Activate", required=True,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    def __init__(self, companies_id, *args, **kwargs):
        super(CategoryEmployeeFieldForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        self.helper.form_id = 'add-new-category-employee-field-form'
        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        if companies_id:
            self.helper.form.fields['companies'].queryset = Company.objects.filter(pk=companies_id)

        self.helper.layout = Layout(
            Fieldset('',
                    Div(
                        PrependedText('companies', ''),
                        PrependedText('name', '<i class="glyphicon glyphicon-home"></i>'),
                        PrependedText('is_active', ''),
                        css_class='col-md-12'
                    ),
            ),
        )
