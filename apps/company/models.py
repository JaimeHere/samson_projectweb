from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Company(models.Model):
    name = models.CharField(max_length=100, unique=True, blank=True)
    ceo = models.CharField(max_length=100, blank=True, null=False)
    president = models.CharField(max_length=100, blank=True, null=False)

    cbe = models.CharField(max_length=50, blank=True)
    fed = models.CharField(max_length=25, blank=True)

    email = models.CharField(max_length=100, blank=True)
    website_url = models.CharField(max_length=100, blank=True)

    phone_number = models.CharField(max_length=14, blank=True)
    phone_number_ext = models.CharField(max_length=5, blank=True)

    country_address = models.CharField(max_length=100, blank=True)
    street_address = models.CharField(max_length=150, blank=True)
    city_address = models.CharField(max_length=100, blank=True)
    state_address = models.CharField(max_length=2, blank=True)
    zip_code_address = models.CharField(max_length=5, blank=True)

    company_logo = models.ImageField(upload_to='./images/company/logo/'.format(name), blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    create_user = models.ForeignKey(User, on_delete=models.CASCADE,)

    updated_at = models.DateTimeField(auto_now=True)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'company'
        verbose_name = 'company'
        verbose_name_plural = 'companies'

        ordering = ['name']

    def __str__(self):
        return '{}'.format(self.name)


class CategoryProject(models.Model):
    companies = models.ForeignKey(Company, on_delete=models.CASCADE,)

    name = models.CharField(max_length=100, blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'category_project'

    def __str__(self):
        return '{}'.format(self.name)


class CategoryCostCode(models.Model):
    companies = models.ForeignKey(Company, on_delete=models.CASCADE,)
    name = models.CharField(max_length=25, blank=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'category_cost_code'

        verbose_name = 'category_cost_code'
        verbose_name_plural = 'category_cost_codes'

    def __str__(self):
        return '{}'.format(self.name)


class CategoryEmployee(models.Model):
    companies = models.ForeignKey(Company, on_delete=models.CASCADE)
    name = models.CharField(blank=True, max_length=50)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'category_employee'

        verbose_name = 'category_employee'
        verbose_name_plural = 'category_employees'

    def __str__(self):
        return '{}'.format(self.name)


class CategoryEmployeeField(models.Model):
    companies = models.ForeignKey(Company, on_delete=models.CASCADE,)
    name = models.CharField(blank=True, max_length=50)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'category_employee_field'
        verbose_name = 'category_employee_field'
        verbose_name_plural = 'category_employees_field'


    def __str__(self):
        return '{}'.format(self.name)


class CostCode(models.Model):
    UNITS = (('SF', 'SF - (Square Foot)'), ('LF', 'LF - (Linear Foot)'), ('EA', 'EA'))
    category_cost_codes = models.ForeignKey(CategoryCostCode, on_delete=models.CASCADE,)

    name = models.CharField(max_length=100, unique=True, blank=True)
    unit = models.CharField(max_length=2, blank=True, choices=UNITS)

    code_labor = models.CharField(max_length=20, unique=True, blank=True)
    code_material = models.CharField(max_length=20, unique=True, blank=True)

    production = models.DecimalField(max_digits=6, decimal_places=2)

    regular_cost = models.DecimalField(max_digits=6, decimal_places=2)
    overtime_cost = models.DecimalField(max_digits=6, decimal_places=2)

    wage_cost = models.DecimalField(max_digits=6, decimal_places=2)
    wage_overtime_cost = models.DecimalField(max_digits=6, decimal_places=2)

    notes = models.TextField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    create_user = models.ForeignKey(User, on_delete=models.CASCADE)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'cost_code'
        verbose_name = 'cost_code'
        verbose_name_plural = 'cost_codes'

    def __str__(self):
        return '{} - {}'.format(self.code_labor, self.name)
#
