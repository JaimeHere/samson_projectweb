from django.db import models
from apps.company.models import Company, CategoryEmployee
from datetime import datetime
from django.utils import timezone
from django.contrib.auth.models import User

import qrcode

from samson_project import settings

EMPLOYEE_STATES = (
    ('A', 'Permanent'),
    ('P', 'Part-timer'),
    ('T', 'Fixed-term'),
    ('E', 'External'),
    ('S', 'Summer worker'),
    ('I', 'Inactive'),
)

class Employee(models.Model):
    now = timezone.now()
    companies = models.ForeignKey(Company, on_delete=models.CASCADE,)
    category_employees = models.ForeignKey(CategoryEmployee, on_delete=models.CASCADE)

    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, null=False)
    date_of_birth = models.DateField(max_length=100, null=True, blank=True)

    employee_code = models.CharField(max_length=10, blank=True, unique=True)

    salary = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)

    email = models.CharField(max_length=100, blank=True)
    avatar = models.ImageField(upload_to='./images/employee/avatar/' + format(employee_code), blank=True, null=True)

    cell_phone_number = models.CharField(max_length=14, blank=True, unique=True)

    country_address = models.CharField(max_length=100, blank=True)
    street_address = models.CharField(max_length=150, blank=True)
    city_address = models.CharField(max_length=100, blank=True)
    state_address = models.CharField(max_length=2, blank=True)
    zip_code_address = models.CharField(max_length=5, blank=True)

    by_direct_deposit = models.BooleanField(default=True)
    by_salary = models.BooleanField(default=True)

    office_or_field = models.BooleanField(default=True, blank=True)

    notes = models.TextField(max_length=255, blank=True, null=True)

    status = models.CharField(max_length=1, blank=True, null=True, choices=EMPLOYEE_STATES, default='A')

    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    create_user = models.ForeignKey(User, on_delete=models.CASCADE)

    updated_at = models.DateTimeField(auto_now=True)
    update_user = models.ForeignKey(User, null=True, blank=True, related_name="+", on_delete=models.CASCADE)

    class Meta:
        db_table = 'employee'
        verbose_name = 'employee'
        verbose_name_plural = 'employees'
        ordering = ('first_name',)

        permissions = (
            ('can_view_employees', 'Can_view_employees'),
            ('can_add_employees', 'Can_add_employees'),
        )


    def __str__(self):
        return '{} {} ({})'.format(self.first_name, self.last_name, self.category_employees)

    @property
    def get_pk(self):
        return self.pk
