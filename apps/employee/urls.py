"""
URLs Employees
"""
from django.contrib.auth.decorators import login_required

from django.conf.urls import url
from apps.employee.views import EmployeeCreate,EmployeeUpdate, EmployeeList, EmployeeDelete, \
    EmployeeListCard, EmployeeProfile

urlpatterns = [
    url(r'^(?P<pk>\d+)/create/$', login_required(EmployeeCreate.as_view()), name='employee_create'),
    url(r'^(?P<pk>\d+)/list/$', login_required(EmployeeList.as_view()), name='employee_list'),
    url(r'^(?P<companies_id>\d+)/update/(?P<pk>\d+)/$', login_required(EmployeeUpdate.as_view()), name='employee_update'),
    url(r'^(?P<companies_id>\d+)/delete/(?P<pk>\d+)/$', login_required(EmployeeDelete.as_view()), name='employee_delete'),

    # Others Actions
    url(r'^(?P<companies_id>\d+)/profile/(?P<pk>\d+)/$', login_required(EmployeeProfile.as_view()), name='employee_profile'),
    url(r'^(?P<companies_id>\d+)/card/(?P<letter>.+)/$', login_required(EmployeeListCard.as_view()), name='employee_list_card'),
]