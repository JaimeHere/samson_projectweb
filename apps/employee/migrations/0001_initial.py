# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-05-10 19:55
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion
import django.views.generic.edit


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('company', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(blank=True, max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('date_of_birth', models.DateField(blank=True, max_length=100, null=True)),
                ('employee_code', models.CharField(blank=True, max_length=10, unique=True)),
                ('salary', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('email', models.CharField(blank=True, max_length=100)),
                ('avatar', models.ImageField(blank=True, null=True, upload_to='./images/employee/avatar/<django.db.models.fields.CharField>')),
                ('cell_phone_number', models.CharField(blank=True, max_length=14, unique=True)),
                ('country_address', models.CharField(blank=True, max_length=100)),
                ('street_address', models.CharField(blank=True, max_length=150)),
                ('city_address', models.CharField(blank=True, max_length=100)),
                ('state_address', models.CharField(blank=True, max_length=2)),
                ('zip_code_address', models.CharField(blank=True, max_length=5)),
                ('by_direct_deposit', models.BooleanField(default=True)),
                ('by_salary', models.BooleanField(default=True)),
                ('office_or_field', models.BooleanField(default=True)),
                ('notes', models.TextField(blank=True, max_length=255, null=True)),
                ('status', models.CharField(blank=True, choices=[('A', 'Permanent'), ('P', 'Part-timer'), ('T', 'Fixed-term'), ('E', 'External'), ('S', 'Summer worker'), ('I', 'Inactive')], default='A', max_length=1, null=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('category_employees', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.CategoryEmployee')),
                ('companies', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Company')),
            ],
            options={
                'verbose_name': 'employee',
                'verbose_name_plural': 'employees',
                'db_table': 'employee',
                'ordering': ('first_name',),
                'permissions': (('can_view_employees', 'Can_view_employees'), ('can_add_employees', 'Can_add_employees')),
            },
        ),
        migrations.CreateModel(
            name='EmployeeUpdate',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            bases=(django.views.generic.edit.UpdateView, 'auth.user'),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AddField(
            model_name='employee',
            name='create_user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='employee',
            name='update_user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL),
        ),
    ]
