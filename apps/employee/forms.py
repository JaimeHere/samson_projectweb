from django import forms
from django.contrib.auth.models import User

from apps.employee.models import Employee
from apps.company.models import Company, CategoryEmployee

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Fieldset
from crispy_forms.bootstrap import PrependedText, Div, InlineRadios, TabHolder, Tab
from datetime import *

US_STATES = (
    ('AL', 'Alabama'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('CA', 'California'), ('CO', 'Colorado'),
    ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'),
    ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'),
    ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'),
    ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'),
    ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'),
    ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'),
    ('PA', 'Pennsylvania'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'),
    ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VA', 'Virginia'),
    ('WA', 'Washington'),
    ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming'))

COUNTRY = (
    ('US', 'United States'), ('CAD', 'Canada')
)

class EmployeeForm(forms.ModelForm):
    # today = datetime.today()
    # WhatWeek = today.strftime("%U")

    class Meta:
        model = Employee
        fields = ['companies', 'category_employees', 'first_name', 'last_name', 'email', 'avatar', \
                  'cell_phone_number', 'city_address', 'country_address', 'state_address', 'zip_code_address', \
                  'street_address', 'office_or_field', 'notes', 'is_active',
                  ]

    first_name = forms.CharField(label="First name:", required=True, max_length=100,
                                 widget=forms.TextInput(
                                     attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                            'data-validate-length-range': "'6'", 'data-validate-words': "'2'",
                                            'placeholder': 'first name(s) Doe'}
                                 ))

    last_name = forms.CharField(label="Last name:", required=True, max_length=100,
                                widget=forms.TextInput(
                                    attrs={'type': 'text', 'class': 'form-control', 'required': True,
                                           'placeholder': 'last name(s) e.g White' }))

    # qrcode = forms.ImageField(required=False, widget=forms.TextInput(attrs={'type': '{% qr_from_text QRCodeData size="5" image_format=png %}'})),

    email = forms.EmailField(label="email:", required=False, max_length=100,
                             widget=forms.TextInput(
                                 attrs={'type': 'email', 'required': True, 'placeholder': 'jon.doe@example.com'}))

    cell_phone_number = forms.CharField(label="Cell Phone Number:", max_length=14,
                                        widget=forms.TextInput(
                                            attrs={'class': 'form-control', 'data-inputmask': "'mask':'(999) 999-9999'",
                                                   'placeholder': '(___) ___-____'}))

    street_address = forms.CharField(label="Street address:", required=False, max_length=150,
                                     widget=forms.TextInput(
                                         attrs={'type': 'text',
                                                'class': 'form-control input-number value="1" min="1" max="10"',
                                                'placeholder': 'e.g: 4422 Faroe Place'}))

    city_address = forms.CharField(label="City address:", required=False, max_length=100,
                                   widget=forms.TextInput(
                                       attrs={'type': 'text',
                                              'class': 'form-control input-number value="1" min="1" max="10"',
                                              'placeholder': 'e.g: Rockville'}))

    country_address = forms.CharField(label="Country address:", required=True, max_length=100,
                                      widget=forms.Select(choices=COUNTRY))

    state_address = forms.CharField(label="State address:", required=True, max_length=2,
                                    widget=forms.Select(choices=US_STATES))

    zip_code_address = forms.CharField(label="zip code address:", required=True, max_length=5,
                                       widget=forms.TextInput(
                                           attrs={'type': 'text', 'class': 'form-control',
                                                  'placeholder': 'e.g: 20850'}))

    is_active = forms.CharField(label="Deactivate/Activate",
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    office_or_field = forms.CharField(label="Office/Field", required=False,
                                widget=forms.CheckboxInput(
                                    attrs={'class': 'js-switch'}))

    notes = forms.CharField(label="Notes:", required=False,
                            widget=forms.Textarea(
                                attrs={'type': 'text', 'class': 'form-control',
                                       'placeholder': 'About this Employee'}))

    def __init__(self, companies_id, *args, **kwargs):
        # self.helper.form.fields['company'].queryset = Company.objects.filter(company_id = 1)

        super(EmployeeForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(form=self)

        # User.objects.get(username = request.GET['username']) self.kwargs['username']).id
        # self.helper.form.fields['category_employee'].queryset = CategoryEmployee.objects.filter(company_id=1)

        # a = User.objects.get(username = self.kwargs['username']).id

        self.helper.form_method = 'POST'
        self.helper.add_input(Submit('submit', 'Submit', css_class='btn-success'))

        # self.helper.form_action = 'submit_survey'
        self.helper.label_class = 'control-label'
        self.helper.form_class = 'animate form-horizontal form-label-left input_mask'

        self.helper.form_show_errors = True
        self.helper.form_show_labels = True

        self.helper.form_tag = True
        self.helper.field_template = 'bootstrap4/field.html'

        if companies_id:
            self.helper.form.fields['companies'].queryset = Company.objects.filter(pk=companies_id)
            self.helper.form.fields['category_employees'].queryset = CategoryEmployee.objects.filter(companies_id=companies_id)

        self.helper.layout = Layout(
            Fieldset('<i class="fa fa-info-circle"></i> General Information',
                     Div(
                         PrependedText('companies', ''),
                         PrependedText('category_employees', ''),

                         PrependedText('first_name', '<i class="glyphicon glyphicon-home"></i>'),
                         PrependedText('last_name', '<i class="glyphicon glyphicon-user"></i>'),
                         css_class='col-md-6'
                     ),
                     Div(
                         PrependedText('email', '<i class="glyphicon glyphicon-envelope"></i>'),
                         PrependedText('cell_phone_number', '<i class="glyphicon glyphicon-phone-alt"></i>',
                                       placeholder='(___) ___-____'),
                         css_class='col-md-6'
                     ),
            ),

            Fieldset('<i class="fa fa-map-marker"></i> Location Information',
                     Div(
                         PrependedText('country_address', ''),
                         PrependedText('street_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         PrependedText('city_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         css_class='col-md-6'
                     ),
                     Div(
                         PrependedText('state_address', ''),
                         PrependedText('zip_code_address', '<i class="glyphicon glyphicon-map-marker"></i>'),
                         Div(
                            PrependedText('is_active', ''),
                            PrependedText('office_or_field', ''),
                            css_class='col-row-12'
                         ),
                         css_class='col-md-6'
                     ),
            ),
            Fieldset('<i class="fa fa-align-left"></i> Notes About of Employee:',
                 Div(
                     PrependedText('notes', '<i class="fa fa-align-left"></i>'),
                     css_class='col-md-12'
                 ),
                 ),
        )