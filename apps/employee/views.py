from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator

from apps.employee.forms import EmployeeForm
from apps.employee.models import Employee
from apps.project.models import ProjectEmployee
from apps.company.models import Company

from django.urls import  reverse_lazy

from django.views.generic import CreateView, ListView, UpdateView, DeleteView, TemplateView, DetailView

from django.contrib.auth.models import User
from random import randint
# from googlevoice import Voice
# from googlevoice.util import input


# Create your views here.
class EmployeeCreate(LoginRequiredMixin, CreateView):
    model = Employee
    form_class = EmployeeForm
    template_name = 'apps/employee/employee_form.html'

    def form_valid(self, form):
        # Genreacion de Codigo de Empleado
        # Correlative for Code

        last_register = Employee.objects.all().filter(companies_id=self.kwargs['pk'])

        if last_register:
            last_register = Employee.objects.all().filter(companies_id=form.instance.companies_id).latest('pk')
            correlative = int(last_register.employee_code[6:]) + 1
        else:
            correlative = 1

        form.instance.create_user = self.request.user
        form.save(commit=False) # Para obtener el ID pero no guardamos el registro aun.

        generate_code = form.instance.first_name[:1] + form.instance.last_name[:1] + '-C' + str(form.instance.companies_id) + '-' + str(correlative)

        form.instance.employee_code = generate_code
        form.save()

        # Get User ID Usuario
        # print(form.instance.create_user.pk)
        # ####
        return super(EmployeeCreate, self).form_valid(form)


    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(EmployeeCreate, self).get_initial()

        if self.kwargs['pk']:
            companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
            initial.update(
                {
                    'companies_id': companies_id['id'],
                    'companies': companies_id['id'],

                }
            )
        return initial

    def get_form_kwargs(self):
        kwargs = super(EmployeeCreate, self).get_form_kwargs()

        if self.kwargs['pk']:
            companies_id = Company.objects.values('id').get(id=self.kwargs['pk'])
            kwargs.update(
                {
                    'companies_id': companies_id['id'],
                })

        return kwargs

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('employee:employee_list', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('employee:employee_list', args=(self.object.id,))


class EmployeeList(ListView):
    model = Employee
    template_name = 'apps/employee/employee_list.html'

    def get_context_data(self, **kwargs):
        context = super(EmployeeList, self).get_context_data(**kwargs)

        employee_list = Employee.objects.all().select_related().filter(companies_id=self.kwargs['pk'])
        context['EmployeeList'] = employee_list

        return context

    def get_success_url(self, **kwargs):
        print(self.kwargs)

        if kwargs != None:
            return reverse_lazy('employee:employee_list', kwargs={'pk': self.kwargs['pk']})
        else:
            return reverse_lazy('employee:employee_list', args=(self.object.id,))


class EmployeeProfile(DetailView):
    model = Employee
    template_name = 'apps/employee/employee_profile.html'

    def get_context_data(self, **kwargs):
        context = super(EmployeeProfile, self).get_context_data(**kwargs)

        employee_list = Employee.objects.select_related(). \
            filter(id=self.kwargs['pk'])

        # employeeList = Employee.objects.all().filter(id=self.kwargs['pk'])

        context['EmployeeList'] = employee_list

        employee_qr = Employee.objects.values('id', 'companies', 'employee_code', 'category_employees', 'first_name', 'last_name', 'email', 'avatar', \
                                                        'cell_phone_number', 'city_address', 'country_address', 'state_address', 'zip_code_address',\
                                                        'street_address', 'office_or_field', 'notes', 'is_active').filter(id=self.kwargs['pk'])

        # context['QRCodeData'] = {
        #     'id': employee_qr[0]['employee_code'],
        #     #'first_name': employee_qr[0].pop('first_name'),
        #     #'last_name': employee_qr[0].pop('last_name'),
        # }
        if employee_qr.count() >0:
            context['QRCodeData'] = {
                'employee_code': employee_qr[0]['employee_code'],
                #'first_name': employee_qr[0].pop('first_name'),
                #'last_name': employee_qr[0].pop('last_name'),
            }

        # Informacion de proyectos en lo que a trabajado
        data = ProjectEmployee.objects.select_related().filter(employees_id=self.kwargs['pk'])
        context['ProjectEmployee'] = data

        return context


class EmployeeListCard(ListView):
    model = Employee
    template_name = 'apps/employee/employee_list_card.html'

    def get_queryset(self):
        print(self.kwargs)

        qs = ''
        if self.kwargs['letter'] == "All":
            qs = Employee.objects.all().filter(companies_id = self.kwargs['companies_id'])
        else:
            qs = Employee.objects.filter(first_name__iregex = r"(^|\s)%s" % self.kwargs['letter'], companies_id = self.kwargs['companies_id'])

        return qs


class EmployeeUpdate(UpdateView, User):
    model = Employee
    form_class = EmployeeForm
    template_name = 'apps/employee/employee_form.html'

    # Inicializar valores por default en formulario
    def get_initial(self):
        initial = super(EmployeeUpdate, self).get_initial()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        initial.update(
            {
                'companies_id': companies_id['id'],
            }
        )

        return initial

    def get_form_kwargs(self):
        kwargs = super(EmployeeUpdate, self).get_form_kwargs()

        companies_id = Company.objects.values('id').get(id=self.kwargs['companies_id'])
        kwargs.update(
            {
                'companies_id': companies_id['id'],
            })

        return kwargs

    def get_success_url(self, **kwargs):
        print(self.kwargs)

        if kwargs != None:
            return reverse_lazy('employee:employee_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('employee:employee_list', args=(self.object.id,))


class EmployeeDelete(DeleteView):
    model = Employee
    template_name = 'apps/employee/employee_delete.html'

    def get_context_data(self, **kwargs):

        context = super(EmployeeDelete, self).get_context_data(**kwargs)
        print(self.kwargs)

        context['DeleteRegisterExist'] = ProjectEmployee.objects.filter(employees_id=self.kwargs['pk'])
        context['DeleteRegister'] = Employee.objects.get(companies_id=self.kwargs['companies_id'], pk=self.kwargs['pk'])

        return context

    def get_success_url(self, **kwargs):
        if kwargs != None:
            return reverse_lazy('employee:employee_list', kwargs={'pk': self.kwargs['companies_id']})
        else:
            return reverse_lazy('employee:employee_list', args=(self.object.id,))