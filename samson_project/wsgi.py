"""
WSGI config for samson_project project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os, sys, site
import logging

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, "/home/giovanni/public_html/samsom_project/")

sys.path.append("/home/giovanni/public_html/samson_project")
site.addsitedir("/home/giovanni/public_html/samson_project/venv/lib/python3.6/site-packages")

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "samson_project.settings")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "conduit.settings.development")


from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
