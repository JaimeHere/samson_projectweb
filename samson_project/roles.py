from rolepermissions.roles import AbstractUserRole

class SystemAdmin(AbstractUserRole):
    available_permissions = {
        'create_employee_record': True,
    }

class System(AbstractUserRole):
    available_permissions = {
        # Employee Permission
        'create_employee': True,
        'edit_employee': True,
        'delete_employee': True,
        'view_employee': True,

    }