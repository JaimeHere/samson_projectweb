"""samson_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import login, logout_then_login

from django.conf import settings
from django.conf.urls.static import static
from django.template.backends import django

from apps.company.views import index, CompanyDashboard, RedirectDashboard, CompanyPending

urlpatterns = [
                  url(r'^admin/', admin.site.urls),
                  url(r'^404/$', login, {'template_name': '404.html'}, name='error_404'),
                  url(r'^company/', include(('apps.company.urls', 'company'), namespace='company')),
                  url(r'^company_dashboard/',
                      include(('apps.user_profile.urls', 'user_profile'), namespace="company_dashboard")),

                  url(r'^project/', include(('apps.project.urls', 'project'), namespace="project")),

                  url(r'^employee/', include(('apps.employee.urls', 'employee'), namespace="employee")),
                  url(r'^contractor/', include(('apps.contractor.urls','contractor'), namespace="contractor")),
                  url(r'^general_contractor/', include(('apps.general_contractor.urls', 'general_contractor'), namespace="general_contractor")),

                  url(r'^user_profile/', include(('apps.user_profile.urls', 'user_profile'), namespace="user_profile")),

                  url(r'^accounts/login/', login, {'template_name': 'index.html'}, name='login'),
                  url(r'^$', login, {'template_name': 'index.html'}, name='login'),
                  # url(r'^dashboard/', DashBoard.as_view(), name='dashboard'),
                  url(r'^logout/', logout_then_login, name='logout'),
                  url(r'^redirect_dashboard/', RedirectDashboard, name='redirect_dashboard'),
                  url(r'^dashboard/', CompanyDashboard, name='company_dashboard'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # Necesario por error de Page no found en directorio Media
